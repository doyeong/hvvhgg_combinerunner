#!/usr/bin/env python
import sys, os, re
import ROOT
import printer
from optparse import OptionParser

parser = OptionParser()
parser.add_option('--input', '-i', action='store',
                  default='/hdfs/store/user/knam/output_Nov04/data_MANUAL_optBins_fa3_ggHint_AutoRebin_swaped_newFSA_HWWSMPowheg_Syst_mergedBins_withSyst_MELAVBF/cmb/125/',
                  help='input path'
                  )
parser.add_option('--output', '-o', action='store',
                  default='GoFtest',
                  help='output path'
                  )
parser.add_option('--channel', '-c', action='store',
                  default='all',
                  help='tt/mt/et/em/all' # all is run individual
                  )
parser.add_option('--year', '-y', action='store',
                  default='all',
                  help='2016/2017/2018/Run2'
                  )
parser.add_option('--printcommands', '-p', action='store_true',
                  default=False, dest='printcommnds',
                  help='Do not run command lines, just print out them.'
                  )
parser.add_option('--inclusiveVBF', '-v', action='store_true',
                  default=False, dest='inclusiveVBF',
                  help='create inclusive VBF txt datacards, and run GoF over it'
                  )
parser.add_option('--inclusiveCAT', action='store_true',
                  default=False, dest='inclusive',
                  help='create inclusive(all categories) txt datacards, and run GoF over it'
                  )
parser.add_option('--WorkspaceOnly','-w', action='store_true',
                  default=False, dest='WorkspaceOnly',
                  help='create WorkspaceOnly'
                  )
parser.add_option('--FullCombination','-f', action='store_true',
                  default=False, dest='FullCombination',
                  help='FullCombination'
                  )
(options, args) = parser.parse_args()

def mkdir(dirPath):
    if not(os.path.isdir(dirPath)):
        printer.info("mkdir "+dirPath)
        if options.printcommnds is False: os.makedirs(os.path.join(dirPath))
    else:
        printer.msg(dirPath+" is already exist")

def runCmdP(cmd, PrinterMode):
    #printer.gray(cmd+"\n")
    print(cmd+"\n")
    if PrinterMode is False : os.system(cmd)

def identifier(folder):
    if "2018" in folder: year = "2018"
    elif "2017" in folder: year = "2017"
    elif "2016" in folder: year = "2016"

    if "_tt_" in folder: channel, tag = "tt", "#tau#tau"
    elif "_mt_" in folder: channel, tag = "mt", "#mu#tau"
    elif "_et_" in folder: channel, tag = "et", "e#tau"
    elif "_em_" in folder: channel, tag = "em", "e#mu"

    return year, channel, tag


def runGoF(folder, pathTxt, year, tag):
    # Create workspace
    #wsPath = "%s/%s.root"%(options.output, folder)
    #txt2wsCmd = "text2workspace.py -m 125 %s -o %s"%(pathTxt, wsPath)
    wsPathCreate = "fa3_Workspace_%s.root"%(folder)
    catdPath = pathTxt[:pathTxt.rfind("/")]
    wsPath = "%s/fa3_Workspace_%s.root"%(catdPath, folder)
    txt2wsCmd = "combineTool.py -M T2W -m 125 -P HiggsAnalysis.CombinedLimit.FA3_Interference_JHU_ggHSyst_rw_MengsMuVGGH:FA3_Interference_JHU_ggHSyst_rw_MengsMuVGGH -i %s -o %s"%(pathTxt, wsPathCreate)

    runCmdP(txt2wsCmd, options.printcommnds)
    # Run on toys
    toyRunCmd = "combineTool.py -M GoodnessOfFit --algorithm saturated -m 125 --there -d %s -n '.saturated.%s.toys'  -t 25 -s 0:19:1 --parallel 12 --setParameters muV=1.,CMS_zz4l_fai1=0.,muf=1.,fa3_ggH=0. "%(wsPath, folder)
    runCmdP(toyRunCmd, True) if options.WorkspaceOnly is True else runCmdP(toyRunCmd, options.printcommnds)
    # Run on data
    dataRunCmd = "combineTool.py -M GoodnessOfFit --algorithm saturated -m 125 --there -d %s -n '.saturated.%s' --setParameters muV=1.,CMS_zz4l_fai1=0.,muf=1.,fa3_ggH=0. "%(wsPath, folder)
    runCmdP(dataRunCmd, True) if options.WorkspaceOnly is True else runCmdP(dataRunCmd, options.printcommnds)
    # Creat json file 
    jsonCmd = """combineTool.py -M CollectGoodnessOfFit --input {catdPath}/higgsCombine.saturated.{folder}.GoodnessOfFit.mH125.root {catdPath}/higgsCombine.saturated.{folder}.toys.GoodnessOfFit.mH125.*.root -o {output}/{folder}.json""".format(output = options.output, folder=folder, catdPath=catdPath)
    runCmdP(jsonCmd, True) if options.WorkspaceOnly is True else runCmdP(jsonCmd, options.printcommnds)
    # Draw Plot
    if "_1_" in folder:    stringCat = "0jet category"
    elif "_2_" in folder:    stringCat = "Boosted category" 
    else: stringCat = folder

    plotCmd = """python ../CombineTools/scripts/plotGof.py --statistic saturated --mass 125.0 {output}/{folder}.json --output='{output}/saturated_{folder}' --title-left='{year} {tag}' --title-right='{stringCat}' """.format(output = options.output, folder=folder, year=year, tag = tag, stringCat = stringCat )
    runCmdP(plotCmd, True) if options.WorkspaceOnly is True else runCmdP(plotCmd, options.printcommnds)
    
    print "\n\n"
        
def combineVBFcards(folder, pathTxt, year, tag, channel):
    outputVBFcard = "%s/%s"%(options.output, folder.replace("_3_","_vbf_"))
    # Create combine txt cards
    combineCardCmd = "combineCards.py "
    NTdir = 10+1
    if "_tt_" in folder and "ggH" in pathTxt: NTdir = 8+1
    if "fa3" not in pathTxt: NTdir = 6+1
    # Run2
    if year == "Run2": 
        outputVBFcard = outputVBFcard.replace("2016", "Run2")
        for idx in range(3,NTdir):
            pathTxtcard2016 = pathTxt.replace("_3_", "_"+str(idx)+"_")
            combineCardCmd+="htt_%s_%s_13TeV_2016=%s "%(channel, str(idx), pathTxtcard2016)
            combineCardCmd+="htt_%s_%s_13TeV_2017=%s "%(channel, str(idx), pathTxtcard2016.replace("_2016.txt", "_2017.txt"))
            combineCardCmd+="htt_%s_%s_13TeV_2018=%s "%(channel, str(idx), pathTxtcard2016.replace("_2016.txt", "_2018.txt"))
    # individual year
    else:    
        for idx in range(3,NTdir):
            combineCardCmd+="htt_%s_%s_13TeV_%s=%s "%(channel, str(idx),year, pathTxt.replace("_3_", "_"+str(idx)+"_"))

    combineCardCmd += "&> "+outputVBFcard

    runCmdP(combineCardCmd, options.printcommnds)
    return outputVBFcard

def combineAllCATcards(folder, pathTxt, year, tag, channel):
    outputAllCATcard = "%s/%s"%(options.output, folder.replace("_1_","_AllCAT_"))
    # Create combine txt cards
    combineCardCmd = "combineCards.py "
    NTdir = 10+1
    if "_tt_" in folder and "ggH" in pathTxt: NTdir = 8+1
    if "fa3" not in pathTxt: NTdir = 6+1
    # Run2
    if year == "Run2": 
        outputAllCATcard = outputAllCATcard.replace("2016", "Run2")
        for idx in range(1,NTdir):
            pathTxtcard2016 = pathTxt.replace("_1_", "_"+str(idx)+"_")
            combineCardCmd+="htt_%s_%s_13TeV_2016=%s "%(channel, str(idx), pathTxtcard2016)
            combineCardCmd+="htt_%s_%s_13TeV_2017=%s "%(channel, str(idx), pathTxtcard2016.replace("_2016.txt", "_2017.txt"))
            combineCardCmd+="htt_%s_%s_13TeV_2018=%s "%(channel, str(idx), pathTxtcard2016.replace("_2016.txt", "_2018.txt"))
    # individual year    
    else:
        for idx in range(1,NTdir):
            combineCardCmd+="htt_%s_%s_13TeV_%s=%s "%(channel, str(idx),year, pathTxt.replace("_1_", "_"+str(idx)+"_"))

    combineCardCmd += "&> "+outputAllCATcard

    runCmdP(combineCardCmd, options.printcommnds)
    return outputAllCATcard

def combineAllcards(path):
    channels = ["tt","mt","et","em"]
    years = ["2016","2017","2018"]
    NTdir = 10+1

    outputAllcard =  "%s/htt_AllChannels_AllCAT_Run2.txt"%(options.output)
    # Create combine txt cards
    combineCardCmd = "combineCards.py "
    for channel in channels:
        channelPath = path+"/"+channel+"/125"
        if "_tt_" in channel and "ggH" in channelPath: NTdir = 8+1
        if "fa3" not in path: NTdir = 6+1

        for year in years:
            for idx in range(1,NTdir):
                combineCardCmd += "htt_%s_%s_13TeV_%s=%s/htt_%s_%s_%s.txt "%(channel, str(idx), year, channelPath, channel, str(idx), year)

    combineCardCmd += "&> "+outputAllcard
    runCmdP(combineCardCmd, options.printcommnds)
    return outputAllcard

def main():
    path=options.input
    folder_list = os.listdir(path)
    
    # Output dir
    mkdir(options.output)

    # FullCombination
    if options.FullCombination is True:
        printer.whiteRedBold("FullCombination")
        if "125" in path: 
            printer.warning("Input folder should be higher folder than this")
            sys.exit(1)
        chList = ["tt","mt","et","em"]
        haveAllchannels = True
        # Just check if we have all folders for individual channels
        for ch in chList:
            channelPath = path+"/"+ch+"/125"
            if os.listdir(channelPath):
                printer.green(channelPath)
            else:
                haveAllchannels = False
                printer.warning(channelPath + "doesn't exist")
        if haveAllchannels is False: sys.exit(1)
        # Start
        outputAllcard = combineAllcards(path)
        runGoF(outputAllcard[outputAllcard.rfind("/")+1:], outputAllcard, "Run2", "All channels")
                

                
    # Run2, per channel, per year
    else:
        for folder in folder_list:
            if "htt_" not in folder: continue
            if ".txt" not in folder: continue
            if "root" in folder: continue
            year, channel, tag = identifier(folder)
            if options.channel != "all" and options.channel != channel: continue
            if options.year != "all":
                if options.year == "Run2": 
                    if "2016" not in folder: continue
                    printer.info("GoF Run2")
                else: # individual era
                    if options.year != year: continue

            printer.whiteBlueBold(folder)
            # Pick up card per channel/year/TDirectory
            pathTxt = """{path}/{folder}""".format(path=path,folder=folder)
            if os.path.isfile(pathTxt):
                print pathTxt
            else:
                printer.warning("there is no "+pathTxt)
                sys.exit(1)
            print folder
            # Run inclusive VBF
            if options.inclusiveVBF is True:
                if "_1_" in folder or "_2_" in folder: 
                    continue #
                    '''
                    copiedTxt = "%s/%s"%(options.output,folder)
                    copyTxtCmd = "cp %s %s"%(pathTxt,copiedTxt)
                    runCmdP(copyTxtCmd, options.printcommnds)
                    runGoF(folder, copiedTxt, year, tag)
                    '''
                    runGoF(folder, pathTxt, year, tag)
                elif "_3_" in folder:
                    #continue #
                    outputVBFcard = combineVBFcards(folder, pathTxt, "Run2", tag, channel) if options.year == "Run2" else combineVBFcards(folder, pathTxt, year, tag, channel)
                    runGoF(outputVBFcard[outputVBFcard.rfind("/")+1:], outputVBFcard, year, tag)
                else:
                    continue
            elif options.inclusive is True:
                if "_1_" in folder:
                    outputAllCATcard = combineAllCATcards(folder, pathTxt, "Run2", tag, channel) if options.year == "Run2" else combineAllCATcards(folder, pathTxt, year, tag, channel)
                    runGoF(outputAllCATcard[outputAllCATcard.rfind("/")+1:], outputAllCATcard, year, tag)
            else:
                '''
                copiedTxt = "%s/%s"%(options.output,folder)
                copyTxtCmd = "cp %s %s"%(pathTxt,copiedTxt)
                runCmdP(copyTxtCmd, options.printcommnds)
                runGoF(folder, copiedTxt, year, tag)
                '''
                #if "_1_" in folder or "_2_" in folder: continue #
                runGoF(folder, pathTxt, year, tag)



if __name__ == "__main__":
    main()
