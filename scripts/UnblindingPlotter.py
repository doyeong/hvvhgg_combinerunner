#!/usr/bin/env python
import ROOT
import re, sys, os
import argparse
import printer
from ROOT import TLine
from ROOT import TMath

parser = argparse.ArgumentParser("Create pre/post-fit plots")
parser.add_argument(
    "--channel",
    action="store",
    dest="channel",
    default="tt",
    help="Which channel to run over? (et, mt, em, tt)")
parser.add_argument(
    "--year",
    action="store",
    dest="year",
    default="2016",
    help="Which year to run over? (2016,2017,2018)")
parser.add_argument(
    "--variable",
    action="store",
    dest="variable",
    default="L1",
    help="Provide the relative path to the target input file")
parser.add_argument(
    "--fit",
    action="store",
    dest="fit",
    default="prefit",
    help="prefit/postfit")
parser.add_argument(
    "--unblinding",
    action="store_true",
    dest="unblinding",
    default=False,
    help="unblinding")
args = parser.parse_args()


def add_lumi(lumi_x):
    lowX=0.7
    lowY=0.835
    lumi  = ROOT.TPaveText(lowX, lowY+0.06, lowX+0.30, lowY+0.16, "NDC")
    lumi.SetBorderSize(   0 )
    lumi.SetFillStyle(    0 )
    lumi.SetTextAlign(   12 )
    lumi.SetTextColor(    1 )
    lumi.SetTextSize(0.06)
    lumi.SetTextFont (   42 )
    #lumi.AddText("35.9 fb^{-1} (13 TeV)")
    lumi.AddText(str(lumi_x)+" fb^{-1} (13 TeV)")
    return lumi

def add_CMS():
    lowX=0.11
    lowY=0.835
    lumi  = ROOT.TPaveText(lowX, lowY+0.06, lowX+0.15, lowY+0.16, "NDC")
    lumi.SetTextFont(61)
    lumi.SetTextSize(0.08)
    lumi.SetBorderSize(   0 )
    lumi.SetFillStyle(    0 )
    lumi.SetTextAlign(   12 )
    lumi.SetTextColor(    1 )
    lumi.AddText("CMS")
    return lumi

def add_Preliminary():
    lowX=0.35
    lowY=0.835
    lumi  = ROOT.TPaveText(lowX, lowY+0.06, lowX+0.15, lowY+0.16, "NDC")
    lumi.SetTextFont(52)
    lumi.SetTextSize(0.06)
    lumi.SetBorderSize(   0 )
    lumi.SetFillStyle(    0 )
    lumi.SetTextAlign(   12 )
    lumi.SetTextColor(    1 )
    lumi.AddText("Preliminary")
    return lumi

def make_legend(dx):
    #output = ROOT.TLegend(0.85+dx, 0.25, 0.99, 0.9, "", "brNDC")
    output = ROOT.TLegend(0.85+dx, 0.38, 0.98, 0.98, "", "brNDC")
    output.SetLineWidth(0)
    output.SetLineStyle(0)
    output.SetFillColor(0)
    output.SetBorderSize(0)
    output.SetTextFont(62)
    return output

def make_legend_signal(dx):
    #output = ROOT.TLegend(0.85+dx, 0.0, 0.99, 0.24, "", "brNDC")
    output = ROOT.TLegend(0.85+dx, 0.0, 0.99, 0.37, "", "brNDC")
    output.SetLineWidth(0)
    output.SetLineStyle(0)
    output.SetFillColor(0)
    output.SetBorderSize(0)
    output.SetTextFont(62)
    return output

def blinder(Data,Bkg,SM,BSM):
    for ibin in range(Data.GetNbinsX()+1):
        n_sig = SM.GetBinContent(ibin)+BSM.GetBinContent(ibin)
        n_bkg = Bkg.GetBinContent(ibin)
        if n_bkg > 0 and n_sig / TMath.Sqrt(n_bkg + 0.09*0.09*n_bkg*n_bkg) >= 0.3:
            print "bin " +str(ibin)+ " is blinded."
            Data.SetBinContent(ibin, -100000)


palette = {
    "embedded":["#ffcc66","Z#rightarrow#tau#tau"],
    "jetFakes":["#ffccff","jet#rightarrow#tau_{h} misID"], 
    "QCD":["#ffccff","QCD multijet"],
    "TT":["#9999cc","t#bar{t}+jets"],
    "TTL":["#9999cc","t#bar{t}+jets"],
    "W":["#a53db8","W+jets"],
    "ZL":["#4496c8","Z#rightarrow#mu#mu/ee"],
    "ZLL":["#4496c8","Z#rightarrow#mu#mu/ee"],
    "others":["#12cadd","Others"]
}

# DBSM is actually D2j binning in TDirectory
DBSM = ["0.0-0.2","0.2-0.5","0.5-0.8","0.8-1.0"]
if args.channel == "mt" or args.channel == "et": DBSM = ["0.00-0.25","0.25-0.50","0.50-0.75","0.75-1.00"]
if args.channel == "em" : DBSM = ["0.00-0.25","0.25-0.50","0.50-0.75","0.75-1.00"]
ROOT.gStyle.SetFrameLineWidth(2)
ROOT.gStyle.SetLineWidth(2)
ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(True)

myfileStr = "output_shapes_f%s_%s_%s.root"%(args.variable, args.channel, args.year)
file2Str= "htt_input_f%s_%s_%s.root"%(args.variable, args.channel, args.year)
#myfileStr = "unrolledTest"+args.year+"/output_shapes_f%s_%s_%s.root"%(args.variable, args.channel, args.year)
#file2Str= "unrolledTest"+args.year+"/htt_input_f%s_%s_%s.root"%(args.variable, args.channel, args.year)

#myfileStr = "/hdfs/store/user/doyeong/output_Sep29_newFSA/postfit/output_shapes_f%s_%s_%s.root"%(args.variable, args.channel, args.year)
#file2Str= "/hdfs/store/user/doyeong/output_Sep29_newFSA/postfit/htt_input_f%s_%s_%s.root"%(args.variable, args.channel, args.year)
#pathSenka="/hdfs/store/user/senka/forDoyeong/forUnrolled/Aug_AutoRebin/"
#pathSenka="/hdfs/store/user/doyeong/ACunrolled_Aug29/"
#pathSenka="/hdfs/store/user/senka/forDoyeong/forUnrolled/Aug_AutoRebin/"
#pathSenka="/nfs_scratch/doyeong/postfit_26Nov2020/"
#pathSenka="/nfs_scratch/doyeong/postfit_3Dec2020/" #-> em
#pathSenka="/nfs_scratch/doyeong/postfit_03Dec2020/" #->em+ltaua
#pathSenka="/nfs_scratch/doyeong/postfit_10Dec2020/" # mt2016 ZL horn removed
#pathSenka="/nfs_scratch/doyeong/postfit_11Dec2020/" # mt2016 ZL horn removed + remove mtt < 90GeV
#pathSenka="/nfs_scratch/doyeong/postfit_14Dec2020/" # mt2016 ZL systematics
#pathSenka="/nfs_scratch/doyeong/postfit_16Dec2020/" # mt/et 1D
pathSenka="/nfs_scratch/doyeong/postfits_16Dec2020/" # unblinding step 2 latest
if args.channel != "tt": 
    if "ggH" in args.variable:
        myfileStr = pathSenka+"output_shapes_fa3_ggH_%s_%s.root"%(args.channel, args.year)
        file2Str = pathSenka+"htt_input_%s_ggH_%s.root"%(args.year, args.channel)
    else:
        myfileStr = pathSenka+"output_shapes_f%s_%s_%s.root"%(args.variable, args.channel, args.year)
        if "postfit_16Dec2020" in pathSenka: 
            myfileStr = pathSenka+"output_shapes_f%s_%s_0jet_%s.root"%(args.variable, args.channel, args.year) # output_shapes_fa3_mt_0jet_2016.root
        file2Str = pathSenka+"htt_input_f%s_%s_%s.root"%(args.variable, args.channel, args.year)
printer.info("myfile : "+myfileStr)
if os.path.isfile(myfileStr) is False:
    printer.warning(myfileStr + " is not available. ")
    sys.exit(0)
printer.info("file2 "+file2Str)
if os.path.isfile(file2Str) is False:
    printer.warning(file2Str + " is not available. ")
    sys.exit(0)
myfile=ROOT.TFile(myfileStr,"read")
file2=ROOT.TFile(file2Str,"read")

data_yield=0.
couplingMap = {"a3":"0PM", "a1":"0M", "a2":"0PH", "L1":"0L1", "L1Zg":"0L1Zg"}
cate=[]

Ncat = 4 
if "a3" in args.variable: Ncat = 10
for i in range(1, 1+Ncat):
    cate.append("htt_"+args.channel+"_"+str(i)+"_13TeV_"+args.year)

adapt=ROOT.gROOT.GetColor(12)
new_idx=ROOT.gROOT.GetListOfColors().GetSize() + 1
trans=ROOT.TColor(new_idx, adapt.GetRed(), adapt.GetGreen(),adapt.GetBlue(), "",0.4)



# Total bin number
nbinsDBSM = len(cate)
nbinsNN, nbinsD2j = 10, 8
binnum=nbinsNN*nbinsD2j

# Bkg composition
majors=["jetFakes","embedded"]
minors=["TTL","VVL","ZL","STL","ggH_hww125","qqH_hww125","WH_hww125","ZH_hww125"]
if args.channel=="em":
    majors=["QCD","embedded","TT","ZLL","W"]
    minors=["VV","qqH_hww125","ggH_hww125","VH_hww125"] #,"W","ZLL"]
if args.channel=="mt" or args.channel=="et":
    majors=["jetFakes","embedded","TT","ZL"]
    minors=["VVL","VVT","STT","STL","ggH_hww125","qqH_hww125","WH_hww125","ZH_hww125"]
print ":: Used Bkg."
print majors
print minors

printer.gray("Start loop!")
for mycat in cate:
    print " cat: ",mycat
    mycat_wo13TeV = mycat.replace("_13TeV_","_")
    mycat_fit = mycat+"_"+args.fit if args.channel=="tt" else mycat_wo13TeV+"_"+args.fit
    # Pick up SM qqH and SM ggH and porper BSM - from pre-fit file
    SMname = "reweighted_ggH_htt_0PM125" if args.variable=="a3ggH" else "reweighted_qqH_htt_0PM125"
    SMname_other = SMname.replace("ggH", "qqH") if "ggH" in SMname else SMname.replace("qqH", "ggH")
    BSMname = SMname.replace("0PM","0M")
    if args.channel!="tt": # and  args.fit=="prefit":
        if "reweighted_ggH" in SMname: 
            SMname, BSMname = "GGH2Jets_sm_M125", "GGH2Jets_pseudoscalar_M125"
        else:
            SMname_other = "GGH2Jets_sm_M125"

    if args.fit=="prefit":
        SM=file2.Get(mycat_wo13TeV).Get(SMname)
        #SM = myfile.Get(mycat_fit).Get(SMname[:-3])
        SM_other=file2.Get(mycat_wo13TeV).Get(SMname_other)
        BSM=file2.Get(mycat_wo13TeV).Get(BSMname)
    else:
        SMname = SMname[:-3]
        BSMname = BSMname[:-3]
        SMname_other = SMname_other[:-3]
        SM = myfile.Get(mycat_fit).Get(SMname)
        SM_other = myfile.Get(mycat_fit).Get(SMname_other)
        BSM = myfile.Get(mycat_fit).Get(BSMname)

    #printer.gray("SM: "+SMname+"\tBSM: "+BSMname+"\tSM other: "+SMname_other)    
    printer.gray("SM: %s(%.3f)\t BSM: %s(%.3f)\t other: %s(%.3f)"%(SMname, SM.Integral(), BSMname, BSM.Integral(), SMname_other, SM_other.Integral()))
    # Pick up the rest - from post-fit file
    printer.gray(mycat_fit)
    Bkg=myfile.Get(mycat_fit).Get("TotalBkg")
    BkgList = {}
    BkgListOrder = ["others","embedded","jetFakes"]
    if args.channel=="em" : BkgListOrder = ["others","W","ZLL","TT","embedded","QCD"]
    if args.channel=="mt" or args.channel=="et": BkgListOrder = ["others","ZL","TT","embedded","jetFakes"]
    for minor in minors:
        if myfile.Get(mycat_fit).Get(minor):
            print "got "+ minor
        else:
            printer.warning( "No "+ minor)
            continue
        if "others" not in BkgList: #minor == minors[0]: 
            BkgList["others"] = myfile.Get(mycat_fit).Get(minor).Clone("others")
        else:
            BkgList["others"].Add(myfile.Get(mycat_fit).Get(minor), 1)
    for major in majors:
        if (args.channel=="mt" or args.channel=="et") and major=="TT":
            BkgList[major] = myfile.Get(mycat_fit).Get("TTT")
            if myfile.Get(mycat_fit).Get("TTL"):
                BkgList[major].Add(myfile.Get(mycat_fit).Get("TTL"),1)
            printer.info("TTL and TTT merged")
        else:
            BkgList[major] = myfile.Get(mycat_fit).Get(major)

    Signal=myfile.Get(mycat_fit).Get("TotalSig")
    Data=myfile.Get(mycat_fit).Get("data_obs")



    ##################
    ## Build Canvas ##
    ##################
    c=ROOT.TCanvas("canvas","",0,0,1200,600)
    c.cd()
    pad1 = ROOT.TPad("pad1","pad1",0,0.35,1,1)
    pad1.SetLogy()
    pad1.Draw()
    pad1.cd()
    pad1.SetFillColor(0)
    pad1.SetBorderMode(0)
    pad1.SetBorderSize(10)
    pad1.SetTickx(1)
    pad1.SetTicky(1)
    pad1.SetRightMargin(0.15)
    pad1.SetTopMargin(0.122)
    pad1.SetBottomMargin(0.026)
    pad1.SetFrameFillStyle(0)
    pad1.SetFrameLineStyle(0)
    pad1.SetFrameLineWidth(3)
    pad1.SetFrameBorderMode(0)
    pad1.SetFrameBorderSize(10)

    Data.GetXaxis().SetLabelSize(0)
    Data.GetXaxis().SetTitle("")
    Data.GetXaxis().SetTitleSize(0.06)
    Data.GetXaxis().SetNdivisions(505)
    Data.GetYaxis().SetLabelFont(42)
    Data.GetYaxis().SetLabelOffset(0.01)
    Data.GetYaxis().SetLabelSize(0.06)
    Data.GetYaxis().SetTitleSize(0.085)
    Data.GetYaxis().SetTitleOffset(0.6)
    Data.GetYaxis().SetTitle("Events/bin")
    Data.GetYaxis().SetTickLength(0.012)
    if args.unblinding is False: blinder(Data, Bkg, SM, BSM)

    SM.SetLineColor(ROOT.EColor(ROOT.kRed))
    SM.SetLineWidth(3)
    SM_other.SetLineColor(ROOT.EColor(ROOT.kMagenta+1))
    SM_other.SetLineWidth(2)
    SM_other.SetLineStyle(2)
    BSM.SetLineColor(ROOT.EColor(ROOT.kBlack))
    BSM.SetLineWidth(3)
    
    acScale=10
    SM.Scale(acScale)
    SM_other.Scale(acScale)
    BSM.Scale(acScale)

    Data.SetLineColor(1)
    errorBand=Bkg.Clone()
    errorBand.SetMarkerSize(0)
    errorBand.SetFillColor(new_idx)
    errorBand.SetLineColor(1)
    errorBand.SetLineWidth(1)
    mystack=ROOT.THStack("mystack","mystack")
    for bkg in BkgListOrder:  
        if bkg in BkgList: 
            printer.blue(bkg)
            mystack.Add(BkgList[bkg])
    mystack.Add(Signal)


    Data.SetMarkerStyle(20)
    Data.SetMaximum(1000*Data.GetMaximum())
    #Data.SetMaximum(0.3*Data.GetMaximum())
    Data.SetMinimum(0.01)
    Data.Draw("e0pX0")
    Data.SetTitle("")
    Data.SetMinimum(0.09)
    
    mystack.Draw("histsame")
    errorBand.Draw("e2same")
    SM.Draw("histsame")
    SM_other.Draw("histsame")
    BSM.Draw("histsame")
    Data.Draw("e0pX0same")

    legend=make_legend(0.0)
    legend.AddEntry(Data,"Observed","elp")
    legend.AddEntry(Signal,"H#rightarrow#tau#tau","f")
    def GetColor(histName):
        hist = BkgList[histName]
        hist.SetFillColor(ROOT.TColor.GetColor(palette[histName][0]))
        hist.SetLineColor(ROOT.TColor.GetColor(palette[histName][0]))
        legend.AddEntry(hist, palette[histName][1], "f")

    for major in majors:        
        GetColor(major)
    GetColor("others")
    Signal.SetLineColor(2)
    #Signal.SetLineWidth(2)
    Signal.SetFillColor(2)

    legend.AddEntry(errorBand,"Total unc.","f")

    legend.Draw()
    
    legend2=make_legend_signal(0.0)
    if args.variable=="a3ggH":
        legend2.AddEntry(SM,"#splitline{SM ggH H#rightarrow#tau#tau}{(#sigma = "+str(acScale)+" #sigma_{SM})}","l")
        legend2.AddEntry(BSM,"#splitline{BSM ggH H#rightarrow#tau#tau}{(#sigma = "+str(acScale)+" #sigma_{SM})}","l")
        legend2.AddEntry(SM_other,"#splitline{SM VBF H#rightarrow#tau#tau}{(#sigma = "+str(acScale)+" #sigma_{SM})}","l")
    else:
        legend2.AddEntry(SM,"#splitline{SM VBF H#rightarrow#tau#tau}{(#sigma = "+str(acScale)+" #sigma_{SM})}","l")
        legend2.AddEntry(BSM,"#splitline{BSM VBF H#rightarrow#tau#tau}{(#sigma = "+str(acScale)+" #sigma_{SM})}","l")
        legend2.AddEntry(SM_other,"#splitline{SM ggH H#rightarrow#tau#tau}{(#sigma = "+str(acScale)+" #sigma_{SM})}","l")
    legend2.Draw()


    lumi_x=35.9
    if args.year=="2017":
        lumi_x=41.5
    elif args.year=="2018":
        lumi_x=59.5
        
    l1=add_lumi(lumi_x)
    l1.Draw("same")
    l2=add_CMS()
    l2.Draw("same")

    myvariable="D_{#Lambda1}"
    if args.variable=="a3":
        myvariable="D_{0-}"
    if args.variable=="a3ggH":
        myvariable="D_{0-}^{ggH}"
    if args.variable=="a2":
        myvariable="D_{0h+}"
    if args.variable=="L1Zg":
        myvariable="D_{#Lambda1}^{Z#gamma}"

    ## Here TLine
    binMaximum, boundarysize, textOffset = 80, 10, 0.093
    if (args.channel=="mt" or args.channel=="et"): binMaximum, boundarysize, textOffset = 14*10, 10, 0.053 
    # 0jet / boosted
    if cate.index(mycat)==0: 
        binMaximum, boundarysize, textOffset = 9, 9, 0.02
        if (args.channel=="mt" or args.channel=="et"):
            binMaximum, boundarysize, textOffset = 27, 9, 0.25
    elif cate.index(mycat)==1: 
        binMaximum, boundarysize, textOffset = 45, 9, 0.15
        if args.channel=="tt":
            binMaximum, boundarysize, textOffset = 54, 9, 0.125
            
    tlines=[]
    labels=[]
    text=[]
    #NNbinning = ["0.0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1.0"]
    SliceBinning = ["0.0","0.125","0.25","0.375","0.5","0.625","0.75","0.875","1.0"]
    if (args.channel=="mt" or args.channel=="et"):
        SliceBinning = ["0.0","0.05","0.1","0.15","0.2","0.3","0.4","0.5","0.6","0.7","0.8","0.85","0.9","0.95","1.0"]
    if cate.index(mycat)==1:
        if args.channel!="tt":
            SliceBinning = ["0","60","120","200","250","inf"]
            Data.SetMaximum(100*Data.GetMaximum())
        else:
            SliceBinning = ["0","60","120","200","250","300","inf"]
        if (args.channel=="mt" or args.channel=="et"): Data.SetMaximum(100*Data.GetMaximum())
    elif cate.index(mycat)==0 and (args.channel=="et" or args.channel=="mt"):
        SliceBinning = ["30","40","50","inf"]
    for slice in range(0,binMaximum/boundarysize):
        whereX=boundarysize*slice
        l = TLine(whereX, 0, whereX, Data.GetMaximum())
        l.SetLineColor(14)
        l.SetLineStyle(2)
        tlines.append(l)
        if cate.index(mycat)==1:
            labels.append(ROOT.TPaveText(0.12+slice*textOffset, 0.33, 0.16+slice*textOffset+0.04, 0.6, "NDC"))
        else:
            labels.append(ROOT.TPaveText(0.12+slice*textOffset, 0.33, 0.12+slice*textOffset+0.04, 0.6, "NDC"))

        SliceLabel = "%s,%s"%(SliceBinning[slice],SliceBinning[slice+1])
        if cate.index(mycat)==0:
            if (args.channel=="mxt"):
                text.append(labels[slice].AddText("p_{T}^{#tau} #in ["+SliceLabel+"] GeV"))
            elif (args.channel=="et"):
                text.append(labels[slice].AddText("p_{T}^{#tau} #in ["+SliceLabel+"] GeV"))
            else:
                text.append(labels[slice].AddText(""))
        elif cate.index(mycat)==1:
            text.append(labels[slice].AddText("p_{T}^{H} #in ["+SliceLabel+"] GeV"))
        else:
            text.append(labels[slice].AddText("NN_{disc} #in ["+SliceLabel+"]"))


    for tline in tlines:
        tline.SetLineStyle(3)
        tline.Draw()

        labels[tlines.index(tline)].SetBorderSize(   0 )
        labels[tlines.index(tline)].SetFillStyle(    0 )
        labels[tlines.index(tline)].SetTextAlign(   12 )
        #labels[tlines.index(tline)].SetTextSize ( 0.045 )
        labels[tlines.index(tline)].SetTextSize ( 0.05 )
        labels[tlines.index(tline)].SetTextColor(    4 )
        labels[tlines.index(tline)].SetTextFont (   42 )
        text[tlines.index(tline)].SetTextAngle(90)
        labels[tlines.index(tline)].Draw("same")
        
    pad1.RedrawAxis()
    
    categ  = ROOT.TPaveText(0.25, 0.895, 0.63, 0.99, "NDC") if "a3" in args.variable else ROOT.TPaveText(0.33, 0.895, 0.63, 0.99, "NDC")
    categ.SetBorderSize(   0 )
    categ.SetFillStyle(    0 )
    categ.SetTextAlign(   12 )
    categ.SetTextSize ( 0.08 )
    categ.SetTextColor(    1 )
    categ.SetTextFont (   42 )
    if "a3" in args.variable:
        idxDBSM = cate.index(mycat)/2 if cate.index(mycat)%2==0 else (cate.index(mycat)-1)/2 
        idxDBSM = idxDBSM-1
        strDCPbin = "D_{CP}<0" if cate.index(mycat)%2==0 else "D_{CP}>0"
        if args.channel=="mt" or args.channel=="et":
            idxDBSM = cate.index(mycat) if cate.index(mycat)<4+2==0 else (cate.index(mycat)-(4+2))
            strDCPbin = "D_{CP}>0" if cate.index(mycat)<4+2 else "D_{CP}<0"
            printer.red(str(idxDBSM))
        if args.channel=="em":
            if cate.index(mycat)==0:
                categ.AddText("0jet, e#mu")
            elif cate.index(mycat)==1:
                categ.AddText("Boosted, e#mu")                
            else:
                categ.AddText("VBF, e#mu    %s [%s], %s"%("D_{2jets}",DBSM[idxDBSM],strDCPbin))
        if args.channel=="et":
            if cate.index(mycat)==0:
                categ.AddText("0jet, e#tau")
            elif cate.index(mycat)==1:
                categ.AddText("Boosted, e#tau")                
            else:
                categ.AddText("VBF, e#tau_{h}    %s [%s], %s"%("D_{2jets}",DBSM[idxDBSM],strDCPbin))
        if args.channel=="mt":
            if cate.index(mycat)==0:
                categ.AddText("0jet, #mu#tau")
            elif cate.index(mycat)==1:
                categ.AddText("Boosted, #mu#tau")                
            else:
                categ.AddText("VBF, #mu#tau_{h},    %s [%s], %s"%("D_{2jets}",DBSM[idxDBSM],strDCPbin))
        if args.channel=="tt":
            categ.AddText("VBF, #tau_{h}#tau_{h},    %s [%s], %s"%("D_{2jets}",DBSM[idxDBSM],strDCPbin))
        if args.channel=="all":
            categ.AddText("All #tau#tau")
        if args.channel=="emetmt":
            categ.AddText("VBF, e#mu + e#tau_{h} + #mu#tau_{h}")

            

    else:
        if args.channel=="em":
            categ.AddText("VBF, e#mu    %s [%s]"%("D_{2jets}",DBSM[cate.index(mycat)]))
        if args.channel=="et":
            categ.AddText("VBF, e#tau_{h}    %s [%s]"%("D_{2jets}",DBSM[cate.index(mycat)]))
        if args.channel=="mt":
            categ.AddText("VBF, #mu#tau_{h},    %s [%s]"%("D_{2jets}",DBSM[cate.index(mycat)]))
        if args.channel=="tt":
            categ.AddText("VBF, #tau_{h}#tau_{h},    %s [%s]"%("D_{2jets}",DBSM[cate.index(mycat)]))
        if args.channel=="all":
            categ.AddText("All #tau#tau")
        if args.channel=="emetmt":
            categ.AddText("VBF, e#mu + e#tau_{h} + #mu#tau_{h}")
    categ.Draw("same")


    # Now ratio pad
    c.cd()
    pad2 = ROOT.TPad("pad2","pad2",0,0,1,0.35)
    pad2.SetTopMargin(0.05)
    pad2.SetBottomMargin(0.35)
    pad2.SetRightMargin(0.15)
    pad2.SetTickx(1)
    pad2.SetTicky(1)
    pad2.SetFrameLineWidth(3)
    #pad2.SetGridx()
    pad2.SetGridy()
    pad2.Draw()
    pad2.cd()
    h1=Data.Clone()
    h1.SetMaximum(1.4)
    h1.SetMinimum(0.6)
    h1.SetMarkerStyle(20)
    h3=errorBand.Clone()
    hwoE=errorBand.Clone()
    for iii in range (1,hwoE.GetSize()-2):
        hwoE.SetBinError(iii,0)
    h3.Sumw2()
    h1.Sumw2()
    h1.SetStats(0)
    h1.Divide(hwoE)
    h3.Divide(hwoE)
    h1.GetXaxis().SetTitle("m_{#tau#tau} (GeV)")
    h1.GetXaxis().SetTitle("")
    h1.GetYaxis().SetLabelSize(0.08)
    h1.GetYaxis().SetTitle("Obs./Exp.")
    h1.GetXaxis().SetNdivisions(505)
    h1.GetYaxis().SetNdivisions(5)
    
    h1.GetXaxis().SetTitleSize(0.15)
    h1.GetYaxis().SetTitleSize(0.15)
    h1.GetYaxis().SetTitleOffset(0.3)
    h1.GetXaxis().SetTitleOffset(1.04)
    h1.GetXaxis().SetLabelSize(0.11)
    h1.GetYaxis().SetLabelSize(0.11)
    h1.GetXaxis().SetTitleFont(42)
    h1.GetYaxis().SetTitleFont(42)

    
    h1.LabelsOption("v","X")
    h1.GetXaxis().SetLabelOffset(0.02)
    h1.GetXaxis().SetLabelSize(0.06)
    NNbinning = ["0.0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1.0", "0.0"]
    for idx in range(1,binMaximum):  
        strNNbin = NNbinning[idx%10]+"-"+NNbinning[(idx%10)+1]
        #h1.GetXaxis().SetBinLabel(idx,strNNbin)

    h1.GetXaxis().SetLabelSize(0.07)
    h1.GetYaxis().SetLabelSize(0.08)
    h1.GetYaxis().SetTickLength(0.012)
    h1.GetYaxis().SetNdivisions(5)
    
    h1.GetXaxis().SetLabelSize(0.13)
    h1.GetYaxis().SetLabelSize(0.11)
    h1.GetXaxis().SetTitleFont(42)
    h1.GetYaxis().SetTitleFont(42)
    h1.LabelsOption("v","X")

    h1.Draw("e0pX0")
    h3.Draw("e2same")

    categ2  = ROOT.TPaveText(0.86, 0.1, 0.95, 0.4, "NDC")
    categ2.SetBorderSize(   0 )
    categ2.SetFillStyle(    0 )
    categ2.SetTextAlign(   12 )
    categ2.SetTextSize ( 0.16 )
    categ2.SetTextColor(    1 )
    categ2.SetTextFont (   62 )
    #if args.channel!="em": categ2.AddText(myvariable)
    #else: categ2.AddText("D_{2jets}")
    if cate.index(mycat)<=1: categ2.AddText("Bin num")
    else: categ2.AddText(myvariable)
    categ2.Draw("same")
    
    c.cd()
    pad1.Draw()

    ROOT.gPad.RedrawAxis()


    c.Modified()
    namepng = "/nfs_scratch/doyeong/tmp/AN-18-168/Figures/unrolled_unblindingstep2/unrolled_"+args.variable+"_"+args.channel+"_"+mycat+"_"+args.fit+".png" 
    c.SaveAs(namepng)
    c.SaveAs(namepng.replace("png","pdf"))
    



    
