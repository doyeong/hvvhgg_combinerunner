import printer
import os, sys
import ROOT as r

def runCmd(cmd):
    printer.gray(cmd)
    os.system(cmd)

def fileOpener(fileName, openOption):
    if r.TFile(fileName, openOption).IsZombie():
        printer.error("There is no %s" % fileName)
        return sys.exit(1)
    else:
        printer.gray("File opend:\t"+fileName)
        file = r.TFile(fileName, openOption)
        return r.TFile(fileName, openOption)

def imgLister(list):
    for l in list:
        printer.green("imgcat "+l)
