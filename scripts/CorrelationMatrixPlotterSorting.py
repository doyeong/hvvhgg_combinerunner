import os, sys, re
import ROOT
from ROOT import TCanvas, TFile, TH1F, TH2D, TColor
import printer
from ROOT import gROOT
from array import array


ROOT.gStyle.SetPaintTextFormat("4.2f")
ROOT.gStyle.SetFrameLineWidth(2)
ROOT.gStyle.SetLineWidth(1)
ROOT.gStyle.SetOptStat(0)
gROOT.SetBatch(ROOT.kTRUE)


filename=sys.argv[1]
#MeasurementType=sys.argv[2] 
verbose=sys.argv[2]

def add_CMS():
    lowX=0.15
    lowY=0.720
    lumi  = ROOT.TPaveText(lowX, lowY+0.06, lowX+0.15, lowY+0.16, "NDC")
    lumi.SetTextFont(61)
    lumi.SetTextSize(0.05)
    lumi.SetBorderSize(   0 )
    lumi.SetFillStyle(    0 )
    lumi.SetTextAlign(   12 )
    lumi.SetTextColor(    1 )
    lumi.AddText("CMS")
    return lumi

def add_Preliminary():
    lowX=0.15
    lowY=0.67
    lumi  = ROOT.TPaveText(lowX, lowY+0.06, lowX+0.15, lowY+0.16, "NDC")
    lumi.SetTextFont(52)
    lumi.SetTextSize(0.04)
    lumi.SetBorderSize(   0 )
    lumi.SetFillStyle(    0 )
    lumi.SetTextAlign(   12 )
    lumi.SetTextColor(    1 )
    lumi.AddText("Preliminary")
    return lumi

def add_lumi():
    lowX=0.60
    lowY=0.82
    lumi  = ROOT.TPaveText(lowX, lowY+0.06, lowX+0.30, lowY+0.16, "NDC")
    lumi.SetBorderSize(   0 )
    lumi.SetFillStyle(    0 )
    lumi.SetTextAlign(   12 )
    lumi.SetTextColor(    1 )
    lumi.SetTextSize(0.04)
    lumi.SetTextFont (   42 )
    lumi.AddText("137.0 fb^{-1} (13 TeV)")
    return lumi

#parametersToMeasure=["fa3_ggH","CMS_zz4l_fai1","muV","muf"]
parametersToMeasure=["CMS_zz4l_fai1","fa3_ggH","muV","muf"]





def main():
    if "fa3" not in filename: parametersToMeasure.remove("fa3_ggH")
    fIn = TFile(filename, 'READ')
    filenameOut = filename[filename.rfind("/")+1:-5]+"_ReducedCorrMat.root"
    printer.info("input: "+filename+"\n"+"output: "+filenameOut) #+"\nparametersToMeasure : "+MeasurementType)
    #cofH = fIn.Get("covariance_fit_s")    
    cofH = fIn.Get("h_correlation")    
    nPOI = 4 if "fa3" in filename else 3
    # Fill 2D histogram
    hPOI = TH2D("","",nPOI,0,nPOI,nPOI,0,nPOI)
    for par_x in parametersToMeasure:
        for par_y in parametersToMeasure:
            #printer.red(par_x)
            #printer.blue(par_y)
            i, j = parametersToMeasure.index(par_x)+1, parametersToMeasure.index(par_y)+1
            binContent = cofH.GetBinContent(cofH.GetXaxis().FindBin(par_x),cofH.GetYaxis().FindBin(par_y))
            if i>=j: 
                hPOI.SetBinContent(i,j,binContent)
            lableWrite = par_x
            measureCoupling = "a3"
            if "fa3" in filename: 
                if "CMS_zz4l_fai1" in lableWrite: lableWrite = "f_{a3}"
                if "ggH" in filename: measureCoupling = "a3_ggH" 
            elif "fa2" in filename: 
                measureCoupling = "a2"
                if "CMS_zz4l_fai1" in lableWrite: lableWrite = "f_{a2}"
            elif "fL1Zg" in filename:
                measureCoupling = "L1Zg" 
                if "CMS_zz4l_fai1" in lableWrite: lableWrite = "f_{L1}^{Zg}"
            elif "fL1" in filename: 
                measureCoupling = "L1"
                if "CMS_zz4l_fai1" in lableWrite: lableWrite = "f_{L1}"
            if "fa3_ggH" in lableWrite: lableWrite = "f_{a3}^{ggH}"
            if "muV" in lableWrite:lableWrite = "#mu_{V}"
            if "muf" in lableWrite:lableWrite = "#mu_{f}"

        hPOI.SetMarkerSize(1.5)
        hPOI.GetXaxis().SetBinLabel(i,lableWrite)
        hPOI.GetYaxis().SetBinLabel(i,lableWrite)


    ##########################
    ##     Build Canvas     ##
    ##########################
    canvas = ROOT.TCanvas("","",0,0,1000,1000)
    canvas.cd()
    margine = 0.1 # 0.4
    canvas.SetLeftMargin(margine)
    canvas.SetBottomMargin(margine)
    margine2 = 0.13
    canvas.SetRightMargin(margine2)
    canvas.SetTopMargin(margine2)
    
    palette=[4,0,2]
    paletteArr=array("f", palette)
    #TColor.CreateGradientColorTable(3,)
    #ROOT.gStyle.SetPalette(2)
    #hPOI.GetXaxis().ChangeLabel(-1,30,-1.,-1,-1,-1,"test")
    hPOI.SetMinimum(-1.0)
    hPOI.SetMaximum(+1.0)
    # colors 
    ROOT.gStyle.SetNumberContours(128)
    red = array('d', [0., 1., 1.])
    green = array('d', [0., 1., 0.])
    blue = array('d', [1., 1., 0.])
    points = array('d', [0., 0.5, 1.])
    ROOT.TColor.CreateGradientColorTable(3, points, red, green, blue, 128)
    hPOI.GetXaxis().SetLabelSize(0.07)
    hPOI.GetYaxis().SetLabelSize(0.07)
    hPOI.GetZaxis().SetLabelSize(0.04)
    hPOI.LabelsOption("v")
    hPOI.Draw("COLZTEXT")
    # 
    lumi = add_lumi()
    lumi.Draw("same")
    cms = add_CMS()
    cms.Draw("same")
    prelim = add_Preliminary()
    prelim.Draw("same")

    canvas.Update()
    namepdf="/nfs_scratch/doyeong/tmp_Jan27/AN-18-168/Figures/POIs_Correlation/correlation_"+measureCoupling+".pdf"
    canvas.SaveAs(namepdf)
    canvas.SaveAs(namepdf.replace("pdf","png"))





    fOut = TFile(filenameOut, 'RECREATE')

    fIn.Close()
    fOut.Close()

if __name__ == "__main__":
    main()
