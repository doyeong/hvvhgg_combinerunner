import os, sys, re
import array
import ROOT
from ROOT import TCanvas, TFile, TH1F, TH2F
import printer

def main():
    filename = sys.argv[1]
    fIn = TFile(filename, 'READ')
    filenameOutM = filename[0:-5]+"_DCPbinRemoved_M.root"
    fOutM = TFile(filenameOutM, 'RECREATE')
    filenameOutP = filename[0:-5]+"_DCPbinRemoved_P.root"
    fOutP = TFile(filenameOutP, 'RECREATE')

    for dir in fIn.GetListOfKeys(): 
        tdirNameIn, tdirNameOut = dir.GetName(), dir.GetName()
        tdirIn = fIn.Get(tdirNameIn)
        printer.gray("\n>> "+tdirNameIn)
        # Copy DCPm to output1, the rest to output2
        if "DCPm" in tdirNameIn:        
            printer.blue("Copy " + tdirNameIn + " to "+filenameOutM)
            tdirNameOut = tdirNameIn[:tdirNameIn.find("_DCPm")]
            fOutM.mkdir(tdirNameOut)
        elif "DCPp" in tdirNameIn:    
            printer.red("Copy " + tdirNameIn + " to "+filenameOutP)
            tdirNameOut = tdirNameIn[:tdirNameIn.find("_DCPp")]
            fOutP.mkdir(tdirNameOut)    
        else:
            printer.red("Copy " + tdirNameIn + " to "+filenameOutP)
            fOutP.mkdir(tdirNameOut)

        tdirOut = fOutM.Get(tdirNameOut) if "DCPm" in tdirNameIn else fOutP.Get(tdirNameOut)


        for h in tdirIn.GetListOfKeys():
            hName = h.GetName()
            if "ttH" in hName: continue
            if "ggZH" in hName: continue
            if "EWKW" in hName: continue
            # Copy histogram to output
            histo = tdirIn.Get(hName)
            tdirOut.cd()
            histo.Clone().Write()
    cmd = "\n\nhadd -f "+filename[:-5]+"_noDCP.root "+filenameOutM+" "+filenameOutP+"\n\n"
    printer.gray(cmd)
    fIn.Close()
    fOutM.Close()
    fOutP.Close()
    os.system(cmd)

if __name__ == "__main__":
    main()

