import os, sys, re
import array
import ROOT
from ROOT import TCanvas, TFile, TH1F, TH2F
from ROOT import gROOT
from ROOT import gDirectory
from optparse import OptionParser

ROOT.gStyle.SetFrameLineWidth(2)
ROOT.gStyle.SetLineWidth(1)
ROOT.gStyle.SetOptStat(0)
gROOT.SetBatch(ROOT.kTRUE)

parser = OptionParser()
parser.add_option('--poi', '-p', action='store',
                  default="fa3", dest='poi',
                  help='fa3/fa2/fL1/fL1Zg/fa3ggH'
                  )
parser.add_option('--year', '-y', action='store',
                  default='2017', dest='year',
                  help='2016/2017/2018/all'
                  )
(ops, args) = parser.parse_args()

def main():
    filename = sys.argv[1]
    f = TFile(filename, 'READ')
    tree = f.Get("limit")
    if "ggH" in ops.poi:
        tree.Draw("2*deltaNLL:fa3_ggH>>"+ops.poi,"2*deltaNLL<20.0")
    else:
        tree.Draw("2*deltaNLL:CMS_zz4l_fai1>>"+ops.poi,"2*deltaNLL<20.0")
    
    ##########################
    ##     Build Canvas     ##
    ##########################
    
    canvas = ROOT.TCanvas("","",0,0,1000,1100)
    canvas.cd()
    h = f.Get(ops.poi)    
    h.SetMarkerStyle(20)
    h.SetMarkerSize(1)
    h.SetTitle(ops.year)
    h.GetXaxis().SetTitle(ops.poi)
    h.SetMaximum(4.0)
    h.Draw()
    canvas.SaveAs(ops.poi+".pdf")
    
    print "\n\nimgcat "+ops.poi+".pdf\n\n"

    


if __name__ == "__main__":
    main()
