#!/usr/bin/env python
import ROOT
from ROOT import *
import re, sys, os
from array import array
import printer
import operator

filename_1 = sys.argv[1]
filename_out = sys.argv[2]
chn = sys.argv[3]

year=2016
if len(sys.argv)>3:
    year = int(sys.argv[4])

has_DCP_bins=0
print "N args= %s"%len(sys.argv)
printer.info("N args 4 (year) = %s"%sys.argv[4])
#print "  N args 5 = %s"%sys.argv[5]

par = 0

if len(sys.argv)>5:
    has_DCP_bins = int(sys.argv[5])
if len(sys.argv)>6: 
    par = sys.argv[6]


islog=1
unrollSV=1

file=ROOT.TFile(filename_1,"r")
file1=ROOT.TFile(filename_out,"recreate")

file.cd()
dirList = gDirectory.GetListOfKeys()
vbf_dir_names=[]
for dirName in dirList:
    if 'vbf' in dirName.GetName(): vbf_dir_names.append(dirName.GetName())

name_SM_qqH_powheg="VBF125"
name_SM_qqH_JHU="reweighted_qqH_htt_0PM125"
name_PS_qqH_JHU="reweighted_qqH_htt_0M125"

name_SM_WH_powheg="WH125"
name_SM_WH_JHU="reweighted_WH_htt_0PM125"
name_PS_WH_JHU="reweighted_WH_htt_0M125"

name_SM_ZH_powheg="ZH125"
name_SM_ZH_JHU="reweighted_ZH_htt_0PM125"
name_PS_ZH_JHU="reweighted_ZH_htt_0M125"

name_SM_ggH_powheg="ggH125"
name_SM_ggH_JHU="reweighted_ggH_htt_0PM125"

yield_qqH_Powheg=0
yield_qqH_JHU=0
yield_WH_Powheg=0
yield_WH_JHU=0
yield_ZH_Powheg=0
yield_ZH_JHU=0
yield_ggH_Powheg=0
yield_ggH_JHU=0

yield_qqH_Powheg_vbf=0
yield_qqH_JHU_vbf=0
yield_WH_Powheg_vbf=0
yield_WH_JHU_vbf=0
yield_ZH_Powheg_vbf=0
yield_ZH_JHU_vbf=0
yield_ggH_Powheg_vbf=0
yield_ggH_JHU_vbf=0


# sum up yields in VBF category:
for k1 in vbf_dir_names:
    #print "dir: ",k1
    #print " signal DCP_minus: ", k1.GetName()
    #h1 = k1.ReadObj()
    #nom=k1.GetName()

    printer.gray("looking for dir %s and histo %s "%(k1,name_SM_qqH_powheg))
    #file.cd(vbf_dir_names)
    yield_qqH_Powheg_vbf=yield_qqH_Powheg_vbf+file.Get(k1).Get(name_SM_qqH_powheg).Integral()
    yield_ggH_Powheg_vbf=yield_ggH_Powheg_vbf+file.Get(k1).Get(name_SM_ggH_powheg).Integral()
    yield_WH_Powheg_vbf=yield_WH_Powheg_vbf+file.Get(k1).Get(name_SM_WH_powheg).Integral()
    yield_ZH_Powheg_vbf=yield_ZH_Powheg_vbf+file.Get(k1).Get(name_SM_ZH_powheg).Integral()
    yield_qqH_JHU_vbf=yield_qqH_JHU_vbf+file.Get(k1).Get(name_SM_qqH_JHU).Integral()
    print "getting %s histo in %s dir in file %s"%(name_SM_ggH_JHU,k1,filename_1)
    yield_ggH_JHU_vbf=yield_ggH_JHU_vbf+file.Get(k1).Get(name_SM_ggH_JHU).Integral()
    yield_WH_JHU_vbf=yield_WH_JHU_vbf+file.Get(k1).Get(name_SM_WH_JHU).Integral()
    yield_ZH_JHU_vbf=yield_ZH_JHU_vbf+file.Get(k1).Get(name_SM_ZH_JHU).Integral()
    printer.green("Powheg qqH yield: %.4f\t Powheg ggH yield  : %.4f"%(file.Get(k1).Get(name_SM_qqH_powheg).Integral(), file.Get(k1).Get(name_SM_ggH_powheg).Integral()))
    printer.green("JHU qqH yield   : %.4f\t MadGraph ggH yield: %.4f"%(file.Get(k1).Get(name_SM_qqH_JHU).Integral(), file.Get(k1).Get(name_SM_ggH_JHU).Integral()))

print "summed ggH VBF yield: ",yield_ggH_Powheg_vbf
print "summed VBFH VBF yield: ",yield_qqH_Powheg_vbf
printer.gray( " ==> scale ggH = %.4f/%.4f = %.4f"%(yield_ggH_Powheg_vbf, yield_ggH_JHU_vbf, yield_ggH_Powheg_vbf/yield_ggH_JHU_vbf))
printer.gray( " ==> scale qqH = %.4f/%.4f = %.4f"%(yield_qqH_Powheg_vbf, yield_qqH_JHU_vbf, yield_qqH_Powheg_vbf/yield_qqH_JHU_vbf))


#print "-> summed VBFH VBF yield: ",yield_qqH_Powheg_vbf

print "\n\nLoop 2"
for k1 in dirList:
    #print "\n signal DCP_minus: ", k1.GetName()
    h1 = k1.ReadObj()
    nom=k1.GetName()
    if nom=='mt_inclusive' or nom=='et_inclusive':
	continue
    #print "\t summed VBFH VBF yield: ",yield_qqH_Powheg_vbf


    nom_out=nom
   
    nom_out=nom_out.replace("D0ggH_0p00to0p30","ggHMELA_bin1")
    nom_out=nom_out.replace("D0ggH_0p30to0p45","ggHMELA_bin2")
    nom_out=nom_out.replace("D0ggH_0p45to0p55","ggHMELA_bin3")
    nom_out=nom_out.replace("D0ggH_0p55to1p00","ggHMELA_bin4")
    
    nom_out=nom_out.replace("D0ggH_0p0to0p2","ggHMELA_bin1")
    nom_out=nom_out.replace("D0ggH_0p2to0p4","ggHMELA_bin2")
    nom_out=nom_out.replace("D0ggH_0p4to0p7","ggHMELA_bin3")
    nom_out=nom_out.replace("D0ggH_0p7to1p0","ggHMELA_bin4")
 
    printer.gray("\n\nmkdir "+nom_out+"\t in the output root file")
    file1.mkdir(nom_out)
    
    h1.cd()
    histoList = gDirectory.GetListOfKeys()
    name_last=""
    
    
    h_SM_qqH_powheg_c=h1.Get(name_SM_qqH_powheg)
    h_SM_qqH_JHU_c=h1.Get(name_SM_qqH_JHU)
    h_SM_qqH_powheg=h_SM_qqH_powheg_c.Clone()
    h_SM_qqH_JHU=h_SM_qqH_JHU_c.Clone()

    h_SM_WH_powheg_c=h1.Get(name_SM_WH_powheg)
    h_SM_WH_JHU_c=h1.Get(name_SM_WH_JHU)
    h_SM_WH_powheg=h_SM_WH_powheg_c.Clone()
    h_SM_WH_JHU=h_SM_WH_JHU_c.Clone()
    
    h_SM_ZH_powheg_c=h1.Get(name_SM_ZH_powheg)
    h_SM_ZH_JHU_c=h1.Get(name_SM_ZH_JHU)
    h_SM_ZH_powheg=h_SM_ZH_powheg_c.Clone()
    h_SM_ZH_JHU=h_SM_ZH_JHU_c.Clone()
    
    h_SM_ggH_powheg_c=h1.Get(name_SM_ggH_powheg)
    h_SM_ggH_JHU_c=h1.Get(name_SM_ggH_JHU)
    h_SM_ggH_powheg=h_SM_ggH_powheg_c.Clone()
    h_SM_ggH_JHU=h_SM_ggH_JHU_c.Clone()
    
    
    scale_to_Powheg_qqH=1.
    scale_to_Powheg_WH=1.
    scale_to_Powheg_ZH=1.
    scale_to_Powheg_ggH=1.


    # for 0jet/boosted -> 
    if "vbf" not in nom:

        #        print "to not in:  Powheg qqH: %s, ggH %s, WH %s, ZH %s"%(yield_qqH_Powheg,yield_ggH_Powheg,yield_WH_Powheg,yield_ZH_Powheg)
        #        print "to not in:  JHU qqH: %s, ggH %s, WH %s, ZH %s"%(yield_qqH_JHU,yield_ggH_JHU,yield_WH_JHU,yield_ZH_JHU)
        #print " \t\t\t  ===> this is not VBF category!"
        yield_qqH_Powheg=h_SM_qqH_powheg.Integral()
        yield_qqH_JHU=h_SM_qqH_JHU.Integral()
        yield_WH_Powheg=h_SM_WH_powheg.Integral()
        yield_WH_JHU=h_SM_WH_JHU.Integral()
        yield_ZH_Powheg=h_SM_ZH_powheg.Integral()
        yield_ZH_JHU=h_SM_ZH_JHU.Integral()
        yield_ggH_Powheg=h_SM_ggH_powheg.Integral()
        yield_ggH_JHU=h_SM_ggH_JHU.Integral()
    else:
        #print "summed VBFH VBF yield: ",yield_qqH_Powheg_vbf

        yield_qqH_Powheg=yield_qqH_Powheg_vbf
        yield_qqH_JHU=yield_qqH_JHU_vbf
        yield_WH_Powheg=yield_WH_Powheg_vbf
        yield_WH_JHU=yield_WH_JHU_vbf
        yield_ZH_Powheg=yield_ZH_Powheg_vbf
        yield_ZH_JHU=yield_ZH_JHU_vbf
        yield_ggH_Powheg=yield_ggH_Powheg_vbf
        yield_ggH_JHU=yield_ggH_JHU_vbf
        
    
    #print " yield_qqH_Powheg= ",yield_qqH_Powheg 
    file.cd(nom)
    if (h_SM_qqH_JHU.Integral()>0):
        scale_to_Powheg_qqH=yield_qqH_Powheg/yield_qqH_JHU
    if (h_SM_WH_JHU.Integral()>0):
        scale_to_Powheg_WH=yield_WH_Powheg/yield_WH_JHU
    if (h_SM_ZH_JHU.Integral()>0):
        scale_to_Powheg_ZH=yield_ZH_Powheg/yield_ZH_JHU
    if (h_SM_ggH_JHU.Integral()>0):
        scale_to_Powheg_ggH=yield_ggH_Powheg/yield_ggH_JHU
    printer.whiteBlackBold("    Scale Factors    ")
    printer.gray("ggH SF = %.4f/%.4f = %.4f"%(yield_ggH_Powheg, yield_ggH_JHU, scale_to_Powheg_ggH))
    printer.gray("qqH SF = %.4f/%.4f = %.4f"%(yield_qqH_Powheg, yield_qqH_JHU, scale_to_Powheg_qqH))
    printer.gray("WH  SF = %.4f/%.4f = %.4f"%(yield_WH_Powheg, yield_WH_JHU, scale_to_Powheg_WH))
    printer.gray("ZH  SF = %.4f/%.4f = %.4f"%(yield_ZH_Powheg, yield_ZH_JHU, scale_to_Powheg_ZH))

    #print "scale to Powheg ggH= ",scale_to_Powheg_ggH

    for k2 in histoList:
        if (k2.GetName()!=name_last):
            h2 = k2.ReadObj()
            h3=h2.Clone()
            h3.SetName(k2.GetName())
            if "WH_htt_0" in h3.GetName() and h3.Integral()>0.:
                h3.Scale(scale_to_Powheg_WH)
            if "ZH_htt_0" in h3.GetName() and h3.Integral()>0.:
                h3.Scale(scale_to_Powheg_ZH)
            if "ggH_htt_" in h3.GetName() and h3.Integral()>0.:
                h3.Scale(scale_to_Powheg_ggH)
            if "qqH_htt_" in h3.GetName() and h3.Integral()>0.:
                h3.Scale(scale_to_Powheg_qqH)
                   
            nom=k1.GetName()
            dir_m_name=nom
            #print "goto dir: ",dir_m_name
            nom_out=nom
            nom_out=nom_out.replace("D0ggH_0p00to0p30","ggHMELA_bin1")
            nom_out=nom_out.replace("D0ggH_0p30to0p45","ggHMELA_bin2")
            nom_out=nom_out.replace("D0ggH_0p45to0p55","ggHMELA_bin3")
            nom_out=nom_out.replace("D0ggH_0p55to1p00","ggHMELA_bin4")

            nom_out=nom_out.replace("D0ggH_0p0to0p2","ggHMELA_bin1")
            nom_out=nom_out.replace("D0ggH_0p2to0p4","ggHMELA_bin2")
            nom_out=nom_out.replace("D0ggH_0p4to0p7","ggHMELA_bin3")
            nom_out=nom_out.replace("D0ggH_0p7to1p0","ggHMELA_bin4")

            file1.cd(nom_out)
            name_histo=h3.GetName()
            #print " dir: ",nom, "  histo_name= ",name_histo    
            # for 0jet/boosted category do NOT use JHU ggH sample, use Powheg and rename to "GGH2Jets*BLA*"

            # for 0jet/boosted : copy powheg ggH including all shapes as AC named histograms
            if "ggH125" in h3.GetName() and "vbf" not in nom:
                name_histo=h3.GetName().replace("ggH125","reweighted_ggH_htt_0PM125")
                name_histo_PS=h3.GetName().replace("ggH125","reweighted_ggH_htt_0M125")
                name_histo_maxmix=h3.GetName().replace("ggH125","reweighted_ggH_htt_0Mf05ph0125")
                h3.SetName(name_histo)
                h3.Write(name_histo)
                h3.SetName(name_histo_PS)
                h3.Write(name_histo_PS)
                h3.SetName(name_histo_maxmix)
                h3.Write(name_histo_maxmix)

            # 0jet/boosted : copy AC named histograms with different name
            if "reweighted_ggH_htt_0PM125" in h3.GetName() and "vbf" not in nom:
                name_histo=h3.GetName().replace("reweighted_ggH_htt_0PM125","GGH2Jets_sm_origiM")
            if "reweighted_ggH_htt_0M125" in h3.GetName() and "vbf" not in nom:
                name_histo=h3.GetName().replace("reweighted_ggH_htt_0M125","GGH2Jets_pseudoscalar_origiM")
            if "reweighted_ggH_htt_0Mf05ph0125" in h3.GetName() and "vbf" not in nom:
                name_histo=h3.GetName().replace("reweighted_ggH_htt_0Mf05ph0125", "GGH2Jets_pseudoscalar_orrigiMf05ph0")
                h3.SetName(name_histo)
                h3.Write()
            # for vbf categories
            if "reweighted_ggH_htt" in name_histo and ("vbf" in nom):
                name_histo=name_histo.replace("reweighted_ggH_htt_0PM125","GGH2Jets_sm_M")
                name_histo=name_histo.replace("reweighted_ggH_htt_0M125","GGH2Jets_pseudoscalar_M")
                h3.Write()
                h3.SetName(name_histo)     
                h3.Write(name_histo) 
                


            name_histo=name_histo.replace("JetFakes","jetFakes")
            if "ggH" not in h3.GetName() or "ggH_hww" in h3.GetName():
                h3.SetName(name_histo)
                h3.Write(name_histo)

            name_last=h3.GetName()

               
    h1.Close()
printer.green(filename_out + "\t is created!")
