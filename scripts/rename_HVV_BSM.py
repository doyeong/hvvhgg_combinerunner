import os, sys, re
import array
import ROOT
from ROOT import TCanvas, TFile, TH1F, TH2F
import printer
from ROOT import gROOT
from ROOT import gDirectory
import time

def renameHistSTXS(hName, HVV):
    hNameDic = {
        "reweighted_qqH_htt_0M125":"reweighted_qqH_htt_"+HVV+"125",
        "reweighted_qqH_htt_0Mf05ph0125":"reweighted_qqH_htt_"+HVV+"f05ph0125",
        "reweighted_WH_htt_0M125":"reweighted_WH_htt_"+HVV+"125",
        "reweighted_WH_htt_0Mf05ph0125":"reweighted_WH_htt_"+HVV+"f05ph0125",
        "reweighted_ZH_htt_0M125":"reweighted_ZH_htt_"+HVV+"125",
        "reweighted_ZH_htt_0Mf05ph0125":"reweighted_ZH_htt_"+HVV+"f05ph0125",
        "ggH125_ggH125_MINLO":"ggH_MINLO125"
    }
    if hName in hNameDic.keys():
        #printer.purple('\t'+hName+ ' is renamed as ' + hNameDic[hName])
        return hNameDic[hName]
    elif "MINLO" in hName:
        newName = hName.replace("ggH125_ggH125_MINLO", "ggH_MINLO125", 1)
        #printer.purple('\t'+hName+ ' is renamed as ' + newName)
        return newName
    else: 
        return hName 


def main():
    start = time.time()
    filename = sys.argv[1]    
    HVV = "0M"
    if "fa2" in filename: HVV = "0PH"
    elif "L1Zg" in filename: HVV = "0L1Zg"
    elif "L1" in filename: HVV = "0L1"
    fIn = TFile(filename, 'READ')
    dirList = []
    for dir in fIn.GetListOfKeys():
        #print(dir.GetName())
        dirList.append(dir.GetName())
    fIn.Close()

    filenameOnlyPath = filename[:filename.rfind('/')]
    filenameNoPath = filename[filename.rfind('/')+1:]
    filenameOut = filenameOnlyPath+"/datacards/"+filenameNoPath
    printer.yellow("Create "+filenameOut)
    fOut = TFile(filenameOut, 'RECREATE')
    for dir in dirList: #fIn.GetListOfKeys():
        fIn = TFile(filename, 'READ')
        tdirName = dir#.GetName() 
        fOut.mkdir(tdirName)
        tdirOut = fOut.Get(tdirName)
        printer.whiteBlueBold("\n>> "+tdirName)
        tdir = fIn.Get(tdirName)
        for h in tdir.GetListOfKeys():
            hName = h.GetName()
            if 'jetFakes_' in hName:
                if 'jetFakes_CMS' not in hName: continue
            if 'CMS_rawFF' in hName or 'CMS_FF_closure' in hName:
                if 'jetFakes' not in hName: continue
            if 'data_obs_' in hName: continue
            if 'DO_NOT_USE' in hName: continue
            if '_0PM125_' in hName or '_0M125_' in hName or '_0Mf05ph0125_' in hName:
                if 'Up' not in hName and 'Down' not in hName: 
                    continue
                
            histo = tdir.Get(hName)
            #printer.msg('\t'+hName+' is copying ... '+str(histo.Integral()))
            tdirOut.cd()
            '''
            if "reweighted_ggH_htt_0Mf05ph0" in hName: 
                h1 = histo.Clone(renameHistSTXS(hName, HVV))
                #printer.red(str(h1.Integral()))
                #h1.Scale(0.5)
                h1.Write()
                #printer.red(str(h1.Integral()))
            '''
            if "CMS_JER" in hName:
                histo.Clone(renameHistSTXS(hName, HVV).replace("CMS_JER","CMS_res_j")).Write()
            elif "CMS_Jet" in hName:
                histo.Clone(renameHistSTXS(hName, HVV).replace("CMS_Jet","CMS_scale_j_")).Write()
            else: 
                histo.Clone(renameHistSTXS(hName, HVV)).Write()

            if "CMS_scale_emb_t" in hName or "CMS_doubletautrg_emb_dm" in hName:
                histo.Clone(renameHistSTXS(hName, HVV).replace("_emb_","_")).Write()
        fIn.Close()
    fOut.Close()
    printer.gray("time : %.3fs"%(time.time() - start)  )


if __name__ == "__main__":
    main()
