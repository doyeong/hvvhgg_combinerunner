#!/usr/bin/env python

import re
import sys, os
import ROOT
from optparse import OptionParser
from ROOT import TCanvas, TFile, TH1F
from ROOT import gROOT
from ROOT import gDirectory
import printer

ROOT.gStyle.SetFrameLineWidth(2)
ROOT.gStyle.SetLineWidth(1)
ROOT.gStyle.SetOptStat(0)
gROOT.SetBatch(ROOT.kTRUE)

def titleStyler(title, titletext):
  title.SetFillStyle(0)
  title.SetBorderSize(0)
  title.AddText(titletext)

def padMaker(pad):
    pad.SetFillColor(0)
    pad.Draw()
    pad.cd()

def ratioStyle(h,updown,xlable):
    h.SetLineWidth(1)
    h.SetLineColor(1)
    h.SetMarkerStyle(20)
    h.SetMarkerSize(2)
    h.GetXaxis().SetTitle(xlable) #"Visible Mass [GeV]")
    h.GetXaxis().SetTitleSize(0.11)
    h.GetXaxis().SetTitleOffset(0.7)
    h.GetXaxis().SetLabelSize(0.08)
    h.GetYaxis().SetLabelSize(0.08)
    h.GetYaxis().SetTitle("Ratio wrt SM")
    h.GetYaxis().SetTitleSize(0.08)
    h.GetYaxis().SetTitleOffset(0.4)
    #if "BSM" in h.GetName():
    #elif "MIX" in h.GetName():
    if updown>0.0:
        h.SetMarkerColor(2)
    elif updown<0.0:
        h.SetMarkerColor(8)

parser = OptionParser()
parser.add_option('--shape', '-s', action='store',
                  default="DM0", dest='shape',
                  help='shape name without Up/Down'
                  )
parser.add_option('--sam', action='store',
                  default="VBF125", dest='sample',
                  help='sample such as ZTT, VBF125, etc '
                  )
parser.add_option('--var', '-v', action='store',
                  default="vis_mass", dest='var',
                  help='path of input files (output of slicer)'
                  )
parser.add_option('--bin', '-b', action='store',
                  default="30,0,300", dest='bin',
                  help='number of bins'
                  )
(ops, args) = parser.parse_args()

acpoints=["0PM","0PM","0Mf05ph0125"]
signals=["ggH", "qqH", "WH", "ZH"] 
#signals=["qqH", "WH", "ZH"] 
#signals=["ggH"]
cateMap={
  "htt_tt_1_13TeV":"0Jet",
  "htt_tt_2_13TeV":"Boosted",
  "htt_tt_3_13TeV":"D_BSM [0.0:0.2]",
  "htt_tt_4_13TeV":"D_BSM [0.2:0.5]",
  "htt_tt_5_13TeV":"D_BSM [0.5:0.8]",
  "htt_tt_6_13TeV":"D_BSM [0.8:1.0]",
  "tt_0jet":"0Jet",
  "tt_boosted":"Boosted",
  "tt_vbf_ggHMELA_bin1":"D_BSM [0.0:0.2]",
  "tt_vbf_ggHMELA_bin2":"D_BSM [0.2:0.5]",
  "tt_vbf_ggHMELA_bin3":"D_BSM [0.5:0.8]",
  "tt_vbf_ggHMELA_bin4":"D_BSM [0.8:1.0]",
  "tt_vbf_ggHMELA_bin1_DCPm":"D_BSM [0.0:0.2] (-)",
  "tt_vbf_ggHMELA_bin2_DCPm":"D_BSM [0.2:0.5] (-)",
  "tt_vbf_ggHMELA_bin3_DCPm":"D_BSM [0.5:0.8] (-)",
  "tt_vbf_ggHMELA_bin4_DCPm":"D_BSM [0.8:1.0] (-)",
  "tt_vbf_ggHMELA_bin1_DCPp":"D_BSM [0.0:0.2] (+)",
  "tt_vbf_ggHMELA_bin2_DCPp":"D_BSM [0.2:0.5] (+)",
  "tt_vbf_ggHMELA_bin3_DCPp":"D_BSM [0.5:0.8] (+)",
  "tt_vbf_ggHMELA_bin4_DCPp":"D_BSM [0.8:1.0] (+)",
}


path = sys.argv[1] #"htt_input_fL1_tt_2017.root"
poi = "None"
if "_fa3_" in path: poi = "a3"
if "_fa3ggH_" in path: poi = "a3ggH"
elif "_fa2_" in path: poi = "a2"
elif "_fL1_" in path: poi = "L1"
elif "_fL1Zg_" in path: poi = "L1Zg"
printer.info(poi)
file = ROOT.TFile(path,"r")
printer.info("Input file:\t"+path)
# Loop over TDirectory
for dir in file.GetListOfKeys(): 
    tdirName = dir.GetName() 
    printer.whiteBlueBold(">> "+tdirName)
    if "201" in tdirName:
      year = tdirName[-4:]
    else: 
      year = path[path.find("201"):path.find("201")+4]
    printer.blue(year)
    # Loop over production modes
    for signal in signals:
        printer.gray(" Making "+ signal + " overlaid plot")
        nSM, nBSM, nMIX = "reweighted_"+signal+"_htt_0PM125", "reweighted_"+signal+"_htt_0M125", "reweighted_"+signal+"_htt_0Mf05ph0125"

        hSM = file.Get(tdirName+"/"+nSM)
        hSM.SetLineColor(4)
        hMax = hSM.GetMaximum()
        hBSM = file.Get(tdirName+"/"+nBSM)
        if hBSM:# is True:
            hBSM.SetLineColor(2)
            if hMax<hBSM.GetMaximum(): hMax=hBSM.GetMaximum()
        else: printer.msg("Skip "+nBSM)
        hMIX = file.Get(tdirName+"/"+nMIX)
        if hMIX:# is True:
            hMIX.SetLineColor(8)
            if hMax<hMIX.GetMaximum(): hMax=hMIX.GetMaximum()
        else: printer.msg("Skip "+nMIX)

        ##########################
        ##     Build Canvas     ##
        ##########################
        canvasName = tdirName+"_"+signal
        if "201" not in canvasName:
          canvasName = year+"_"+canvasName
        canvas = ROOT.TCanvas(canvasName,"",0,0,1500,1100)
        canvas.cd()

        # Up pad
        upPad = ROOT.TPad("padH","padH",0,0.3,1,1)
        padMaker(upPad)
        upPad.SetBottomMargin(0)
        upPad.SetLogy()

        # draw histograms
        legend = ROOT.TLegend(.13,.70,.38,.87, "", "brNDC")

        hSM.SetMaximum(hMax*100)
        hSM.SetTitle("")
        hSM.Draw("HISTE")        

        legend.AddEntry(hSM,"SM "+signal,"l")
        if hBSM:# is True:
            hBSM.Draw("HISTESAME")
            legend.AddEntry(hBSM,"BSM "+signal,"l")
        if hMIX:# is True:
            hMIX.Draw("HISTESAME")
            legend.AddEntry(hMIX,"Int "+signal,"l")

        # Draw legend and title
        title = ROOT.TPaveText(.30,.90,0.70,.97,"NDC")
        titleString = "NONE"
        if "13TeV" in tdirName:
          titleString = cateMap[tdirName[:-5]].replace("BSM",poi)        
        else:
          cateMap[tdirName].replace("BSM",poi)
        titleStyler(title,"%s %s %s"%(year, signal, titleString)) #cateMap[tdirName[:-5]].replace("BSM",poi)))
        title.Draw("SAME")
        legend.SetLineWidth(0)
        legend.Draw("SAME")

        # Down pad
        canvas.cd()
        downPad = ROOT.TPad("padR","padR",0.,0.0,1,0.3)
        padMaker(downPad)
        downPad.SetGridy()
        downPad.SetBottomMargin(0.25)
        if not hBSM:
          printer.warning("Skip - missing BSM")
          continue
        hratioBSM = hBSM.Clone()
        hratioBSM.Divide(hSM.Clone())
        ratioStyle(hratioBSM,+1, "Bins")
        hratioBSM.SetTitle("")
        
        if hMIX:
          hratioMIX = hMIX.Clone()
          hratioMIX.Divide(hSM.Clone())
          ratioStyle(hratioMIX,-1, "Bins")
          hratioMIX.SetTitle("")

          if hratioBSM.GetMaximum()<hratioMIX.GetMaximum():
            hratioBSM.SetMaximum(hratioMIX.GetMaximum()*1.2)
          else:
            hratioBSM.SetMaximum(hratioBSM.GetMaximum()*1.2)
          if hratioBSM.GetMinimum()>hratioMIX.GetMinimum():
            hratioBSM.SetMinimum(hratioMIX.GetMinimum()*0.8)
          else:
            hratioBSM.SetMinimum(hratioBSM.GetMinimum()*0.8)

        hratioBSM.Draw("E")
        if hMIX:
          hratioMIX.Draw("SAMEE")



        canvas.Update()

        

        namepdf="signalOverlaid/"+canvasName+"_"+poi+".pdf"
        canvas.SaveAs(namepdf)
