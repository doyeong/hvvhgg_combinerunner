#!/usr/bin/env python
import ROOT
from ROOT import *

import re
from array import array
from optparse import OptionParser
import sys

#parser = OptionParser()
import argparse
parser = argparse.ArgumentParser("Compare total template to stage 1.1 templates")

parser.add_argument(
        "--inputfile",
        action="store",
        dest="inputfile",
        default="X.root",
        help="Which file1name to run over?")
parser.add_argument(
        "--Ntoss",
        action="store",
        dest="Ntoss",
        default="10",
        help="Ntoss?")
args = parser.parse_args()

inputfile =args.inputfile
Ntoss=int(args.Ntoss)

histo=ROOT.TH1F("histo","histo",15,0.4,0.7)
#histo=ROOT.TH1F("histo","histo",40,0,0.8)


# open file and fill TH1F
for i in range(1,Ntoss+1):
    inputfile=inputfile.replace("_toss_"+str(i-1),"_toss_"+str(i))
    #print " opening file %s ..."%(inputfile)
    file=ROOT.TFile(inputfile,"r")
    file.cd()
    inTree = file.Get('limit')
    for iSample in range(inTree.GetEntries()-1,inTree.GetEntries()):
        inTree.GetEntry(iSample)
         #if ( abs(inTree.deltaNLL-dN)<0.005 ) or iSample==1 or abs(inTree.CMS_zz4l_fai1)<0.04:
        dN=2.*inTree.deltaNLL
        #print '\t %s ---> saving 2*deltaNLL %s fa3_ggH %s'%(i,dN,inTree.fa3_ggH)
        histo.Fill(dN)
    file.Close()

# do Gaussian fit and RMS:
g1 = TF1("m1","gaus",0,0.8)
histo.Fit(g1)
print " %s %s %s"%(g1.GetParameter(0),g1.GetParameter(1),g1.GetParameter(2))
print " %.3f +- %.3f (%.2f%%)"%(g1.GetParameter(1),g1.GetParameter(2), g1.GetParameter(2)*100./g1.GetParameter(1))

c=ROOT.TCanvas("canvas","",0,0,600,600)
c.cd()
histo.Draw()
c.SaveAs("%s.pdf"%("plot_RMS"))
