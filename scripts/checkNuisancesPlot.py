#!/usr/bin/env python
import sys, os, re
import ROOT
import printer
from ROOT import TCanvas, TFile, TH1F
from ROOT import gROOT
from ROOT import gDirectory
from ROOT import TLine

ROOT.gStyle.SetFrameLineWidth(2)
ROOT.gStyle.SetLineWidth(1)
ROOT.gStyle.SetOptStat(0)
gROOT.SetBatch(ROOT.kTRUE)

## python scripts/checkNuisancesPlot.py shapes/USCMS/htt_tt.inputs-sm-13TeV_2018-2D_MELAVBF_2D_htt_tt_emb_sys_fa2_mergedBins.root reweighted_qqH_htt_0PM125 CMS_scale_t_1prong1pizero_2018
path = sys.argv[1]
process = sys.argv[2]
shape = sys.argv[3]

if "BSM" in process:
  process = "reweighted_"+process[:process.find("BSM")]+"_htt_0M125"
elif "SM" in process :
  process = "reweighted_"+process[:process.find("SM")]+"_htt_0PM125"
elif "Int" in process:
  process = "reweighted_"+process[:process.find("SM")]+"_htt_0Mf05ph0125"

def titleStyler(title, titletext):
  title.SetFillStyle(0)
  title.SetBorderSize(0)
  title.AddText(titletext)

def padMaker(pad):
    pad.SetFillColor(0)
    pad.Draw()
    pad.cd()

def ratioStyle(h,updown,xlable):
    h.SetLineWidth(1)
    h.SetLineColor(1)
    h.SetMarkerStyle(20)
    h.SetMarkerSize(2)
    h.GetXaxis().SetTitle(xlable) #"Visible Mass [GeV]")
    h.GetXaxis().SetTitleSize(0.11)
    h.GetXaxis().SetTitleOffset(0.7)
    h.GetXaxis().SetLabelSize(0.08)
    h.GetYaxis().SetLabelSize(0.08)
    h.GetYaxis().SetTitle("Ratio wrt nominal")
    h.GetYaxis().SetTitleSize(0.08)
    h.GetYaxis().SetTitleOffset(0.4)
    #if "BSM" in h.GetName():
    #elif "MIX" in h.GetName():
    if updown>0.0:
        h.SetMarkerColor(2)
        h.SetLineColor(2)
    elif updown<0.0:
        h.SetMarkerColor(64)
        h.SetLineColor(64)
        h.SetLineStyle(2)

cateMap={
  "htt_tt_1_13TeV":"0Jet",
  "htt_tt_2_13TeV":"Boosted",
  "htt_tt_3_13TeV":"D_BSM [0.0:0.2]",
  "htt_tt_4_13TeV":"D_BSM [0.2:0.5]",
  "htt_tt_5_13TeV":"D_BSM [0.5:0.8]",
  "htt_tt_6_13TeV":"D_BSM [0.8:1.0]",
  "tt_0jet":"0Jet",
  "tt_boosted":"Boosted",
  "tt_boosted_onejet":"Boosted Mono-jet",
  "tt_boosted_multijet":"Boosted Multi-jet",
  "tt_vbf_lowHpT":"VBF Low Higgs pT",
  "tt_vbf_highHpT":"VBF High Higgs pT",
  "tt_vbf_ggHMELA_bin1":"D_BSM [0.0:0.2]",
  "tt_vbf_ggHMELA_bin2":"D_BSM [0.2:0.5]",
  "tt_vbf_ggHMELA_bin3":"D_BSM [0.5:0.8]",
  "tt_vbf_ggHMELA_bin4":"D_BSM [0.8:1.0]",
  "tt_vbf_ggHMELA_bin1_DCPp":"D_BSM [0.0:0.2] +",
  "tt_vbf_ggHMELA_bin2_DCPp":"D_BSM [0.2:0.5] +",
  "tt_vbf_ggHMELA_bin3_DCPp":"D_BSM [0.5:0.8] +",
  "tt_vbf_ggHMELA_bin4_DCPp":"D_BSM [0.8:1.0] +",
  "tt_vbf_ggHMELA_bin1_DCPm":"D_BSM [0.0:0.2] -",
  "tt_vbf_ggHMELA_bin2_DCPm":"D_BSM [0.2:0.5] -",
  "tt_vbf_ggHMELA_bin3_DCPm":"D_BSM [0.5:0.8] -",
  "tt_vbf_ggHMELA_bin4_DCPm":"D_BSM [0.8:1.0] -",
}


# Open root file
printer.gray("Input file:\t"+path)
if os.path.isfile(path) is False: 
    printer.warning(path+ "  doesn't exist")
    sys.exit(1)
file = ROOT.TFile(path,"r")

poi = "Unknown"
if "_fa3_" in path: poi = "fa3"
elif "_fa2_" in path: poi = "fa2"
elif "_fL1_" in path: poi = "fL1"
elif "_fL1Zg_" in path: poi = "fL1Zg"
elif "_fa3ggH_" in path: poi = "fa3ggH"

# Print info
printer.info("process:\t"+process+"\t\tshape:\t"+shape+"\tPOI:"+poi)



# Loop over all TDirectories
for dir in file.GetListOfKeys(): 
    tdirName = dir.GetName() 
    printer.whiteBlueBold("\t"+tdirName+"\t")
    hNominal = file.Get("%s/%s"%(tdirName, process))
    hUp = file.Get("%s/%s_%sUp"%(tdirName, process, shape))
    hDown = file.Get("%s/%s_%sDown"%(tdirName, process, shape))

    ##########################
    ##     Build Canvas     ##
    ##########################
    canvasName = tdirName+"_"+process+"_"+shape
    canvas = ROOT.TCanvas(canvasName,"",0,0,1500,1100)
    canvas.cd()

    # Up pad
    upPad = ROOT.TPad("padH","padH",0,0.3,1,1)
    padMaker(upPad)
    upPad.SetBottomMargin(0)

    # draw histograms
    legend = ROOT.TLegend(.13,.70,.50,.87, "", "brNDC")

    if hNominal:
        printer.gray(hNominal.GetName()+"\t"+str(hNominal.Integral()))
        hNominal.SetLineColor(1)
        hNominal.SetTitle("")
        hNominal.SetMaximum(hNominal.GetMaximum()*1.5)
        hNominal.Draw("HISTE")
        legend.AddEntry(hNominal,"Nominal: "+str(hNominal.Integral()),"l")
    else:
        printer.warning("%s/%s"%(tdirName, process)+"\t doesn't exist")
        sys.exit(1)
    if hUp:
        printer.gray(hUp.GetName()+"\t"+str(hUp.Integral()))
        hUp.SetLineColor(2)
        hUp.SetLineStyle(2)
        hUp.Draw("HISTESAME")
        legend.AddEntry(hUp,"Up shape: "+str(hUp.Integral()),"l")
        if hUp.GetMaximum() > hNominal.GetMaximum():
          hNominal.SetMaximum(hUp.GetMaximum()*1.5)
    else:
        printer.warning("%s/%s_%sUp"%(tdirName, process, shape)+" \t doesn't exist")
    if hDown:
        printer.gray(hDown.GetName()+"\t"+str(hDown.Integral()))
        hDown.SetLineColor(64)
        hDown.SetLineStyle(2)
        hDown.Draw("HISTESAME")
        legend.AddEntry(hDown,"Down shape: "+str(hDown.Integral()),"l")
        if hDown.GetMaximum() > hNominal.GetMaximum():
          hNominal.SetMaximum(hDown.GetMaximum()*1.5)
    else:
        printer.warning(hNominal.GetName()+" Down\t doesn't exist")
    
    # Draw legend and title
    title = ROOT.TPaveText(.30,.90,0.70,.97,"NDC")
    titleString = cateMap[tdirName].replace("BSM",poi)
    titleStyler(title,titleString+" "+ shape)
    title.Draw("SAME")
    legend.SetLineWidth(0)
    legend.Draw("SAME")

    # Down pad
    canvas.cd()
    downPad = ROOT.TPad("padR","padR",0.,0.0,1,0.3)
    padMaker(downPad)
    downPad.SetGridy()
    downPad.SetBottomMargin(0.25)
    
    if hUp:
      hRUp = hUp.Clone()
      hRUp.Divide(hNominal.Clone())
      ratioStyle(hRUp,+1, "Bins")
      hRUp.SetTitle("")
    if hDown:
      hRDown = hDown.Clone()
      hRDown.Divide(hNominal.Clone())
      ratioStyle(hRDown, -1, "Bins")

    if hUp and hDown:
      #if hRUp.GetMaximum()<hRDown.GetMaximum():
      hRUp.SetMaximum(max([hRUp.GetMaximum(),hRDown.GetMaximum()])*1.1)
      #if hRUp.GetMinimum()>hRDown.GetMinimum():
      hRUp.SetMinimum(min([hRUp.GetMinimum(),hRDown.GetMinimum()])*0.9)
      hRUp.Draw("E")      
      hRDown.Draw("ESAME")

    l = TLine(0, 1, hRUp.GetNbinsX(), 1)
    l.SetLineColor(91)
    l.Draw("same")

    canvas.Update()
    
    namepdf="checkNuisances/"+canvasName+"_"+poi+".pdf"
    canvas.SaveAs(namepdf)

printer.red("\n\nimgcat checkNuisances/*"+process+"_"+shape+"_"+poi+".pdf\n\n")
