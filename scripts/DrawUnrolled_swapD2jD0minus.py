#!/usr/bin/env python
import ROOT
import re, sys, os
import math
from array import array
from collections import OrderedDict
import argparse
from ROOT import TMath, TLine
import printer

parser = argparse.ArgumentParser(
    "Create pre/post-fit plots for aHTT")
parser.add_argument(
    "--channel",
    action="store",
    dest="channel",
    default="tt",
    help="Which channel to run over? (et, mt, em, tt)")
parser.add_argument(
    "--year",
    action="store",
    dest="year",
    default="2016",
    help="Which year to run over? (2016,2017,2018)")
parser.add_argument(
    "--catN",
    action="store",
    dest="catN",
    default="3",
    help="Which year to run over? (2016,2017,2018)")
parser.add_argument(
    "--NN",
    action="store",
    dest="NNbinNum",
    default="8",
    help="Which year to run over? (2016,2017,2018)")
parser.add_argument(
    "--variable",
    action="store",
    dest="variable",
    default="L1",
    help="Provide the relative path to the target input file")
parser.add_argument(
    "--doLog",
    action="store",
    dest="doLog",
    default="1",
    help="Provide the relative path to the target input file")
parser.add_argument(
    "--isEmbed",
    action="store",
    dest="isEmbed",
    default="1",
    help="Provide the relative path to the target input file")
parser.add_argument(
    "--isPrefit",
    action="store_true",
    dest="isPrefit",
    default=False,
    help="draw prefit plots if this is true")
args = parser.parse_args()


def add_lumi(lumi_x):
    lowX=0.7
    lowY=0.835
    lumi  = ROOT.TPaveText(lowX, lowY+0.06, lowX+0.30, lowY+0.16, "NDC")
    lumi.SetBorderSize(   0 )
    lumi.SetFillStyle(    0 )
    lumi.SetTextAlign(   12 )
    lumi.SetTextColor(    1 )
    lumi.SetTextSize(0.06)
    lumi.SetTextFont (   42 )
    #lumi.AddText("35.9 fb^{-1} (13 TeV)")
    lumi.AddText(str(lumi_x)+" fb^{-1} (13 TeV)")
    return lumi

def add_CMS():
    lowX=0.11
    lowY=0.835
    lumi  = ROOT.TPaveText(lowX, lowY+0.06, lowX+0.15, lowY+0.16, "NDC")
    lumi.SetTextFont(61)
    lumi.SetTextSize(0.08)
    lumi.SetBorderSize(   0 )
    lumi.SetFillStyle(    0 )
    lumi.SetTextAlign(   12 )
    lumi.SetTextColor(    1 )
    lumi.AddText("CMS")
    return lumi

def add_Preliminary():
    lowX=0.35
    lowY=0.835
    lumi  = ROOT.TPaveText(lowX, lowY+0.06, lowX+0.15, lowY+0.16, "NDC")
    lumi.SetTextFont(52)
    lumi.SetTextSize(0.06)
    lumi.SetBorderSize(   0 )
    lumi.SetFillStyle(    0 )
    lumi.SetTextAlign(   12 )
    lumi.SetTextColor(    1 )
    lumi.AddText("Preliminary")
    return lumi

def make_legend(dx):
    #output = ROOT.TLegend(0.85+dx, 0.25, 0.99, 0.9, "", "brNDC")
    output = ROOT.TLegend(0.85+dx, 0.38, 0.98, 0.98, "", "brNDC")
    output.SetLineWidth(0)
    output.SetLineStyle(0)
    output.SetFillColor(0)
    output.SetBorderSize(0)
    output.SetTextFont(62)
    return output
    
def make_legend_2(dx, dy):
    #output = ROOT.TLegend(0.85+dx, 0.0, 0.99, 0.24, "", "brNDC")
    output = ROOT.TLegend(0.85+dx, dy, 0.99, 0.37, "", "brNDC")
    output.SetLineWidth(0)
    output.SetLineStyle(0)
    output.SetFillColor(0)
    output.SetBorderSize(0)
    output.SetTextFont(62)
    if dy>0:
        output.SetTextFont(63)
    return output

    
ROOT.gStyle.SetFrameLineWidth(2)
ROOT.gStyle.SetLineWidth(2)
ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(True)

c=ROOT.TCanvas("canvas","",0,0,1200,600)
c.cd()
if (args.doLog=="1"):
    c.SetLogy()
    
hdfspath="" #/hdfs/store/user/doyeong/output_Oct02/unrolledInputs/"
#myfileStr = hdfspath+"output_shapes_f%s_%s_%s.root"%(args.variable, args.channel, args.year)
myfileStr = hdfspath+"output_shapes_f%s_%s_Run2.root"%(args.variable, args.channel)
file2Str= hdfspath+"htt_input_f%s_%s_%s.root"%(args.variable, args.channel, args.year)
printer.info("myfile : "+myfileStr)
if os.path.isfile(myfileStr) is False:
    printer.warning(myfileStr + "is not available. ")
    sys.exit(0)
printer.info("file2 "+file2Str)
if os.path.isfile(file2Str) is False:
    printer.warning(file2Str + "is not available. ")
    sys.exit(0)
myfile=ROOT.TFile(myfileStr,"read")
file2=ROOT.TFile(file2Str,"read")

if args.variable=="CP":
   myfile=ROOT.TFile("output_shapes_fa3.root","read")
   file2=ROOT.TFile("htt_input_fa3.root","read")

adapt=ROOT.gROOT.GetColor(12)
new_idx=ROOT.gROOT.GetListOfColors().GetSize() + 1
trans=ROOT.TColor(new_idx, adapt.GetRed(), adapt.GetGreen(),adapt.GetBlue(), "",0.4)

catNmap = {
    "3":"D_{2jet} [0.0,0.3], D_{CP}^{ggH}<0",
    "4":"D_{2jet} [0.0,0.3], D_{CP}^{ggH}>0 ",
    "5":"D_{2jet} [0.3,0.7], D_{CP}^{ggH}<0",
    "6":"D_{2jet} [0.3,0.7], D_{CP}^{ggH}>0",
    "7":"D_{2jet} [0.7,1.0], D_{CP}^{ggH}<0",
    "8":"D_{2jet} [0.7,1.0], D_{CP}^{ggH}>0",
}

catN=args.catN
categ=[
    "htt_"+args.channel+"_"+catN+"_13TeV_"+args.year,
]

if "2016" in args.year:
    if catN=="3" or catN=="4":
        binningNN = [0.0, 0.05, 0.5, 0.75, 1.0]
        binningDBSM = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    elif catN=="5" or catN=="6":
        binningNN = [0.0, 0.01, 0.05, 0.1, 0.2, 0.4, 0.7, 1.0]
        binningDBSM = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    elif catN=="7" or catN=="8":
        binningNN = [0.0, 0.05, 0.1, 0.2, 0.3, 0.5, 1.0]
        binningDBSM = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0]
elif "2017" in args.year:
    if catN=="3" or catN=="4":
        binningNN = [0.0, 0.05, 0.5, 0.75, 1.0]
        binningDBSM = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    elif catN=="5" or catN=="6":
        binningNN = [0.0, 0.1, 0.3, 0.4, 0.5, 0.625, 0.75, 0.875, 1.0]
        binningDBSM = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    elif catN=="7" or catN=="8":
        binningNN = [0.0,0.1,0.2,0.3,0.6,1.0] #0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 1.0]
        binningDBSM = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0]
elif "2018" in args.year:
    if catN=="3" or catN=="4":
        binningNN = [0.0, 0.05, 0.5, 0.75, 1.0]
        binningDBSM = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    elif catN=="5" or catN=="6":
        binningNN = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.8, 1.0]
        binningDBSM = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    elif catN=="7" or catN=="8":
        binningNN = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.625, 1.0]
        binningDBSM = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]

# HERE
nnbinnum = len(binningNN)-1 #int(args.NNbinNum)
DBSMbinnum = len(binningDBSM)-1
binnum=int(DBSMbinnum*nnbinnum)
printer.red("#NNbin: %.0f, #DBSMbin: %.0f, #total: %.0f"%(nnbinnum, DBSMbinnum, binnum))
#if args.year!="2018" and (catN=="7" or catN=="8"): binnum=int(5*nnbinnum)
hist_ZTT_em=ROOT.TH1F("hist_ZTT_em","hist_ZTT_em",binnum,0,binnum)
hist_ZL_em=ROOT.TH1F("hist_ZL_em","hist_ZL_em",binnum,0,binnum)
hist_TT_em=ROOT.TH1F("hist_TT_em","hist_TT_em",binnum,0,binnum)
hist_W_em=ROOT.TH1F("hist_W_em","hist_W_em",binnum,0,binnum)
hist_QCD_em=ROOT.TH1F("hist_QCD_em","hist_QCD_em",binnum,0,binnum)
hist_others_em=ROOT.TH1F("hist_others_em","hist_others_em",binnum,0,binnum)
hist_D_em=ROOT.TH1F("hist_D_em","hist_D_em",binnum,0,binnum)
hist_B_em=ROOT.TH1F("hist_B_em","hist_B_em",binnum,0,binnum)
hist_S_em=ROOT.TH1F("hist_S_em","hist_S_em",binnum,0,binnum)
hist_SM_em=ROOT.TH1F("hist_SM_em","hist_SM_em",binnum,0,binnum)
hist_SM_other_em=ROOT.TH1F("hist_SM_other_em","hist_SM_other_em",binnum,0,binnum)
hist_BSM_em=ROOT.TH1F("hist_BSM_em","hist_BSM_em",binnum,0,binnum)

data_yield=0.

couplingMap = {
    "a3":"0PM",
    "a1":"0M",
    "a2":"0PH",
    "L1":"0L1",
    "L1Zg":"0L1Zg"
}

printer.gray("Start loop!")
for mycat in categ:
    print " cat: ",mycat
    mycat2=mycat.replace("_13TeV_","_")
    preORpost = "_postfit" if args.isPrefit is False else "_prefit"
    print preORpost
    SM=myfile.Get(mycat+preORpost).Get("reweighted_ggH_htt_0PM")
    MIX=myfile.Get(mycat+preORpost).Get("reweighted_ggH_htt_0Mf05ph0")
    BSM=myfile.Get(mycat+preORpost).Get("reweighted_ggH_htt_0M")
    SM_other=myfile.Get(mycat+preORpost).Get("reweighted_qqH_htt_0PM")
    BSM_other=myfile.Get(mycat+preORpost).Get("reweighted_qqH_htt_0M")
    MIX_other=myfile.Get(mycat+preORpost).Get("reweighted_qqH_htt_0Mf05ph0")
    ###
    TotalSignal=SM.Clone()
    TotalSignal.Add(BSM)
    TotalSignal.Add(MIX)
    TotalSignalOther=SM_other.Clone()
    TotalSignalOther.Add(BSM_other)
    TotalSignalOther.Add(MIX_other)
    ###
    
    printer.gray(mycat+preORpost)
    B=myfile.Get(mycat+preORpost).Get("TotalBkg")

    ZTT=myfile.Get(mycat+preORpost).Get("embedded")
    ZL=myfile.Get(mycat+preORpost).Get("ZL")
    if myfile.Get(mycat+preORpost).Get("ZJ") and ZL:
        ZL.Add(myfile.Get(mycat+preORpost).Get("ZJ"))
    if not ZL:
        if args.channel=="em":
            ZL=myfile.Get(mycat+preORpost).Get("ZLL")
        else:
            ZL=myfile.Get(mycat+preORpost).Get("ZJ")
    TT=myfile.Get(mycat+preORpost).Get("TT")
    if not TT and args.isEmbed=="0":
        TT=myfile.Get(mycat+preORpost).Get("TTT")
        if myfile.Get(mycat+preORpost).Get("TTL"):
            TT.Add(myfile.Get(mycat+preORpost).Get("TTL"))
        if myfile.Get(mycat+preORpost).Get("TTJ"):
            TT.Add(myfile.Get(mycat+preORpost).Get("TTJ"))
      
    VV=myfile.Get(mycat+preORpost).Get("VV")
    if not VV and args.isEmbed=="0":
        VV=myfile.Get(mycat+preORpost).Get("VVT")
        if myfile.Get(mycat+preORpost).Get("VVJ"):
            VV.Add(myfile.Get(mycat+preORpost).Get("VVJ"))
    EWKZ=myfile.Get(mycat+preORpost).Get("EWKZ")
    ggHWW=myfile.Get(mycat+preORpost).Get("ggH_hww125")
    qqHWW=myfile.Get(mycat+preORpost).Get("qqH_hww125")
    WHWW=myfile.Get(mycat+preORpost).Get("WH_hww125")
    ZHWW=myfile.Get(mycat+preORpost).Get("ZH_hww125")
    if args.isEmbed=="1" and args.channel=="tt":
        TT=myfile.Get(mycat+preORpost).Get("TTL")
        print " getting VVL..."
        VV=myfile.Get(mycat+preORpost).Get("VVL")
    if args.isEmbed=="1" and (args.channel=="mt" or args.channel=="et"):
        TT=myfile.Get(mycat+preORpost).Get("TTT")
        print " getting VVL..."
        VV=myfile.Get(mycat+preORpost).Get("VVT")

    if ggHWW:
        if qqHWW: ggHWW.Add(myfile.Get(mycat+preORpost).Get("qqH_hww125"))
        if WHWW: ggHWW.Add(myfile.Get(mycat+preORpost).Get("WH_hww125"))
        if ZHWW: ggHWW.Add(myfile.Get(mycat+preORpost).Get("ZH_hww125"))
        HWW = ggHWW.Clone()
    elif qqHWW:
        if WHWW: qqHWW.Add(myfile.Get(mycat+preORpost).Get("WH_hww125"))
        if ZHWW: qqHWW.Add(myfile.Get(mycat+preORpost).Get("ZH_hww125"))
        HWW = qqHWW.Clone()
    elif WHWW:
        if ZHWW: qqHWW.Add(myfile.Get(mycat+preORpost).Get("ZH_hww125"))
        HWW = WHWW.Clone()
    elif ZHWW:
        HWW = ZHWW.Clone()

    W=myfile.Get(mycat+preORpost).Get("W")
    QCD=myfile.Get(mycat+preORpost).Get("jetFakes")
    if args.channel=="em":
        QCD=myfile.Get(mycat+preORpost).Get("QCD")
    S=myfile.Get(mycat+preORpost).Get("TotalSig")
    D=myfile.Get(mycat+preORpost).Get("data_obs")
    factor=1

    for j in range(1,B.GetSize()-1):
        
        #HERE
        bin = int(j) + 12*(factor-1)
        if args.channel=="et" or args.channel=="tt":
            #bin = int(j) + 28*(factor-1)
            bin = int(j) + 6*(factor-1)
        if args.channel=="em":
            #bin = int(j) + 28*(factor-1)
            bin = int(j) + 16*(factor-1)
        if args.channel=="mt"  or (args.channel=="et" and args.year=="2016"):
            #bin = int(j) + 28*(factor-1)
            bin = int(j) + 8*(factor-1)

        myweight=1.0


        hist_ZTT_em.SetBinContent(bin,hist_ZTT_em.GetBinContent(bin)+ZTT.GetBinContent(j)*myweight)

        if args.isEmbed=="0":
            hist_ZTT_em.SetBinContent(bin,hist_ZTT_em.GetBinContent(bin)+EWKZ.GetBinContent(j)*myweight)

        if TT:
            hist_TT_em.SetBinContent(bin,hist_TT_em.GetBinContent(bin)+TT.GetBinContent(j)*myweight)
       
        if QCD:
            hist_QCD_em.SetBinContent(bin,hist_QCD_em.GetBinContent(bin)+QCD.GetBinContent(j)*myweight)
       
        if ZL:
            hist_ZL_em.SetBinContent(bin,hist_ZL_em.GetBinContent(bin)+ZL.GetBinContent(j)*myweight)
        if VV:
            hist_others_em.SetBinContent(bin,hist_others_em.GetBinContent(bin)+VV.GetBinContent(j)*myweight)
        if EWKZ:
            hist_others_em.SetBinContent(bin,hist_others_em.GetBinContent(bin)+EWKZ.GetBinContent(j)*myweight)

        if HWW:
            hist_others_em.SetBinContent(bin,hist_others_em.GetBinContent(bin)+HWW.GetBinContent(j)*myweight)
        if W:
            hist_W_em.SetBinContent(bin,hist_W_em.GetBinContent(bin)+W.GetBinContent(j)*myweight)

        hist_D_em.SetBinContent(bin,hist_D_em.GetBinContent(bin)+D.GetBinContent(j)*myweight)
        if hist_D_em.GetBinContent(bin)>0:	
            hist_D_em.SetBinError(bin,((hist_D_em.GetBinContent(bin)*myweight)**0.5))


        hist_B_em.SetBinError(bin,(hist_B_em.GetBinError(bin)*hist_B_em.GetBinError(bin)+B.GetBinError(j)*B.GetBinError(j)*myweight*myweight)**0.5)
        #hist_B_em.SetBinError(bin,hist_B_em.GetBinError(bin)+B.GetBinError(j)*myweight)
        hist_B_em.SetBinContent(bin,hist_B_em.GetBinContent(bin)+B.GetBinContent(j)*myweight)

        hist_B_em.SetBinError(bin,(hist_B_em.GetBinError(bin)*hist_B_em.GetBinError(bin)+S.GetBinError(j)*S.GetBinError(j)*myweight*myweight)**0.5)
        #hist_B_em.SetBinError(bin,hist_B_em.GetBinError(bin)+S.GetBinError(j)*myweight)
        hist_B_em.SetBinContent(bin,hist_B_em.GetBinContent(bin)+S.GetBinContent(j)*myweight)
       
        hist_S_em.SetBinContent(bin,hist_S_em.GetBinContent(bin)+S.GetBinContent(j)*myweight)
        
        hist_SM_em.SetBinContent(bin,hist_SM_em.GetBinContent(bin)+SM.GetBinContent(j)*myweight)
        hist_SM_other_em.SetBinContent(bin,hist_SM_other_em.GetBinContent(bin)+SM_other.GetBinContent(j)*myweight)
        #printer.red(hist_BSM_em.GetName())
        hist_BSM_em.SetBinContent(bin,hist_BSM_em.GetBinContent(bin)+BSM.GetBinContent(j)*myweight)

        # do data blinding: if data is non-zero, S/sqrt(B+0.09B^2)>0.3
        data_yield=hist_D_em.Integral()
        if hist_B_em.GetBinContent(bin) > 0 and (hist_SM_em.GetBinContent(bin)+hist_BSM_em.GetBinContent(bin))/ TMath.Sqrt(hist_B_em.GetBinContent(bin) + 0.09*0.09*hist_B_em.GetBinContent(bin)*hist_B_em.GetBinContent(bin)) >= 0.3:

            if args.isPrefit is True: 
                hist_D_em.SetBinContent(bin, -100)
                printer.red(str(bin)+" is blinded\tB:"+str(hist_B_em.GetBinContent(bin))+"\tSM:"+str(hist_SM_em.GetBinContent(bin))+"\tBSM:"+str(hist_BSM_em.GetBinContent(bin)))

err_B=ROOT.Double(0)
err_S=ROOT.Double(0)

c.cd()

pad1 = ROOT.TPad("pad1","pad1",0,0.35,1,1)
pad1.Draw()
pad1.cd()
pad1.SetFillColor(0)
pad1.SetBorderMode(0)
pad1.SetBorderSize(10)
pad1.SetTickx(1)
pad1.SetTicky(1)
#pad1.SetLeftMargin(0.18)
pad1.SetRightMargin(0.15)
pad1.SetTopMargin(0.122)
pad1.SetBottomMargin(0.026)
pad1.SetFrameFillStyle(0)
pad1.SetFrameLineStyle(0)
pad1.SetFrameLineWidth(3)
pad1.SetFrameBorderMode(0)
pad1.SetFrameBorderSize(10)
if (args.doLog=="1"):
    pad1.SetLogy()

hist_D_em.GetXaxis().SetLabelSize(0)
hist_D_em.GetXaxis().SetTitle("")
hist_D_em.GetXaxis().SetTitleSize(0.06)
hist_D_em.GetXaxis().SetNdivisions(505)
hist_D_em.GetYaxis().SetLabelFont(42)
hist_D_em.GetYaxis().SetLabelOffset(0.01)
hist_D_em.GetYaxis().SetLabelSize(0.06)
hist_D_em.GetYaxis().SetTitleSize(0.085)
hist_D_em.GetYaxis().SetTitleOffset(0.6)
hist_D_em.GetYaxis().SetTitle("Events/bin")
hist_D_em.GetYaxis().SetTickLength(0.012)

hist_ZTT_em.SetFillColor(ROOT.TColor.GetColor("#ffcc66"))
hist_ZTT_em.SetLineColor(ROOT.TColor.GetColor("#ffcc66"))
hist_ZL_em.SetFillColor(ROOT.TColor.GetColor("#4496c8"))
hist_ZL_em.SetLineColor(ROOT.TColor.GetColor("#4496c8"))
hist_TT_em.SetFillColor(ROOT.TColor.GetColor("#9999cc"))
hist_TT_em.SetLineColor(ROOT.TColor.GetColor("#9999cc"))
hist_QCD_em.SetFillColor(ROOT.TColor.GetColor("#ffccff"))
hist_QCD_em.SetLineColor(ROOT.TColor.GetColor("#ffccff"))
hist_W_em.SetFillColor(ROOT.TColor.GetColor("#a53db8"))
hist_W_em.SetLineColor(ROOT.TColor.GetColor("#a53db8"))
hist_others_em.SetFillColor(ROOT.TColor.GetColor("#12cadd"))
hist_others_em.SetLineColor(ROOT.TColor.GetColor("#12cadd"))
hist_S_em.SetLineColor(0)
hist_S_em.SetLineWidth(2)
hist_S_em.SetFillColor(2)

TotalSignal.SetLineColor(ROOT.EColor(ROOT.kRed))
TotalSignal.SetLineWidth(3)
hist_SM_em.SetLineColor(ROOT.EColor(ROOT.kRed))
hist_SM_em.SetLineWidth(3)
#hist_SM_other_em.SetLineColor(28)
TotalSignalOther.SetLineColor(ROOT.EColor(ROOT.kBlack))
TotalSignalOther.SetLineWidth(3)
hist_SM_other_em.SetLineColor(ROOT.EColor(ROOT.kMagenta+1))
hist_SM_other_em.SetLineWidth(2)
hist_SM_other_em.SetLineStyle(2)
hist_BSM_em.SetLineColor(ROOT.EColor(ROOT.kBlack))
hist_BSM_em.SetLineWidth(3)

acScale=100
hist_SM_em.Scale(acScale)
hist_SM_other_em.Scale(acScale)
hist_BSM_em.Scale(acScale)
TotalSignal.Scale(acScale)
TotalSignalOther.Scale(acScale)
hist_D_em.SetLineColor(1)
errorBand=hist_B_em.Clone()
errorBand.SetMarkerSize(0)
errorBand.SetFillColor(new_idx)
errorBand.SetLineColor(1)
errorBand.SetLineWidth(1)
#histS.SetLineColor(2)
#histS.SetLineWidth(2)
mystack=ROOT.THStack("mystack","mystack")
mystack.Add(hist_others_em)
mystack.Add(hist_W_em)
mystack.Add(hist_QCD_em)
mystack.Add(hist_TT_em)
mystack.Add(hist_ZL_em)
mystack.Add(hist_ZTT_em)
mystack.Add(hist_S_em)
yield_stack=0
for hist in mystack.GetHists():
    yield_stack=yield_stack+hist.Integral()
    print " histo %s yield: %s -> sum %s "%(hist.GetName(),hist.Integral(),yield_stack)


# as if hist_B_em does include the signal!
print "\nYIELD CHECK: yield all mystack: %s, total S(%s)+B(%s): %s, ==> ratio %s "%(yield_stack,hist_S_em.Integral(),hist_B_em.Integral()-hist_S_em.Integral(), hist_B_em.Integral(),yield_stack/(hist_B_em.Integral()))
print  " YIELD CHECK: yield data: %s ==> ratio to mystack %s, to S+B %s "%(data_yield, data_yield/yield_stack,data_yield/(hist_B_em.Integral()))


hist_D_em.SetMarkerStyle(20)
hist_D_em.Draw("e0pX0")
hist_D_em.SetTitle("")
hist_D_em.SetMinimum(0.009)
if args.channel=="et":
	hist_D_em.SetMinimum(0.5)
if args.channel=="em":
	hist_D_em.SetMinimum(0.05)

if (args.doLog=="1"):
    hist_D_em.SetMaximum(10000*hist_B_em.GetMaximum())
if (args.doLog=="1" and args.channel=="em"):
    hist_D_em.SetMaximum(1000000*hist_B_em.GetMaximum())
#hist_D_em.SetMaximum(max(1.2*hist_B_em.GetMaximum(),2.15*hist_D_em.GetMaximum()))
mystack.Draw("histsame")
errorBand.Draw("e2same")
#hist_SM_em.Draw("histsame")
#hist_SM_other_em.Draw("histsame")
#hist_BSM_em.Draw("histsame")
TotalSignal.Draw("histsame")
TotalSignalOther.Draw("histsame")
hist_D_em.Draw("e0pX0same")
legend=make_legend(0.0)
legend.AddEntry(hist_D_em,"Observed","elp")
legend.AddEntry(hist_S_em,"H#rightarrow#tau#tau","f")
legend.AddEntry(hist_ZTT_em,"Z#rightarrow#tau#tau","f")
legend.AddEntry(hist_ZL_em,"Z#rightarrow#mu#mu/ee","f")
legend.AddEntry(hist_TT_em,"t#bar{t}+jets","f")
legend.AddEntry(hist_QCD_em,"QCD multijet","f")
legend.AddEntry(hist_W_em,"W+jets","f")
legend.AddEntry(hist_others_em,"Others","f")
legend.AddEntry(errorBand,"Total unc.","f")
legend.Draw()

legend2=make_legend_2(0.0, 0.0) if acScale!=1 else make_legend_2(0.0, 0.15)
if acScale!=1:
    if args.isPrefit == False:
        legend2.AddEntry(TotalSignal,"ggH#rightarrow#tau#tau (X"+str(acScale)+")","l")
        legend2.AddEntry(TotalSignalOther,"VBF H#rightarrow#tau#tau (X"+str(acScale)+")","l")
    else:
        legend2.AddEntry(TotalSignal,"#splitline{SM ggH H#rightarrow#tau#tau}{(X"+str(acScale)+")}","l")
        legend2.AddEntry(TotalSignalOther,"#splitline{SM VBF H#rightarrow#tau#tau}{(X"+str(acScale)+")}","l")

    #legend2.AddEntry(hist_SM_em,"#splitline{SM ggH H#rightarrow#tau#tau}{(#sigma = "+str(acScale)+" #sigma_{SM})}","l")
    #legend2.AddEntry(hist_BSM_em,"#splitline{BSM ggH H#rightarrow#tau#tau}{(#sigma = "+str(acScale)+" #sigma_{SM})}","l")
    #legend2.AddEntry(hist_SM_other_em,"#splitline{SM VBF H#rightarrow#tau#tau}{(#sigma = "+str(acScale)+" #sigma_{SM})}","l")
else:
    legend2.AddEntry(hist_SM_em,"SM ggH H#rightarrow#tau#tau","l")
    legend2.AddEntry(hist_BSM_em,"BSM ggH H#rightarrow#tau#tau","l")
    legend2.AddEntry(hist_SM_other_em,"SM VBF H#rightarrow#tau#tau","l")
legend2.Draw()


if args.year=="2016":
    lumi_x=35.9
elif args.year=="2017":
    lumi_x=41.5
elif args.year=="2018":
    lumi_x=59.5
else:
    lumi_x=137

l1=add_lumi(lumi_x)
l1.Draw("same")
l2=add_CMS()
l2.Draw("same")
#l3=add_Preliminary()
#l3.Draw("same")

myvariable="D_{#Lambda1}"
if args.variable=="a3":
   myvariable="D_{0-}"
if args.variable=="a3ggH":
   myvariable="D_{NN}" #{0-}^{ggH}"
if args.variable=="a2":
   myvariable="D_{0h+}"
if args.variable=="L1Zg":
   myvariable="D_{#Lambda1}^{Z#gamma}"

line2=[]
label2=[]


if "tt" in args.channel:
    nx, ny =nnbinnum, DBSMbinnum
    tlines=[]
    labels=[]
    text=[]
    if binnum/DBSMbinnum==4: textOffset = 0.19
    if binnum/DBSMbinnum==5: textOffset = 0.15
    if binnum/DBSMbinnum==6: textOffset = 0.125
    if binnum/DBSMbinnum==7: textOffset = 0.107
    if binnum/DBSMbinnum==8: textOffset = 0.094
    for slice in range(0,binnum/DBSMbinnum):
        whereX=DBSMbinnum*slice
        l = TLine(whereX, 0, whereX, hist_D_em.GetMaximum())
        l.SetLineStyle(3)
        tlines.append(l)
        labels.append(ROOT.TPaveText(0.12+slice*textOffset, 0.33, 0.12+slice*textOffset+0.04, 0.6, "NDC"))
        SliceLabel = "%.2f,%.2f"%(binningNN[slice],binningNN[slice+1])
        text.append(labels[slice].AddText(myvariable+" #in ["+SliceLabel+"]"))
        
    for tline in tlines:
        tline.SetLineStyle(3)
        tline.Draw()
        labels[tlines.index(tline)].SetBorderSize(   0 )
        labels[tlines.index(tline)].SetFillStyle(    0 )
        labels[tlines.index(tline)].SetTextAlign(   12 )
        labels[tlines.index(tline)].SetTextSize ( 0.05 )
        labels[tlines.index(tline)].SetTextColor(    4 )
        labels[tlines.index(tline)].SetTextFont (   42 )
        text[tlines.index(tline)].SetTextAngle(90)
        labels[tlines.index(tline)].Draw("same")

line=[]
label=[]
text=[]
if 1>0:
   nx=12 # 16->12
   ny=2
   for z in range(1, nx+1):
       line.append(ROOT.TLine(z*ny,0,z*ny,0.085*hist_D_em.GetMaximum()))
       line[z-1].SetLineStyle(5)
       line[z-1].SetLineColor(4)
       #line[z-1].Draw("same")
       posx=0.109+0.75*(z-1)/nx
       label.append(ROOT.TPaveText(posx, 0.33, posx+0.15, 0.5, "NDC"))

       text.append(label[z-1].AddText("NN_{disc} #in [0,0.4]"))
       if (int(z-1)%3==1):
          label[z-1]=ROOT.TPaveText(posx, 0.33, posx+0.15, 0.42, "NDC")
	  #if args.year=="2016" and args.variable=="a3ggH":
          #	text[z-1]=label[z-1].AddText("NN_{disc} #in [0.4,0.6]")
          #else:
          text[z-1]=label[z-1].AddText("NN_{disc} #in [0.4,0.7]")
       if (int(z-1)%3==2):
          label[z-1]=ROOT.TPaveText(posx, 0.33, posx+0.15, 0.42, "NDC")
          text[z-1]=label[z-1].AddText("NN_{disc} #in [0.7,1.0]")
       #elif (int(z-1)%4==3):
       #   label[z-1]=ROOT.TPaveText(posx, 0.33, posx+0.15, 0.58, "NDC")
       #   #text[z-1]=label[z-1].AddText("NN_{disc} > 1100 GeV")
       #   text[z-1]=label[z-1].AddText("NN_{disc} #in [0.7,1.0]")

       label[z-1].SetBorderSize(   0 )
       label[z-1].SetFillStyle(    0 )
       label[z-1].SetTextAlign(   12 )
       label[z-1].SetTextSize ( 0.05 )
       label[z-1].SetTextColor(    4 )
       label[z-1].SetTextFont (   42 )
       text[z-1].SetTextAngle(90)
       #label[z-1].Draw("same")



pad1.RedrawAxis()

categ  = ROOT.TPaveText(0.33, 0.895, 0.63, 0.99, "NDC")
categ.SetBorderSize(   0 )
categ.SetFillStyle(    0 )
categ.SetTextAlign(   12 )
categ.SetTextSize ( 0.08 )
categ.SetTextColor(    1 )
categ.SetTextFont (   42 )
if args.channel=="em":
    categ.AddText("VBF, e#mu")
if args.channel=="et":
    categ.AddText("VBF, e#tau_{h}")
if args.channel=="mt":
    categ.AddText("VBF, #mu#tau_{h}")
if args.channel=="tt":
    categ.AddText("VBF, #tau_{h}#tau_{h}   "+catNmap[catN])
if args.channel=="all":
    categ.AddText("All #tau#tau")
if args.channel=="emetmt":
    categ.AddText("VBF, e#mu + e#tau_{h} + #mu#tau_{h}")
categ.Draw("same")

c.cd()
pad2 = ROOT.TPad("pad2","pad2",0,0,1,0.35);
pad2.SetTopMargin(0.05);
pad2.SetBottomMargin(0.35);
#pad2.SetLeftMargin(0.18);
pad2.SetRightMargin(0.15);
pad2.SetTickx(1)
pad2.SetTicky(1)
pad2.SetFrameLineWidth(3)
#pad2.SetGridx()
pad2.SetGridy()
pad2.Draw()
pad2.cd()
h1=hist_D_em.Clone()
h1.SetMaximum(1.8)
h1.SetMinimum(0.2)
h1.SetMarkerStyle(20)
h3=errorBand.Clone()
hwoE=errorBand.Clone()
for iii in range (1,hwoE.GetSize()-2):
  hwoE.SetBinError(iii,0)
h3.Sumw2()
h1.Sumw2()
h1.SetStats(0)
h1.Divide(hwoE)
h3.Divide(hwoE)
h1.GetXaxis().SetTitle("m_{#tau#tau} (GeV)")
h1.GetXaxis().SetTitle("")
h1.GetYaxis().SetLabelSize(0.08)
h1.GetYaxis().SetTitle("Obs./Exp.")
h1.GetXaxis().SetNdivisions(505)
h1.GetYaxis().SetNdivisions(5)

h1.GetXaxis().SetTitleSize(0.15)
h1.GetYaxis().SetTitleSize(0.15)
h1.GetYaxis().SetTitleOffset(0.3)
h1.GetXaxis().SetTitleOffset(1.04)
h1.GetXaxis().SetLabelSize(0.11)
h1.GetYaxis().SetLabelSize(0.11)
h1.GetXaxis().SetTitleFont(42)
h1.GetYaxis().SetTitleFont(42)

h1.LabelsOption("v","X")
h1.GetXaxis().SetLabelOffset(0.02)
h1.GetXaxis().SetLabelSize(0.06)
if (((args.channel == "et" or args.year!="2016") or args.channel == "tt") and (args.channel != "em") and (args.channel == "et" and args.year!="2016") or args.channel == "tt"):
   print " \t ========>  this is et/tt!!! "
   strD2jetBin1="0-0.75"
   strD2jetBin2="0.75-1"
   if args.variable=="a3ggH":
       strD2jetBin=[
           "0.0-0.1",
           "0.1-0.2",
           "0.2-0.3",
           "0.3-0.4",
           "0.4-0.5",
           "0.5-0.6",
           "0.6-0.7",
           "0.7-0.8",
           "0.8-0.9",
           "0.9-1.0"
       ]

       strD2jetBin2=[
           "0.0-0.2",
           "0.2-0.4",
           "0.4-0.6",
           "0.6-0.8",
           "0.8-1.0",
       ]
   if len(binningDBSM)-1 == 5: #args.year!="2018" and catN=="5":
       for idx in range(1,h1.GetNbinsX()+1):
           h1.GetXaxis().SetBinLabel(idx,strD2jetBin2[(idx%len(strD2jetBin2))-1])
   else:
       for idx in range(1,h1.GetNbinsX()+1):
           h1.GetXaxis().SetBinLabel(idx,strD2jetBin[(idx%len(strD2jetBin))-1])    
h1.GetXaxis().SetLabelSize(0.07)
h1.GetYaxis().SetLabelSize(0.08)
h1.GetYaxis().SetTickLength(0.012)
h1.GetYaxis().SetNdivisions(5)

h1.GetXaxis().SetLabelSize(0.13)
if args.channel=="tt":
   #h1.GetXaxis().SetLabelSize(0.08)
   h1.GetXaxis().SetLabelSize(0.13)
h1.GetYaxis().SetLabelSize(0.11)
h1.GetXaxis().SetTitleFont(42)
h1.GetYaxis().SetTitleFont(42)
h1.LabelsOption("v","X")

h1.Draw("e0pX0")
h3.Draw("e2same")

categ2  = ROOT.TPaveText(0.86, 0.1, 0.95, 0.4, "NDC")
categ2.SetBorderSize(   0 )
categ2.SetFillStyle(    0 )
categ2.SetTextAlign(   12 )
categ2.SetTextSize ( 0.16 )
categ2.SetTextColor(    1 )
categ2.SetTextFont (   62 )
categ2.AddText("D_{0-}^{ggH}")
categ2.Draw("same")

c.cd()
pad1.Draw()

ROOT.gPad.RedrawAxis()

c.Modified()
imgName = "unrolled_Jan27/unrolled_"+args.variable+"_"+args.channel+"_"+args.year+"_"+catN+".pdf" 
if args.isPrefit is True:
    imgName = imgName.replace(".pdf","_prefit.pdf")
c.SaveAs(imgName)
c.SaveAs(imgName.replace("pdf","png"))




