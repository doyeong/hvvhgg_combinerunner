import os, sys, re
import array
import ROOT
from ROOT import TCanvas, TFile, TH1F, TH2F
import printer
from ROOT import gROOT
from ROOT import gDirectory
import myExcalibur as ex

sysMap={
    "nominal":"",
    "EEScale":"CMS_scale_e", #Electron Energy scale uncertainties
    "EESigma":"CMS_EESigma", ## FIXME ##
    "JetAbsolute":"CMS_JetAbsolute",
    "JetAbsoluteyear":"CMS_JetAbsolute_year",
    "JetBBEC1":"CMS_JetBBEC1",
    "JetBBEC1year":"CMS_JetBBEC1_year",
    "JetEC2":"CMS_JetEC2",
    "JetEC2year":"CMS_JetEC2_year",
    "JetFlavorQCD":"CMS_JetFlavorQCD",
    "JetHFyear":"CMS_JetHF_year",
    "JetHF":"CMS_JetHF",
    "JetJER":"CMS_JER_year",
    "JetRelBal":"CMS_JetRelativeBal",
    "JetRelSam":"CMS_JetRelativeSample_year",
    "JetUES":"CMS_scale_met_unclustered_year",
    "MES":"CMS_MES", ## FIXME ##
    "RecoilReso":"CMS_htt_boson_reso_met_year", ## FIXME ##
    "RecoilResp":"CMS_htt_boson_scale_met_year", ## FIXME ##
    "Rivet0":"THU_ggH_Mu",
    "Rivet1":"THU_ggH_Res",
    "Rivet2":"THU_ggH_Mig01",
    "Rivet3":"THU_ggH_Mig12",
    "Rivet4":"THU_ggH_VBF2j",
    "Rivet5":"THU_ggH_VBF3j",
    "Rivet6":"THU_ggH_PT60",
    "Rivet7":"THU_ggH_PT120",
    "Rivet8":"THU_ggH_qmtop",
    "ttbarShape":"CMS_htt_ttbarShape",
    "dyShape":"CMS_htt_dyShape",
    "RecoilReso_njets0_YEA":"CMS_htt_boson_reso_met_0jet_year",
    "RecoilReso_njets1_YEA":"CMS_htt_boson_reso_met_1jet_year",
    "RecoilReso_njets2_YEA":"CMS_htt_boson_reso_met_2jet_year",
    "RecoilResp_njets0_YEA":"CMS_htt_boson_scale_met_0jet_year", 
    "RecoilResp_njets1_YEA":"CMS_htt_boson_scale_met_1jet_year",
    "RecoilResp_njets2_YEA":"CMS_htt_boson_scale_met_2jet_year",
    "CMS_htt_emb_ttbar_YEA":"CMS_htt_emb_ttbar",
    "prefiring":"CMS_prefiring",
    "":"CMS_QCDsyst"

    
    # CMS_scale_e_Scale_13TeV
    # "CMS_scale_e_Sigma_13TeV"
    # "muES"
    # "CMS_scale_met_unclustered
    # "CMS_htt_dyShape
    #  "CMS_htt_ttbarShape
    

}
def renameHistSys(hName, year):
    hNameNew,process,sys = hName, hName, "nominal"
    if "Up" in hName or "Down" in hName: # or "up" in hName or "down" in hName: 
        if "reweighted" in hName:
            process = hName[:hName.rfind("125")+3]
        elif "HWW" in hName:
            process = hName[:hName.find("_")+4]
        else:
            process = hName[:hName.find("_")]
        sys = hName[len(process)+1:-3] if "Up" in hName else hName[len(process)+1:-5]

        #printer.gray(process)
        #printer.gray(sys)
        hNameNew = "%s_%sUp"%(process, sysMap[sys]) if "Up" in hName else "%s_%sDown"%(process, sysMap[sys])
        if "year" in hNameNew: hNameNew = hNameNew.replace("year",year)
        #printer.gray(hNameNew)
    if "prefiring_down" in hName: hNameNew = hNameNew.replace("prefiring_down", "CMS_prefiringDown")
    if "prefiring_up" in hName: hNameNew = hNameNew.replace("prefiring_up", "CMS_prefiringUp")


    if "ZTT" in hNameNew: hNameNew = hNameNew.replace("ZTT", "embedded")
    if "ZLL" in hNameNew: hNameNew = hNameNew.replace("ZLL", "ZL")
    if "TT" in hNameNew: hNameNew = hNameNew.replace("TT", "TTL")
    if "ggH_HWW" in hNameNew: hNameNew = hNameNew.replace("ggH_HWW", "ggH_hww125")
    if "qqH_HWW" in hNameNew: hNameNew = hNameNew.replace("qqH_HWW", "qqH_hww125")
    if "VH_HWW" in hNameNew: hNameNew = hNameNew.replace("VH_HWW", "VH_hww125")
    return hNameNew


def emChecker(filename):
    if "_em_" not in filename: 
        printer.warning("This is not em datacard!! ")
        sys.exit(1)
    if "_2018" in filename: year = "2018"
    if "_2017" in filename: year = "2017"
    if "_2016" in filename: year = "2016"
    if "fa3" in filename: return "0M", year
    if "fa2" in filename: return "0PH", year
    if "fL1" in filename: return "L1", year
    if "fL1Zg" in filename: return "L1Zg", year

def histoChecker(hName, poi):
    if "reweighted" in hName:
        if "0PM" in hName: return True
        if poi in hName:
            if "L1"==poi and "L1Zg" in hName: 
                return False
            else: 
                return True
        else:
            return False
    elif "mc_ZTT" in hName:
        return False
    elif "VBFtheory" in hName:
        return False
    elif "JJHiggs" in hName:
        return False
    elif "VBFHiggs" in hName:
        return False
    else:
        return True

def main():
    filename = sys.argv[1]    
    isVerbose = sys.argv[2]
    poi, year = emChecker(filename)
    printer.info(poi+" datacard")
    fIn = ex.fileOpener(filename, 'READ')
    filenameOut = filename[:-5]+"_converted.root"
    fOut = TFile(filenameOut, 'RECREATE')

    for dir in fIn.GetListOfKeys():
        tdirName = dir.GetName()
        if tdirName=="em_vbf": continue

        if "0jet" not in tdirName and "boosted" not in tdirName:
            if poi=="0PM" and "DCP" in tdirName: continue
            if poi!="0PM" and "DCP" not in tdirName: continue
        
        fOut.mkdir(tdirName)
        tdirOut = fOut.Get(tdirName)
        printer.whiteBlueBold("\n>> "+tdirName)
        tdir = fIn.Get(tdirName)
        for h in tdir.GetListOfKeys():
            hName = h.GetName()
            keepThis = histoChecker(hName, poi)
            if keepThis is False: 
                continue
            histo = tdir.Get(hName)
            if 'v' in isVerbose:
                printer.msg('\t'+hName+' is copying ... '+str(histo.Integral())+' as '+renameHistSys(hName, year))
            tdirOut.cd()
            histo.Clone(renameHistSys(hName, year)).Write()

    printer.green("%s created"%filenameOut)
    printer.green("python merge_bins.py --inputfile %s"%filenameOut)

if __name__ == "__main__":
    main()
