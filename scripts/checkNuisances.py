#!/usr/bin/env python
import ROOT
from ROOT import *
import re, sys, os
from array import array
from optparse import OptionParser
import printer

#parser = OptionParser()
import argparse
parser = argparse.ArgumentParser("Compare total template to stage 1.1 templates")
parser.add_argument(
        "--fixOnesided",
        action="store",
        dest="fixOnesided",
        default=0,
        help="Fix the one-sided nuisances to two-sided nuisances? ")
parser.add_argument(
        "--fixZeroNominal",
        action="store",
        dest="fixZeroNominal",
        default=0,
        help="Fix zero nomianl and non-zero nuisances? ")
parser.add_argument(
        "--inputfile",
        action="store",
        dest="inputfile",
        default="X.root",
        help="Which file1name to run over?")
parser.add_argument(
        "--outputfile",
        action="store",
        dest="outputfile",
        default="Y.root",
        help="Which file1name to run over?")
parser.add_argument(
        '--verbose', '-v', action='store_true',
        default=False, dest='verbose',
        help='verbose mode')
args = parser.parse_args()

plotCheckingTxtFile = open("runNuisancesPlot.txt","w")
fixOnesided=int(args.fixOnesided)
fixZeroNominal=int(args.fixZeroNominal)
inputfile =args.inputfile
outputfile=args.outputfile
file=ROOT.TFile(inputfile,"r")
file.cd()
dirList = gDirectory.GetListOfKeys()

ofile=ROOT.TFile(outputfile,"recreate")
categories_list= file.GetListOfKeys()
categories=[]
for k2 in categories_list:
	categories.append(k2.GetName())
ncat=len(categories)

N_bins_ok=0
N_bins_oneSided=0

for k1 in dirList: # loop over categories

    names_histos_used=[]

    h1 = k1.ReadObj()
    nom=k1.GetName()
    ofile.mkdir(nom)

    h1.cd()
    histoList = gDirectory.GetListOfKeys()
    name_last=""

    #h_TT=gDirectory.Get("TT")

    #mydir=ofile.mkdir(nom)
    #print "dir: ",nom

    # look only in D0- bin4
    #if '_bin4' not in nom:
    #    continue

    N_histo=0

    for histo in histoList:
        h1.cd()
        h2 = histo.ReadObj()
        h3_Up=h2.Clone()
        histo_name=h2.GetName()
        if (h2.GetName()==name_last):
            continue
        name_last=histo_name

        h3_Up.SetName(histo_name)
        h_TT=gDirectory.Get("TT")
        # get central histogram
        name_central=""
        if histo_name.startswith("embedded"):
            name_central="embedded"
        elif histo_name.startswith("data_obs"):
            name_central="data_obs"
        elif histo_name.startswith("jetFakes"):
            name_central="jetFakes"
        elif histo_name.startswith("ZL"):
            name_central="ZL"
        elif histo_name.startswith("TTL"):
            name_central="TTL"
        elif histo_name.startswith("VVL"):
            name_central="VVL"
        elif histo_name.startswith("STL"):
            name_central="STL"
        elif histo_name.startswith("reweighted_ggH_htt_0PM125"):
            name_central="reweighted_ggH_htt_0PM125"
        elif histo_name.startswith("reweighted_ggH_htt_0M125"):
            name_central="reweighted_ggH_htt_0M125"
        elif histo_name.startswith("reweighted_ggH_htt_0Mf05ph0125"):
            name_central="reweighted_ggH_htt_0Mf05ph0125"
        elif histo_name.startswith("reweighted_WH_htt_0PM125"):
            name_central="reweighted_WH_htt_0PM125"
        elif histo_name.startswith("reweighted_WH_htt_0M125"):
            name_central="reweighted_WH_htt_0M125"
        elif histo_name.startswith("reweighted_WH_htt_0Mf05ph0125"):
            name_central="reweighted_WH_htt_0Mf05ph0125"
        elif histo_name.startswith("reweighted_ZH_htt_0PM125"):
            name_central="reweighted_ZH_htt_0PM125"
        elif histo_name.startswith("reweighted_ZH_htt_0Mf05ph0125"):
            name_central="reweighted_ZH_htt_0Mf05ph0125"
        elif histo_name.startswith("reweighted_ZH_htt_0M125"):
            name_central="reweighted_ZH_htt_0M125"
        elif histo_name.startswith("reweighted_qqH_htt_0PM125"):
            name_central="reweighted_qqH_htt_0PM125"
        elif histo_name.startswith("reweighted_qqH_htt_0M125"):
            name_central="reweighted_qqH_htt_0M125"
        elif histo_name.startswith("reweighted_qqH_htt_0Mf05ph0125"):
            name_central="reweighted_qqH_htt_0Mf05ph0125"

        if "Down" in histo_name and args.verbose is True:
                printer.whiteGreenBold( "\nhisto name: %s\n  central histo name: %s"%(histo_name,name_central))
        else:
                printer.whiteGreenBold( "\nhisto name: %s\n  central histo name: %s"%(histo_name,name_central))
        isOneSided = 0 #False
        if name_central=="":
            continue
        h_central=gDirectory.Get(name_central)
        if "Up" in histo_name:
            h3_Down=gDirectory.Get(histo_name.replace("Up","Down"))

            for i in range(1,h3_Down.GetNbinsX()+1):
                # Get yields of norminal/up/down in each bin
                yield_nominal=h_central.GetBinContent(i)
                yield_Up=h3_Up.GetBinContent(i)
                yield_Down=h3_Down.GetBinContent(i)
                rel_Up=-100
                rel_Down=-100
                ratio=-100
                # If yield of nominal is zero and up/down are non-zero set all to zero (nominal value)
                if fixZeroNominal and yield_nominal<0.0011:
                    if yield_Up>0.0011: 
                            h3_Up.SetBinContent(i,yield_nominal)
                            h3_Up.SetBinError(i,h_central.GetBinError(i))
                    if yield_Down>0.0011: 
                            h3_Down.SetBinContent(i,yield_nominal)
                            h3_Down.SetBinError(i,h_central.GetBinError(i))
                    
                # If yield of nominal is non-zero
                if h_central.GetBinContent(i)>0.0011:
                    rel_Up=h3_Up.GetBinContent(i)/h_central.GetBinContent(i) # up/nominal
                    rel_Down=h3_Down.GetBinContent(i)/h_central.GetBinContent(i) # down/nominal
                    if rel_Down!=0:
                        ratio=rel_Up/rel_Down
                #print "     histo: %s (%s), has relative nuisance value in bin %s %s %s  ==> ratio %s"%(histo_name,name_central,i,rel_Up, rel_Down,ratio)
                printer.gray("\n   %s  histo: %s (%s), nominal %s, nuisance in bin %s %s %s => relative nuisance %s %s  ==> ratio %s"%(nom, histo_name,name_central,h_central.GetBinContent(i),i,h3_Up.GetBinContent(i), h3_Down.GetBinContent(i),rel_Up, rel_Down,ratio))
                if (1-rel_Up)*(rel_Down-1)<0 and not (yield_nominal<0.0011 and yield_Up<0.0011 and yield_Down<0.0011) and not ((yield_Up>0.0011 or yield_Down>0.0011) and (yield_Down<0.0011 or yield_Up<0.0011)):
                    printer.whiteRedBold("\tOneSided!\t")
                    isOneSided += 1#True
                    N_bins_oneSided=N_bins_oneSided+1
                    # now fix the nuisance if onesided
                    if fixOnesided:
                        print "\t   ==> fixing one-sided"
                        integral_Up=h3_Up.Integral()
                        integral_Down=h3_Down.Integral()
                        if integral_Up>integral_Down: # move Up up and Down down
                            if 1-rel_Up<0: # both above
                                # move Down down
                                new_down=2-rel_Down
                                new_yield=new_down*h_central.GetBinContent(i)
                                if new_yield<0:
                                        new_yield=0
                                h3_Down.SetBinContent(i,new_yield)
                                print "\t      move Down down %s"%(new_down*h_central.GetBinContent(i))
                            else: # both down
                                # move Up up
                                new_up=2-rel_Up
                                new_yield=new_up*h_central.GetBinContent(i)
                                if new_yield<0:
                                        new_yield=0
                                h3_Up.SetBinContent(i,new_yield)
                                print "\t      move Up up %s"%(new_up*h_central.GetBinContent(i))
                        else: # move Up down and Down up
                            if 1-rel_Up<0: # both above
                                # move Up down
                                new_up=2-rel_Up
                                new_yield=new_up*h_central.GetBinContent(i)
                                if new_yield<0:
                                        new_yield=0
                                h3_Up.SetBinContent(i,new_yield)
                                print "\t      move Up down %s"%(new_up*h_central.GetBinContent(i))
                            else: # both down
                                # move Down up
                                new_down=2-rel_Down
                                new_yield=new_down*h_central.GetBinContent(i)
                                if new_yield<0:
                                        new_yield=0
                                h3_Down.SetBinContent(i,new_yield)
                                print "\t     move Down up %s"%(new_down*h_central.GetBinContent(i))
                else:
                    N_bins_ok=N_bins_ok+1
                    
            # write to output file:
        if "Up" in histo_name:
            printer.green("\t===> write Up/Down %s/%s"%(h3_Up.GetName(),h3_Down.GetName()))
            ofile.cd(nom)
            h3_Up.Write()
            h3_Down.Write()
        elif "Down" in histo_name and args.verbose is True:
            printer.warning("\t===> don't save this is Down")
        else:
            printer.gray( "\t===> write nominal %s"%(h_central.GetName()))
            ofile.cd(nom)
            h_central.Write()
        if args.verbose is True: printer.orange(str(isOneSided))
        if isOneSided > (h_central.GetNbinsX())*0.5:#is True:
                if "Up" in h3_Up.GetName():
                        plotCheckingTxtFile.write("\n\npython scripts/checkNuisancesPlot.py "+inputfile+" "+h_central.GetName()+" "+(h3_Up.GetName())[(h3_Up.GetName()).find("CMS"):-2])
                        plotCheckingTxtFile.write("\n\npython scripts/checkNuisancesPlot.py "+outputfile+" "+h_central.GetName()+" "+(h3_Up.GetName())[(h3_Up.GetName()).find("CMS"):-2])
                elif "Down" in h3_Up.GetName():
                        plotCheckingTxtFile.write("\npython scripts/checkNuisancesPlot.py "+inputfile+" "+h_central.GetName()+" "+(h3_Up.GetName())[(h3_Up.GetName()).find("CMS"):-4])
                        plotCheckingTxtFile.write("\npython scripts/checkNuisancesPlot.py "+outputfile+" "+h_central.GetName()+" "+(h3_Up.GetName())[(h3_Up.GetName()).find("CMS"):-4])

#ofile.Close()

N_bins_all=N_bins_oneSided+N_bins_ok
print "\n\t SUMMARY:   N_bins_ok= %s (%s), N_bins_oneSidesNuisances= %s (%s)"%(N_bins_ok,float(N_bins_ok)/float(N_bins_all),N_bins_oneSided,float(N_bins_oneSided)/float(N_bins_all))
plotCheckingTxtFile.close()
