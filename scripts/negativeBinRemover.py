import os, sys, re
import array
import ROOT
from ROOT import TCanvas, TFile, TH1F, TH2F
import printer
import time

def zeroBinRemover(histo1D,tdirOut,tdirIn,hName,isVerbose):
    nx=histo1D.GetXaxis().GetNbins()
    #hName = histo1D.GetName()
    histo=TH1F(hName,hName,nx,0,nx)
    l=0
    hNominalName=hName[:hName.find("_CMS")]
    if "THU" in hName: hNominalName=hName[:hName.find("_THU")]
    hNominal=tdirIn.Get(hNominalName)
    if isVerbose is True and hNominal:
        strFormat = '%-25s%-80s%-10s%-20s\n'
        strOut = strFormat % (tdirOut.GetName(), hName,'nominal:',hNominalName)
        printer.gray(strOut)
        #printer.gray(hName+"\t nominal:\t"+hNominalName)
    elif isVerbose is True:
        strFormat = '%-25s%-80s%-10s%-20s\n'
        strOut = strFormat % (tdirOut.GetName(), hName,'nominal:',hNominalName)
        printer.whiteRedBold(strOut)
    for j in range(1,nx+1):
        l=l+1
        if isVerbose is True and hNominal:
            printer.msg(str(hNominal.GetBinContent(l))+"\t"+ str(histo1D.GetBinContent(l)))
        if histo1D.GetBinContent(l)<0.0: # negative bin
            if isVerbose is True: printer.info("Case1: Nagative bin")
            histo.SetBinContent(l,0.0)
            histo.SetBinError(l,0.0)
            #printer.orange("!"+str(l)+" "+tdirOut.GetName()+" "+hName)
        elif hNominal and hNominal.GetBinContent(l)<=0.0 and histo1D.GetBinContent(l)!=0.0:
            if isVerbose is True: printer.info("Case2: Nagative nominal bin")
            histo.SetBinContent(l,0.0)#hNominal.GetBinContent(l))
            histo.SetBinError(l,0.0)#hNominal.GetBinError(l))
            if isVerbose is True: printer.whiteRedBold("!!!"+str(l)+"th bin set to zero as nominal is zero(or negative)\t ")
            '''
            elif hNominal and hNominal.GetBinContent(l)>0.0 and histo1D.GetBinContent(l)/hNominal.GetBinContent(l)>5.0: 
            if isVerbose is True: printer.info("Case3: Large deviation")
            histo.SetBinContent(l,hNominal.GetBinContent(l))
            histo.SetBinError(l,hNominal.GetBinError(l))
            if isVerbose is True: printer.whiteRedBold("!!!"+str(l)+"th bin set to nominal values\t ")
            '''
        else:
            if isVerbose is True: printer.msg("Case4: Simple copy")
            histo.SetBinContent(l,histo1D.GetBinContent(l))
            histo.SetBinError(l,histo1D.GetBinError(l))

    #printer.gray('\t No more negative bin ... '+str(histo.Integral()))

    if "Up" in hName or "Down" in hName:
        # Add small values for empty up/down when nominal is non-zero        
        if histo.Integral()<=0.0 :
            if hNominal.Integral()!=0.0:
                histo.SetBinContent(1,0.001)
                histo.SetBinError(1,0.0)
                if isVerbose is True:
                    if "reweight" in hName: 
                        printer.red("Null shapes set to 0.001\t\t"+tdirOut.GetName()+"\t"+hName+"\t"+str(histo.Integral()))
                    else : 
                        printer.msg("Null shapes set to 0.001\t\t"+tdirOut.GetName()+"\t"+hName+"\t"+str(histo.Integral()))
    
    tdirOut.cd()
    histo.Write()

    

def main():
    start = time.time()
    filename = sys.argv[1]
    isVerbose = False
    if len(sys.argv)>2: 
        isVerbose = True
        printer.info("Verbose Mode is on")
    
        
    fIn = TFile(filename, 'READ')
    dirList = []
    for dir in fIn.GetListOfKeys():
        #print(dir.GetName())
        dirList.append(dir.GetName())
    fIn.Close()
    

    filenameOut = filename[0:-5]+"_noNegativeBins.root"
    fOut = TFile(filenameOut, 'RECREATE')

    for dir in dirList:
        fIn = TFile(filename, 'READ')
        tdirName = dir
        fOut.mkdir(tdirName)
        tdirIn = fIn.Get(tdirName)
        tdirOut = fOut.Get(tdirName)
        if isVerbose:
            printer.whiteBlueBold("\n>> "+tdirName)
        else:
            printer.gray("\n>> "+tdirName)
        tdir = fIn.Get(tdirName)
        for h in tdir.GetListOfKeys():
            hName = h.GetName()
            if "ttH" in hName: continue
            if "ggZH" in hName: continue
            if "MINLO" in hName: continue
            if "EWKW" in hName: continue
            if "htt_0PM125_" in hName and "htt_0PM125_CMS" not in hName: 
                if "_THU_" not in hName:
                    continue
            if "htt_0M125_" in hName and "htt_0M125_CMS" not in hName: 
                if "_THU_" not in hName:
                    continue
            if "htt_0Mf05ph0125_" in hName and "htt_0Mf05ph0125_CMS" not in hName:
                if "_THU_" not in hName:
                    continue
            histo1D = tdir.Get(hName)
            #printer.blue(str(histo1D.Integral()))
            zeroBinRemover(histo1D,tdirOut,tdirIn,hName,isVerbose)
            #printer.msg("\t"+hName)
        fIn.Close()
    printer.gray("Saving output...\n\t"+filenameOut)
    fOut.Close()
    printer.gray("time : %.3fs"%(time.time() - start)  )

if __name__ == "__main__":
    main()

            
        
