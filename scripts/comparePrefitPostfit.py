#!/usr/bin/env python
import sys, os, re
import ROOT
import printer
from ROOT import TCanvas, TFile, TH1F
from ROOT import gROOT
from ROOT import gDirectory
from ROOT import TLine

ROOT.gStyle.SetFrameLineWidth(2)
ROOT.gStyle.SetLineWidth(1)
ROOT.gStyle.SetOptStat(0)
gROOT.SetBatch(ROOT.kTRUE)

## python scripts/checkNuisancesPlot.py shapes/USCMS/htt_tt.inputs-sm-13TeV_2018-2D_MELAVBF_2D_htt_tt_emb_sys_fa2_mergedBins.root reweighted_qqH_htt_0PM125 CMS_scale_t_1prong1pizero_2018
path = sys.argv[1]
process = sys.argv[2]

def titleStyler(title, titletext):
  title.SetFillStyle(0)
  title.SetBorderSize(0)
  title.AddText(titletext)

def padMaker(pad):
    pad.SetFillColor(0)
    pad.Draw()
    pad.cd()

def ratioStyle(h,updown,xlable):
    h.SetLineWidth(1)
    h.SetLineColor(1)
    h.SetLineStyle(1)
    h.SetMarkerStyle(20)
    h.SetMarkerSize(2)
    h.GetXaxis().SetTitle(xlable) #"Visible Mass [GeV]")
    h.GetXaxis().SetTitleSize(0.11)
    h.GetXaxis().SetTitleOffset(0.7)
    h.GetXaxis().SetLabelSize(0.08)
    h.GetYaxis().SetLabelSize(0.08)
    h.GetYaxis().SetTitle("post/pre")
    h.GetYaxis().SetTitleSize(0.08)
    h.GetYaxis().SetTitleOffset(0.4)
    #if "BSM" in h.GetName():
    #elif "MIX" in h.GetName():
    if updown>0.0:
        h.SetMarkerColor(2)
        h.SetLineColor(2)
    elif updown<0.0:
        h.SetMarkerColor(64)
        h.SetLineColor(64)
        h.SetLineStyle(2)

cateMap={
  "htt_tt_1_13TeV_2017_prefit":"0jet",
  "htt_tt_2_13TeV_2017_prefit":"Boosted",
  "htt_tt_3_13TeV_2017_prefit":"VBF BSM [0.0:0.2]",
  "htt_tt_4_13TeV_2017_prefit":"VBF BSM [0.2:0.5]",
  "htt_tt_5_13TeV_2017_prefit":"VBF BSM [0.5:0.8]",
  "htt_tt_6_13TeV_2017_prefit":"VBF BSM [0.8:1.0]",
}


# Open root file
printer.gray("Input file:\t"+path)
if os.path.isfile(path) is False: 
    printer.warning(path+ "  doesn't exist")
    sys.exit(1)
file = ROOT.TFile(path,"r")

poi = "Unknown"
if "_fa3_" in path: poi = "fa3"
elif "_fa2_" in path: poi = "fa2"
elif "_fL1_" in path: poi = "fL1"
elif "_fL1Zg_" in path: poi = "fL1Zg"
elif "_fa3ggH_" in path: poi = "fa3ggH"

# Print info
printer.info("process:\t"+process) #+"\t\tshape:\t"+shape+"\tPOI:"+poi)



# Loop over all TDirectories
for dir in file.GetListOfKeys(): 
    tdirName = dir.GetName() 
    if "postfit" in tdirName: continue
    printer.whiteBlueBold("\t"+tdirName+"\t")
    hNominal = file.Get("%s/%s"%(tdirName, process)) # pre-fit
    hNominal.Sumw2()
    hUp = file.Get("%s/%s"%(tdirName.replace("prefit","postfit"), process))
    hUp.Sumw2()
    ##########################
    ##     Build Canvas     ##
    ##########################
    canvasName = tdirName+"_"+process+"_"+poi
    canvas = ROOT.TCanvas(canvasName,"",0,0,1500,1100)
    canvas.cd()

    # Up pad
    upPad = ROOT.TPad("padH","padH",0,0.3,1,1)
    padMaker(upPad)
    upPad.SetBottomMargin(0)

    # draw histograms
    legend = ROOT.TLegend(.13,.70,.55,.87, "", "brNDC")

    if hNominal:
        printer.gray(hNominal.GetName()+"\t"+str(hNominal.Integral()))
        hNominal.SetLineColor(8)
        hNominal.SetTitle("")
        if hNominal.Integral()>=0.0:
          hNominal.SetMinimum(0.0)
        if hNominal.GetMaximum()>hUp.GetMaximum():
          hNominal.SetMaximum(hNominal.GetMaximum()*1.5)
        else:
          hNominal.SetMaximum(hUp.GetMaximum()*1.5)

        hNominal.Draw("HISTE")
        legend.AddEntry(hNominal,"Pre-fit: "+str(hNominal.Integral()),"l")
    else:
        printer.warning(hNominal.GetName()+"\t doesn't exist")
        sys.exit(1)
    if hUp:
        printer.gray(hUp.GetName()+"\t"+str(hUp.Integral()))
        hUp.SetLineColor(2)
        hUp.SetLineStyle(2)
        hUp.Draw("HISTESAME")
        legend.AddEntry(hUp,"Post-fit: "+str(hUp.Integral()),"l")
    else:
        printer.warning(hUp.GetName()+"\t doesn't exist")
    
    # Draw legend and title
    title = ROOT.TPaveText(.30,.90,0.70,.97,"NDC")
    titleString = cateMap[tdirName].replace("BSM",poi)
    titleStyler(title,"2018 "+titleString+" ("+process+")")
    title.Draw("SAME")
    legend.SetLineWidth(0)
    legend.Draw("SAME")

    # Down pad
    canvas.cd()
    downPad = ROOT.TPad("padR","padR",0.,0.0,1,0.3)
    padMaker(downPad)
    downPad.SetGridy()
    downPad.SetBottomMargin(0.25)
    
    if hUp:
      hRUp = hUp.Clone()
      hRUp.Divide(hNominal.Clone())
      ratioStyle(hRUp,0, "Bins")
      hRUp.SetTitle("")

    if hUp :
      #if hRUp.GetMaximum()<hRDown.GetMaximum():
      hRUp.SetMaximum(1.2)
      hRUp.SetMinimum(0.8)
      #if hRUp.GetMinimum()>hRDown.GetMinimum():
      #hRUp.SetMinimum(min([hRUp.GetMinimum(),hRDown.GetMinimum()])*0.9)
      #hRUp.SetMinimum(2.0-hRUp.GetMaximum())
      hRUp.Sumw2()
      hRUp.Draw("E")      

    l = TLine(0, 1, hRUp.GetNbinsX(), 1)
    l.SetLineColor(95)
    l.Draw("same")

    canvas.Update()
    
    namepdf="comparePrefitPostfit/"+canvasName+".pdf"
    canvas.SaveAs(namepdf)

printer.red("\n\nimgcat comparePrefitPostfit/*"+process+"_"+poi+".pdf\n\n")
