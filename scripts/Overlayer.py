#!/usr/bin/env python
import sys, os, re
import math
import ROOT
import printer
from ROOT import TCanvas, TFile, TH1F
from ROOT import gROOT
from ROOT import gDirectory
from ROOT import TLine

ROOT.gStyle.SetFrameLineWidth(2)
ROOT.gStyle.SetLineWidth(1)
ROOT.gStyle.SetOptStat(0)
gROOT.SetBatch(ROOT.kTRUE)

var = sys.argv[1]
path = "shapes/USCMS/htt_tt.inputs-sm-13TeV_2018-2D_MELAVBF_2D_htt_tt_emb_sys_fa3_mergedBins.root"
def titleStyler(title, titletext):
  title.SetFillStyle(0)
  title.SetBorderSize(0)
  title.AddText(titletext)

def padMaker(pad):
    pad.SetFillColor(0)
    pad.Draw()
    pad.cd()

def ratioStyle(h,updown,xlable):
    h.SetLineWidth(1)
    h.SetLineColor(1)
    h.SetMarkerStyle(20)
    h.SetMarkerSize(2)
    h.GetXaxis().SetTitle(xlable) #"Visible Mass [GeV]")
    h.GetXaxis().SetTitleSize(0.11)
    h.GetXaxis().SetTitleOffset(0.7)
    h.GetXaxis().SetLabelSize(0.08)
    h.GetYaxis().SetLabelSize(0.08)
    h.GetYaxis().SetTitle("JHU / Powheg")
    h.GetYaxis().SetTitleSize(0.08)
    h.GetYaxis().SetTitleOffset(0.4)
    #if "BSM" in h.GetName():
    #elif "MIX" in h.GetName():
    if updown==1: 
      h.SetMarkerColor(2)
      h.SetLineColor(2)
    if updown==2: 
      h.SetMarkerColor(2)
      h.SetLineColor(2)
    if updown==3: 
      h.SetMarkerColor(8)
      h.SetLineColor(8)
    if updown==4: 
      h.SetMarkerColor(1)
      h.SetLineColor(1)

    '''
    if updown>0.0:
        h.SetMarkerColor(2)
        h.SetLineColor(2)
    elif updown<0.0:
        h.SetMarkerColor(64)
        h.SetLineColor(64)
        #h.SetLineStyle(2)
    '''
cateMap={
    "tt_inclusive":"Multi (ggH vs embedded vs jetFakes)",
}

# Open root file
printer.gray("Input file:\t"+path)
if os.path.isfile(path) is False:
    printer.warning(path+ "  doesn't exist")
    sys.exit(1)
file = ROOT.TFile(path,"r")


hName1="reweighted_qqH_htt_0PM125"
hName2="VBF125"
# dummy for the moment
hName3="reweighted_qqH_htt_0PM125"
hName4="VBF125"

lName1="JHU SM qqH"
lName2="Powheg SM qqH"

# Loop over all TDirectories
for dir in file.GetListOfKeys():
    tdirName = dir.GetName()
    printer.whiteBlueBold("\t"+tdirName+"\t")
    h1 = file.Get("%s/%s"%(tdirName, hName1))
    h2 = file.Get("%s/%s"%(tdirName, hName2))
    h3 = file.Get("%s/%s"%(tdirName, hName3))
    h4 = file.Get("%s/%s"%(tdirName, hName4))

    ##########################
    ##     Build Canvas     ##
    ##########################
    canvasName = tdirName
    canvas = ROOT.TCanvas(canvasName,"",0,0,1500,1100)
    canvas.cd()

    # Up pad
    upPad = ROOT.TPad("padH","padH",0,0.3,1,1)
    padMaker(upPad)
    upPad.SetBottomMargin(0)
    upPad.SetLogy()
    # draw histograms
    legend = ROOT.TLegend(.13,.70,.50,.87, "", "brNDC")
    maxFactor = 20
    if h1:
        printer.gray("%-30s \t %.5f"%(h1.GetName(), h1.Integral()))
        h1.SetLineColor(64)
        h1.SetTitle("")
        h1.SetMaximum(h1.GetMaximum()*maxFactor)
        h1.SetMinimum(0.01)
        h1.SetLineWidth(1)
        h1.Draw("HISTE")
        legend.AddEntry(h1, "%-17s: %.4f"%(lName1, h1.Integral()),"l")
        
    if h2:
        printer.gray("%-30s \t %.5f"%(h2.GetName(), h2.Integral()))
        h2.SetLineColor(2)
        h2.SetTitle("")
        h2.SetLineWidth(2)
        h2.SetLineStyle(2)
        h2.Draw("HISTESAME")
        legend.AddEntry(h2, "%-17s: %.4f"%(lName2, h2.Integral()),"l")
        if h2.GetMaximum() > h1.GetMaximum(): h1.SetMaximum(h2.GetMaximum()*maxFactor)


    # Draw legend and title
    title = ROOT.TPaveText(.15,.90,0.85,.97,"NDC")
    titleString = ""# cateMap[tdirName]+" ("+var+")"
    titleStyler(title,titleString)
    title.Draw("SAME")
    legend.SetLineWidth(0)
    legend.Draw("SAME")
    # Down pad
    canvas.cd()
    downPad = ROOT.TPad("padR","padR",0.,0.0,1,0.3)
    padMaker(downPad)
    downPad.SetGridy()
    downPad.SetBottomMargin(0.25)
    #downPad.SetLogy()
    if h1:
        hR = h2.Clone()
        hR.Divide(h1.Clone())
        ratioStyle(hR,4, tdirName)
        hR.Draw("E")
        hR.SetMinimum(0.0)
        hR.SetMaximum(2.5)
        print "VBF/ggH Ratio RMS\t",hR.GetRMS()

    Nbins= h2.GetNbinsX()
    lowX = h2.GetBinLowEdge(1)
    highX = h2.GetBinLowEdge(Nbins+1)
    l0 = TLine(lowX, 1, highX, 1)
    l0.SetLineColor(91)
    l0.SetLineWidth(1)
    l0.Draw("same")
    hR.SetMinimum(0.0)
    hR.SetMaximum(3)

    canvas.Update()
    namepdf="plots/%s_%s_overlay.pdf"%(var, tdirName)
    canvas.SaveAs(namepdf)
    canvas.SaveAs(namepdf.replace("pdf","png"))
    
printer.green("\n\nimgcat plots/%s_*_overlay.png\n\n"%(var))
        
