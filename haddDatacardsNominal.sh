echo "~/Alex-Hadd/ahadd.py -f /scratch/doyeong/2D_htt_tt_emb_sys_fa2VBF_${2}.root /hdfs/store/user/doyeong/datacardCondor/${2}X10_${1}_fa2/*nominal*root"
echo "~/Alex-Hadd/ahadd.py -f /scratch/doyeong/2D_htt_tt_emb_sys_fL1VBF_${2}.root /hdfs/store/user/doyeong/datacardCondor/${2}X10_${1}_fL1/*nominal*root"
echo "~/Alex-Hadd/ahadd.py -f /scratch/doyeong/2D_htt_tt_emb_sys_fL1ZgVBF_${2}.root /hdfs/store/user/doyeong/datacardCondor/${2}X10_${1}_fL1Zg/*nominal*root"

echo "~/Alex-Hadd/ahadd.py -f /scratch/doyeong/2D_htt_tt_emb_sys_fa3VBF_${2}.root /hdfs/store/user/doyeong/datacardCondor/${2}X10_${1}_fa3/m_mtt*nominal* /hdfs/store/user/doyeong/datacardCondor/${2}X10_${1}_fa3/AC_datacards_HVV_VBFcat_DeepMedium_OS_Iso_myFakes_*nominal*_${2}.root"
echo "~/Alex-Hadd/ahadd.py -f /scratch/doyeong/2D_htt_tt_emb_sys_fa3ggH_${2}.root /hdfs/store/user/doyeong/datacardCondor/${2}X10_${1}_fa3/m_mtt*nominal* /hdfs/store/user/doyeong/datacardCondor/${2}X10_${1}_fa3/AC_datacards_HGG_VBFcat_DeepMedium_OS_Iso_myFakes_*nominal*_${2}.root"

echo "python scripts/rename_HVV_BSM.py /scratch/doyeong/2D_htt_tt_emb_sys_fa3VBF_${2}.root"
echo "python scripts/rename_HVV_BSM.py /scratch/doyeong/2D_htt_tt_emb_sys_fa3ggH_${2}.root"
echo "python scripts/rename_HVV_BSM.py /scratch/doyeong/2D_htt_tt_emb_sys_fa2VBF_${2}.root"
echo "python scripts/rename_HVV_BSM.py /scratch/doyeong/2D_htt_tt_emb_sys_fL1VBF_${2}.root"
echo "python scripts/rename_HVV_BSM.py /scratch/doyeong/2D_htt_tt_emb_sys_fL1ZgVBF_${2}.root"

echo "cd /scratch/doyeong/datacards"
# /hdfs/store/user/doyeong/datacardCondor/2016X10_Mar17_fa3
