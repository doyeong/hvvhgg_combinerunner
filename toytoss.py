#!/usr/bin/env python
import ROOT
from ROOT import *

import re, sys, os
from array import array
from optparse import OptionParser
import argparse
from scripts import printer

parser = argparse.ArgumentParser("Compare total template to stage 1.1 templates")
parser.add_argument(
        "--chn",
        action="store",
        dest="chn",
        default="tt",
        help="Which channel is this ?")
parser.add_argument(
        "--emb",
        action="store",
        dest="emb",
        default=0,
        help="Which file1name to run over?")
parser.add_argument(
        "--input","-i",
        action="store",
        dest="input",
        default="MANUAL_optBins_syst",
        help="Which file1name to run over?")
parser.add_argument(
        "--useDCP",
        action="store",
        dest="useDCP",
        default=0,
        help="Which file1name to run over?")
parser.add_argument(
        "--par",
        action="store",
        dest="par",
        default="fa3",
        help="Which file1name to run over?")
parser.add_argument(
        "--print","-p",
        action="store_true",
        dest="printTrue",
        default=False,
        help="print command line, do not run")
parser.add_argument(
        "--shape","-s",
        action="store_true",
        dest="shape",
        default=False,
        help="Shape is True in Morphing")
parser.add_argument(
        "--ggHInt",
        action="store_true",
        dest="ggHInt",
        default=False,
        help="")
parser.add_argument(
        "--sync",
        action="store",
        dest="sync",
        default="noSync",
        help="")
args = parser.parse_args()

def runCommand(cmd):
    printer.gray(cmd)
    if args.printTrue is False: os.system(cmd)

def cdDir(dir):
    printer.yellow("Where are we???\n"+os.getcwd()+"\n\n")
    printer.gray("cd "+dir)
    if os.path.isdir(dir) is False:
        printer.warning("There is no directory - "+ dir)
        if args.printTrue is False: 
            sys.exit(0)
    else: 
        os.chdir(dir)
    printer.green("Where are we???\n"+os.getcwd()+"\n\n")

chn=args.chn # nbins->chn 1
emb=bool(args.emb) # year->emb 3
useDCP=int(args.useDCP) # ok 6
par=args.par # 7
input=args.input
shape=args.shape
year=-999
pureInputFileName = args.input[args.input.rfind("/"):]
print pureInputFileName
if "2016" in pureInputFileName : year=2016
elif "2017" in pureInputFileName : year=2017
elif "2018" in pureInputFileName : year=2018
if year<0:
    printer.warning("Failure to detect year")
    sys.exit(0)

inputString = pureInputFileName[1:pureInputFileName.find(par)]+par#+"_toytoss" 
if "noDCP" in input:
    inputString+="_noDCP"

printer.info("inputString: "+inputString)
outptuRename=input.replace(".root","_renamed.root")
newName="""htt_{chn}.inputs-sm-13TeV-2D_{year}_{inputString}_mergedBins.root""".format(chn=chn, year=year,inputString=inputString)
newNameNoNegative="""htt_{chn}.inputs-sm-13TeV-2D_{year}_{inputString}_mergedBins_noNegativeBins.root""".format(chn=chn, year=year,inputString=inputString)
newNameAfterNormToPowheg="""htt_{chn}.inputs-sm-13TeV_{year}-2D_MELAVBF_{inputString}_mergedBins.root""".format(chn=chn, year=year,inputString=inputString)
folderName="data_"+inputString

initialDir=os.getcwd()
'''
## Step 1
printer.blue("Rename BSM histo for combine")
if os.path.isfile(input) is True:
    cmd="""python scripts/rename_HVV_histos.py --par={par} --input={input}\n""".format(par=par,input=input)

    if os.path.isfile(newName) is True: os.remove(newName)
    cmd+="""cp {outptuRename} {newName}\n""".format(outptuRename=outptuRename, newName=newName)
else:
    printer.warning("!!! You don't have "+input)
    if args.printTrue is False: sys.exit(0)
runCommand(cmd)

## Step 2
printer.blue("Remove negative bins")
cmd="python scripts/negativeBinRemover.py "+newName
runCommand(cmd)
'''

## Step 3
if os.path.isfile(newNameAfterNormToPowheg) is True: os.remove(newNameAfterNormToPowheg)
printer.blue("Normalize by Powheg yields - channel, year, DCP, par")
cmd="""python scripts/normToPowheg.py {newNameNoNegative} {newNameAfterNormToPowheg} {chn} {year} {useDCP} {par}\n""".format(chn=chn,year=year,useDCP=useDCP,par=par,newNameNoNegative=newNameNoNegative,newNameAfterNormToPowheg=newNameAfterNormToPowheg)
runCommand(cmd)
if os.path.isfile(newNameAfterNormToPowheg) is False:
    printer.warning("Problem in NormToPowheg")
    if args.printTrue is False: sys.exit(0)

printer.blue("Move NormToPowhegOut.root to output folder")
cmd="cp "+newNameAfterNormToPowheg+" shapes/USCMS/"+newNameAfterNormToPowheg
if os.path.isfile(newNameAfterNormToPowheg) is False:
    printer.warning("There is no "+newNameAfterNormToPowheg)
    sys.exit(1)    
runCommand(cmd)

if os.path.isfile("shapes/USCMS/"+newNameAfterNormToPowheg) is False:
    printer.warning("Problem in Copying NormToPowheg")
    if args.printTrue is False: sys.exit(0)


## Step 3
printer.whiteBlueBold("Morphing: MorphingSM2016_flexible")
cmd="""MorphingSM2018_flexible --output_folder="{folderName}" --postfix="-2D_MELAVBF_{inputString}_mergedBins" --control_region=0 --manual_rebin=false --real_data=false --mm_fit=false --ttbar_fit=false --embedded={emb} --jetfakes=true --shapeSyst=false --year={year}  --chn={chn} --par={par}\n\n""".format(inputString=inputString, emb=emb, year=year, chn=chn, par=par,folderName=folderName)
if "ggH" in par:
    cmd=cmd.replace("--par=fa3ggH","--par=fa3",1)
    if args.ggHInt is True:
        printer.info("ggH interference will be included")
        cmd=cmd.replace("--par=fa3","--par=fa3 --use_ggHint=True",1)
    if "danny" in args.sync:
        printer.whiteGreenBold("This is sync datacards")
        cmd=cmd.replace("--par=fa3","--par=fa3 --do_sync=danny",1)
if shape is True:
    cmd=cmd.replace("--shapeSyst=false", "--shapeSyst=true",1)
runCommand(cmd)

## Step 4
printer.blue("move to the output folder")
datacardsFolder = """output/{folderName}""".format(folderName=folderName)
cdDir(datacardsFolder)

## Step 5
printer.blue("Delete small size files")
cmd="find . -name \"*.txt\" -size -2k -delete\n\n"
runCommand(cmd)

## Step 6
printer.whiteBlueBold("Make workspace")
cmd="""combineTool.py -M T2W -i {chn}/* -o workspace.root --parallel 18\n\n\n""".format(chn=chn)
runCommand(cmd)
pdir=os.getcwd()
cdDir(chn+"/125")

## Step 7
printer.whiteBlueBold("Combine datacards")
cmd="\n\n"

if chn=="em":
    if year==2018:
        cmd+=""" 
        sed -i '/zmumuShape/d' "*{chn}*{year}*.txt"
        """.format(chn=chn,year=year)
if "danny" in args.sync:
    cmd+="""combineCards.py htt_{chn}_1_13TeV_{year}=htt_{chn}_1_13TeV_{year}.txt htt_{chn}_2_13TeV_{year}=htt_{chn}_2_13TeV_{year}.txt htt_{chn}_3_13TeV_{year}=htt_{chn}_3_13TeV_{year}.txt htt_{chn}_4_13TeV_{year}=htt_{chn}_4_13TeV_{year}.txt htt_{chn}_5_13TeV_{year}=htt_{chn}_5_13TeV_{year}.txt htt_{chn}_6_13TeV_{year}=htt_{chn}_6_13TeV_{year}.txt &> combined_{chn}_{year}.txt.cmb
    """.format(chn=chn,year=year)
elif useDCP:
    cmd+="""combineCards.py htt_{chn}_1_13TeV_{year}=htt_{chn}_1_13TeV_{year}.txt htt_{chn}_2_13TeV_{year}=htt_{chn}_2_13TeV_{year}.txt htt_{chn}_3_13TeV_{year}=htt_{chn}_3_13TeV_{year}.txt htt_{chn}_4_13TeV_{year}=htt_{chn}_4_13TeV_{year}.txt htt_{chn}_5_13TeV_{year}=htt_{chn}_5_13TeV_{year}.txt htt_{chn}_6_13TeV_{year}=htt_{chn}_6_13TeV_{year}.txt  htt_{chn}_7_13TeV_{year}=htt_{chn}_7_13TeV_{year}.txt  htt_{chn}_8_13TeV_{year}=htt_{chn}_8_13TeV_{year}.txt  htt_{chn}_9_13TeV_{year}=htt_{chn}_9_13TeV_{year}.txt  htt_{chn}_10_13TeV_{year}=htt_{chn}_10_13TeV_{year}.txt &> combined_{chn}_{year}.txt.cmb
    """.format(chn=chn,year=year)
else:
    cmd+="""combineCards.py htt_{chn}_1_13TeV_{year}=htt_{chn}_1_13TeV_{year}.txt htt_{chn}_2_13TeV_{year}=htt_{chn}_2_13TeV_{year}.txt htt_{chn}_3_13TeV_{year}=htt_{chn}_3_13TeV_{year}.txt htt_{chn}_4_13TeV_{year}=htt_{chn}_4_13TeV_{year}.txt htt_{chn}_5_13TeV_{year}=htt_{chn}_5_13TeV_{year}.txt htt_{chn}_6_13TeV_{year}=htt_{chn}_6_13TeV_{year}.txt &> combined_{chn}_{year}.txt.cmb\n\n\n""".format(chn=chn,year=year)
runCommand(cmd)

## Step 8
printer.blue("Delete one bad lnN")
cdDir(pdir)
cmd="""sed -i 's/ CMS_ggH_STXSVBF2j//g' {chn}/125/combined_{chn}_{year}.txt.cmb\n\n""".format(chn=chn,year=year)
runCommand(cmd)


## Step 9
printer.whiteBlueBold("Combine datacards 2")
if "fa3" in par and args.ggHInt is False:
    cmd="""combineTool.py -M T2W -m 125 -P HiggsAnalysis.CombinedLimit.FA3_Interference_JHU_ggHSyst_rw_MengsMuV:FA3_Interference_JHU_ggHSyst_rw_MengsMuV -i {chn}/125/combined_{chn}_{year}.txt.cmb -o fa03_Workspace_MengsMuV_{chn}_{year}.root""".format(chn=chn,year=year,inputString=inputString)
elif "fa3" in par and args.ggHInt is True:
    cmd="""combineTool.py -M T2W -m 125 -P HiggsAnalysis.CombinedLimit.FA3_Interference_JHU_ggHSyst_rw_MengsMuV_HeshyXsec_ggHInt:FA3_Interference_JHU_ggHSyst_rw_MengsMuV_HeshyXsec_ggHInt -i {chn}/125/combined_{chn}_{year}.txt.cmb -o fa03_Workspace_MengsMuV_{chn}_{year}.root""".format(chn=chn,year=year,inputString=inputString)
elif par=="fa2":
    cmd="""combineTool.py -M T2W -m 125 -P HiggsAnalysis.CombinedLimit.FA2_Interference_JHU_rw_MengsMuV:FA2_Interference_JHU_rw_MengsMuV -i {chn}/125/combined_{chn}_{year}.txt.cmb -o fa03_Workspace_MengsMuV_{chn}_{year}.root""".format(chn=chn,year=year,inputString=inputString)
elif par=="fL1":
    cmd="""combineTool.py -M T2W -m 125 -P HiggsAnalysis.CombinedLimit.FL1_Interference_JHU_rw_MengsMuV:FL1_Interference_JHU_rw_MengsMuV -i {chn}/125/combined_{chn}_{year}.txt.cmb -o fa03_Workspace_MengsMuV_{chn}_{year}.root""".format(chn=chn,year=year,inputString=inputString)
elif par=="fL1Zg":
    cmd="""combineTool.py -M T2W -m 125 -P HiggsAnalysis.CombinedLimit.FL1Zg_Interference_JHU_rw_MengsMuV:FL1Zg_Interference_JHU_rw_MengsMuV -i {chn}/125/combined_{chn}_{year}.txt.cmb -o fa03_Workspace_MengsMuV_{chn}_{year}.root""".format(chn=chn,year=year,inputString=inputString)
cmd+="\n\n"
runCommand(cmd)


## Step 10
printer.whiteBlueBold("Start 1D scan")
cmd="""combineTool.py -n 1D_scan_fa3_{chn}_{year}  -M MultiDimFit -m 125 --setParameterRanges muV=0.0,4.0:muf=0.0,10.0:fa3_ggH=0.,1.:CMS_zz4l_fai1=-0.00001,0.00001 {chn}/125/fa03_Workspace_MengsMuV_{chn}_{year}.root --algo=grid --points=101 --robustFit=1 --setRobustFitAlgo=Minuit2,Migrad -P CMS_zz4l_fai1 --floatOtherPOIs=1 --X-rtd FITTER_NEW_CROSSING_ALGO --setRobustFitTolerance=0.1 --X-rtd FITTER_NEVER_GIVE_UP --X-rtd FITTER_BOUND --cminFallbackAlgo \"Minuit2,0:1.\" --setParameters muV=1.,CMS_zz4l_fai1=0.,muf=1.,fa3_ggH=0.  -t -1 --freezeParameters allConstrainedNuisances\n\n""".format(chn=chn,year=year,inputString=inputString)
if "ggH" in par:
    cmd = cmd.replace("-P CMS_zz4l_fai1", "-P fa3_ggH", 1)
runCommand(cmd)



'''
## Step 11
printer.blue("\n\nPlot")
cdDir(initialDir)
#cmd="python plotAnomalousCoupling.py "+datacardsFolder+"/higgsCombine1D_scan_fa3_"+chn+"_"+str(year)+".MultiDimFit.mH125.root -p "+par+" -y "+str(year)+"\n\n"
cmd="python ./scripts/plot1DScan.py --main "+datacardsFolder+"/higgsCombine1D_scan_fa3_"+chn+"_"+str(year)+".MultiDimFit.mH125.root --POI CMS_zz4l_fai1 -o limits/"+par+"_"+str(year)+" --y-max 5.0 --x-title f_{a3}^{HVV} --main-label "+str(year)+"\n\n"
if "ggH" in par:
    cmd = cmd.replace("CMS_zz4l_fai1", "fa3_ggH", 1)
    cmd = cmd.replace("f_{a3}^{HVV}", "f_{a3}^{ggH}", 1)
    cmd = cmd.replace("--y-max 5.0", "--y-max 0.6", 1)
if "a2" in par:
    cmd = cmd.replace("f_{a3}^{HVV}", "f_{a2}^{HVV}", 1)
elif "L1Zg" in par:
    cmd = cmd.replace("f_{a3}^{HVV}", "f_{L1Zg}^{HVV}", 1)
elif "L1" in par:
    cmd = cmd.replace("f_{a3}^{HVV}", "f_{L1}^{HVV}", 1)
if "fa3noDCP" in inputString:
    cmd = cmd.replace("limits/", "limits/noDCP_", 1)
runCommand(cmd)



## Step 12
printer.whiteBlueBold("Unroll")
printer.blue("FitDiagnostics")
if "fa3" in input and "noDCP" not in input:
    printer.orange("Skip unrolled distribution plot...")
else:
    cmd = """combine -M FitDiagnostics output/{folderName}/tt/125/fa03_Workspace_MengsMuV_{chn}_{year}.root  -m 125  --setParameterRanges muV=0.0,4.0:muf=0.0,10.0:CMS_zz4l_fai1=-0.01,0.01 --robustFit=1 --setRobustFitAlgo=Minuit2,Migrad --X-rtd FITTER_NEW_CROSSING_ALGO --setRobustFitTolerance=0.1 --X-rtd FITTER_NEVER_GIVE_UP --X-rtd FITTER_BOUND --setParameters muV=1.,CMS_zz4l_fai1=0.,muf=1.,fa3_ggH=0.  -t -1 --expectSignal=1""".format(folderName=folderName,chn=chn,year=year)
    runCommand(cmd)

    printer.blue("PostFitShapesFromWorkspace")
    cmd = """PostFitShapesFromWorkspace -o output_shapes_{par}_{chn}_{year}.root -m 125 -f fitDiagnostics.root:fit_s --postfit --sampling --print -d output/{folderName}/{chn}/125/combined_{chn}_{year}.txt.cmb -w output/{folderName}/{chn}/125/fa03_Workspace_MengsMuV_{chn}_{year}.root""".format(par=par, chn=chn, year=year, folderName=folderName)
    runCommand(cmd)

    printer.blue("Copy rootfile")
    cmd = """cp output/{folderName}/{chn}/common/htt_input_{year}.root htt_input_{par}_{chn}_{year}.root""".format(par=par, chn=chn, year=year, folderName=folderName)
    runCommand(cmd)

    printer.blue("Draw plot")
    cmd = """python scripts/DrawUnrolled_Run2.py --variable {par} --channel {chn} --year {year}""".format(par=par, chn=chn, year=year)
    cmd = cmd.replace("--variable f", "--variable ", 1)
    runCommand(cmd)


## Step 13
if "fa3" in input and "noDCP" not in input and "fa3ggH" not in input:

    printer.whiteBlueBold("Run Impact")
    printer.blue("move to datacard folder")

    cdDir(datacardsFolder)

    printer.blue("Impact")
    cmd = """combineTool.py -M Impacts -d {chn}/125/fa03_Workspace_MengsMuV_{chn}_{year}.root -m 125 --doInitialFit --setParameterRanges muV=0.0,4.0:muf=0.0,10.0:fa3_ggH=0.,1.:CMS_zz4l_fai1=-1,1  --robustFit=1 --setRobustFitAlgo=Minuit2,Migrad --X-rtd FITTER_NEW_CROSSING_ALGO --setRobustFitTolerance=0.1 --X-rtd FITTER_NEVER_GIVE_UP --X-rtd FITTER_BOUND --cminFallbackAlgo \"Minuit2,0:1.\" --setParameters muV=1.,CMS_zz4l_fai1=0.25,muf=1.,fa3_ggH=0.  -t -1 --expectSignal=1""".format(chn=chn, year=year)
    runCommand(cmd)
    
    printer.blue("nice")
    cmd = """nice combineTool.py -M Impacts -d {chn}/125/fa03_Workspace_MengsMuV_{chn}_{year}.root -m 125 --doFits --parallel 8 -t -1 --setParameters muV=1.,fa3_ggH=0,CMS_zz4l_fai1=0.25,muf=1. --setParameterRanges muV=0.0,2.0:muf=0.0,10.0:fa3_ggH=0.,1.:CMS_zz4l_fai1=-1.,1. --setRobustFitAlgo=Minuit2,Migrad --robustFit=1 --X-rtd FITTER_NEW_CROSSING_ALGO --setRobustFitTolerance=0.1 --X-rtd FITTER_NEVER_GIVE_UP --X-rtd FITTER_BOUND --cminFallbackAlgo \"Minuit2,0:1.\"""".format(chn=chn, year=year)
    runCommand(cmd)
    
    printer.blue("nice")
    cmd = """combineTool.py -M Impacts -d {chn}/125/fa03_Workspace_MengsMuV_{chn}_{year}.root -m 125 -t -1 --setParameters muV=1.,fa3_ggH=0,CMS_zz4l_fai1=0.25,muf=1. --setParameterRanges muV=0.0,2.0:muf=0.0,10.0:fa3_ggH=0.,1.:CMS_zz4l_fai1=-1,1 --setRobustFitAlgo=Minuit2,Migrad --robustFit=1 --X-rtd FITTER_NEW_CROSSING_ALGO --setRobustFitTolerance=0.1 --X-rtd FITTER_NEVER_GIVE_UP --X-rtd FITTER_BOUND --cminFallbackAlgo \"Minuit2,0:1.\" -o impacts_p0p25_{chn}_{year}_{par}.json""".format(chn=chn, year=year, par=par)
    runCommand(cmd)
    
    printer.blue("Plot Impact")
    cmd = """plotImpacts.py -i impacts_p0p25_{chn}_{year}_{par}.json -o ../../impact/impacts_p0p25_{chn}_{year}_{par} --POI CMS_zz4l_fai1""".format(chn=chn, year=year, par=par)
    runCommand(cmd)

'''
