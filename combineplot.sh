# Hgg fa3 
python run_optimizedBin_limits_syst_combYears.py --emb=1 --inputString=2D_htt_tt_emb_sys_fa3ggH --useDCP=1 --par=fa3ggH --isGGH=1
python ./scripts/plot1DScan.py --main output/data_2D_htt_tt_emb_sys_fa3ggH/higgsCombine1D_scan_fa3_tt_2018.MultiDimFit.mH125.root --POI fa3_ggH -o limits/fa3ggH_all --y-max 1.0 --x-title f_{a3}^{ggH} --main-label "2018 " --others 'output/data_2D_htt_tt_emb_sys_fa3ggH/higgsCombine1D_scan_fa3_tt_2017.MultiDimFit.mH125.root:2017:2' 'output/data_2D_htt_tt_emb_sys_fa3ggH/higgsCombine1D_scan_fa3_tt_2016.MultiDimFit.mH125.root:2016:4' 'output/data_2D_htt_tt_emb_sys_fa3ggH/higgsCombine1D_scan_fa3_ggH_tt_all.MultiDimFit.mH125.root:Combined:1'

# HVV fa3
python run_optimizedBin_limits_syst_combYears.py --emb=1 --inputString=2D_htt_tt_emb_sys_fa3 --useDCP=1 --par=fa3
python ./scripts/plot1DScan.py --main output/data_2D_htt_tt_emb_sys_fa3/higgsCombine1D_scan_fa3_tt_2018.MultiDimFit.mH125.root --POI CMS_zz4l_fai1 -o limits/fa3_all --y-max 10.0 --x-title f_{a3}^{HVV} --main-label "2018 " --others 'output/data_2D_htt_tt_emb_sys_fa3/higgsCombine1D_scan_fa3_tt_2017.MultiDimFit.mH125.root:2017:2' 'output/data_2D_htt_tt_emb_sys_fa3/higgsCombine1D_scan_fa3_tt_2016.MultiDimFit.mH125.root:2016:4' 'output/data_2D_htt_tt_emb_sys_fa3/higgsCombine1D_scan_fa3_tt_all.MultiDimFit.mH125.root:Combined:1'


# HVV fa2
python run_optimizedBin_limits_syst_combYears.py --emb=1 --inputString=2D_htt_tt_emb_sys_fa2 --useDCP=0 --par=fa2
python ./scripts/plot1DScan.py --main output/data_2D_htt_tt_emb_sys_fa2/higgsCombine1D_scan_fa3_tt_2018.MultiDimFit.mH125.root --POI CMS_zz4l_fai1 -o limits/fa2_all --y-max 13.0 --x-title f_{a2}^{HVV} --main-label "2018 " --others 'output/data_2D_htt_tt_emb_sys_fa2/higgsCombine1D_scan_fa3_tt_2017.MultiDimFit.mH125.root:2017:2' 'output/data_2D_htt_tt_emb_sys_fa2/higgsCombine1D_scan_fa3_tt_2016.MultiDimFit.mH125.root:2016:4' 'output/data_2D_htt_tt_emb_sys_fa2/higgsCombine1D_scan_fa3_tt_all.MultiDimFit.mH125.root:Combined:1'

# HVV fL1
python run_optimizedBin_limits_syst_combYears.py --emb=1 --inputString=2D_htt_tt_emb_sys_fL1 --useDCP=0 --par=fL1
python ./scripts/plot1DScan.py --main output/data_2D_htt_tt_emb_sys_fL1/higgsCombine1D_scan_fa3_tt_2018.MultiDimFit.mH125.root --POI CMS_zz4l_fai1 -o limits/fL1_all --y-max 13.0 --x-title f_{L1}^{HVV} --main-label "2018 " --others 'output/data_2D_htt_tt_emb_sys_fL1/higgsCombine1D_scan_fa3_tt_2017.MultiDimFit.mH125.root:2017:2' 'output/data_2D_htt_tt_emb_sys_fL1/higgsCombine1D_scan_fa3_tt_2016.MultiDimFit.mH125.root:2016:4' 'output/data_2D_htt_tt_emb_sys_fL1/higgsCombine1D_scan_fa3_tt_all.MultiDimFit.mH125.root:Combined:1'

# HVV fL1Zg
python run_optimizedBin_limits_syst_combYears.py --emb=1 --inputString=2D_htt_tt_emb_sys_fL1Zg --useDCP=0 --par=fL1Zg
python ./scripts/plot1DScan.py --main output/data_2D_htt_tt_emb_sys_fL1Zg/higgsCombine1D_scan_fa3_tt_2018.MultiDimFit.mH125.root --POI CMS_zz4l_fai1 -o limits/fL1Zg_all --y-max 13.0 --x-title f_{L1Zg}^{HVV} --main-label "2018 " --others 'output/data_2D_htt_tt_emb_sys_fL1Zg/higgsCombine1D_scan_fa3_tt_2017.MultiDimFit.mH125.root:2017:2' 'output/data_2D_htt_tt_emb_sys_fL1Zg/higgsCombine1D_scan_fa3_tt_2016.MultiDimFit.mH125.root:2016:4' 'output/data_2D_htt_tt_emb_sys_fL1Zg/higgsCombine1D_scan_fa3_tt_all.MultiDimFit.mH125.root:Combined:1'
