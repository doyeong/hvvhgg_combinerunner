HTT HVV & HGG Anomalous Coupligs Combine
=========================================

## 1. Setting up CMSSW and Combine

### Set up a new CMSSW area
```
export SCRAM_ARCH=slc7_amd64_gcc700
scram pro -n HVVHGGCombine_10_2_13 CMSSW CMSSW_10_2_13
cd HVVHGGCombine_10_2_13/src
cmsenv
```

### Checkout the combine package

```
git clone https://github.com/cms-analysis/HiggsAnalysis-CombinedLimit.git HiggsAnalysis/CombinedLimit
cd HiggsAnalysis/CombinedLimit
cd $CMSSW_BASE/src/HiggsAnalysis/CombinedLimit
source env_standalone.sh 
make -j 8; make # second make fixes compilation error of first
cd $CMSSW_BASE/src
```

### Checkou CombineHarvester
This contains some high-level tools for working with combinecontains some high-level tools for working with combine.

Full documentation: http://cms-analysis.github.io/CombineHarvester
```
cd $CMSSW_BASE/src
git clone https://github.com/cms-analysis/CombineHarvester.git CombineHarvester
```

## 2. Getting this combine repsitory

### Clone the repository
```
cd CombineHarvester
git clone ssh://git@gitlab.cern.ch:7999/doyeong/hvvhgg_combinerunner.git
cd hvvhgg_combinerunner
source build.sh
```

### Compile
```
cd $CMSSW_BASE/src
scramv1 b clean; scramv1 b
```


## 3. How to run

### Datacards location
`build.sh` creates the area for raw datacards which have 2D distributions.

```
$CMSSW_BASE/src/auxiliaries/datacards
```

Put the raw datacards under the proper coupling folders with the following naming convention.

**2D_htt_<tt,mt,et,em>\_<emb,mc>\_<sys,nosys>\_f<a3,a2,L1,L1Zg><VBF,ggH>\_<2016,2017,2018>\_<tag\-optional>.root**

For example, 2D_htt_tt_emb_sys_fa3VBF_2016_Mar17.root


### Running combine

Once you run the following command lines, it'll give you the next command.
```
cd $CMSSW_BASE/src/CombineHarvester/hvvhgg_combinerunner
python merge_bins.py --channel <tt,mt,et,em> --nbins 64 --inputfile <datacard full path>
```

If the POI is fa2, fL1, or fL1Zg, this will give you **(1) limits scan** and **(2) unrolled distribution** plots.  
If you run for fa3 HVV, the command line will produce **(1) limits scan** and **(2) impact** plots.  
If it is fa3 ggH, you'll get only **limit scan**.   

In order to produce fa3 unrolled, do this first and treat is as if there is no DCP bins. The following command is going to automatically instruct you.
```
python scripts/fa3DCPbinRemover.py <datacard full path>
```

## 4. Detail of the scripts

### 1) merge_bins.py

The initial datacards has finer binning than what we actually use for fitting. 
By having finer binning, we are able to have a freedom to choose a number of different templates without re-run datacards maker.
The number of total bins (without DCP bins) in VBF categories are mandatory input variable. 
Depending on the number of bins and channel, NN_disc and D2jet bins are hardcodeded.
In this step 2D histograms are unrolled to 1D.  
`--inputfile`, `--nbins`, `--channel`

python scripts/checkingSignal.py -i testChecker.root -o test.root -g 0 -c tt -p 1 --poi L1