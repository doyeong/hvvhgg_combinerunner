#!/usr/bin/env python

import re, sys, os
from scripts import printer
import argparse

parser = argparse.ArgumentParser("Compare total template to stage 1.1 templates")
parser.add_argument(
    "--print","-p",
    action="store_true",
    dest="printTrue",
    default=False,
    help="print command line, do not run")
parser.add_argument(
    "--chn","-c",
    action="store",
    dest="chn",
    default="tt",
    help="Which channel is this ?")
parser.add_argument(
    "--input","-i",
    action="store",
    dest="input",
    default="dontknow",
    help="Input rootfile - after negative bins removing")
parser.add_argument(
    "--cmd",
    action="store",
    dest="runCmd",
    default="dontknow",
    help="running all the test ")
parser.add_argument(
    "--Ntoss","-n",
    action="store",
    dest="Ntoss",
    default="10",
    help="Number of toss")
parser.add_argument(
    "--par",
    action="store",
    dest="par",
    default="fa3",
    help="fa3,fa2,fL1,fL1Zg,fa3ggH")
args = parser.parse_args()


def runCommand(cmd):
    printer.gray(cmd)
    if args.printTrue is False: os.system(cmd)

input = args.input
channel = args.chn

if os.path.isdir("tossTemplates") is False:
    os.makedirs(os.path.join("tossTemplates"))
if os.path.isdir("output_toss") is False:
    os.makedirs(os.path.join("output_toss"))

#command = sys.argv[3]
# Create tossed templetes
for idx in range(1,int(args.Ntoss)):
    stridx = str(idx)
    output = "tossTemplates/"+input.replace(".root","_"+stridx+".root",1)

    printer.blue("Output : " + output)
    cmd = "python scripts/TossToys_VBF.py %s %s %s"%(input, output, channel)
    runCommand(cmd)
    
# copy the original with index of 0
cmd = "cp "+input+" "+"tossTemplates/"+input.replace(".root","_0.root",1)
runCommand(cmd)

# run 
file_list = os.listdir("tossTemplates")
plotCmd=""
for toy in file_list:
    if "root" not in toy: continue    
    cmd = "cp tossTemplates/"+toy+" "+input+"\n"
    cmd += args.runCmd
    runCommand(cmd)
    index=toy[toy.rfind("_"):toy.rfind(".root")]
    cmd = "mv output/data_2D_htt_tt_emb_sys_"+args.par+" output_toss/data_2D_htt_tt_emb_sys_"+args.par+index
    runCommand(cmd)
    if index!="0":
        plotCmd+=" \'output_toss/data_2D_htt_tt_emb_sys_"+args.par+index+"/higgsCombine1D_scan_fa3_tt_2018.MultiDimFit.mH125.root:2:3\'"

plotCmd = "\n\npython ./scripts/plot1DScan.py --main output_toss/data_2D_htt_tt_emb_sys_"+args.par+"_0/higgsCombine1D_scan_fa3_tt_2018.MultiDimFit.mH125.root --POI CMS_zz4l_fai1 -o output_toss/"+args.par+"_2018_toss --y-max 5.0 --x-title f_{"+args.par[1:]+"}^{HVV} --main-label 2018 --isToss --others "+plotCmd+"\n\n"

if "ggH" in args.par:
    plotCmd = plotCmd.replace("--POI CMS_zz4l_fai1", "--POI fa3_ggH")
    plotCmd = plotCmd.replace("--y-max 5.0", "--y-max 0.4")
    plotCmd = plotCmd.replace("f_{a3ggH}^{HVV}", "f_{a3}^{ggH}")
printer.blue(plotCmd)
