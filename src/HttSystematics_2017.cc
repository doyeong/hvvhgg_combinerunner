#include "CombineHarvester/hvvhgg_combinerunner/interface/HttSystematics_2017.h"
#include <vector>
#include <string>
#include "CombineHarvester/CombineTools/interface/Systematics.h"
#include "CombineHarvester/CombineTools/interface/Process.h"
#include "CombineHarvester/CombineTools/interface/Utilities.h"

using namespace std;

namespace ch {
    
  using ch::syst::SystMap;
  using ch::syst::SystMapAsymm;
  using ch::syst::era;
  using ch::syst::channel;
  using ch::syst::bin_id;
  using ch::syst::process;
  using ch::syst::bin;
  using ch::JoinStr;
  
  void AddSMRun2Systematics_2017(CombineHarvester & cb, int tt_cate_count, int mt_cate_count, int control_region, bool do_shapeSyst, int year, bool mm_fit, bool ttbar_fit) {
    std::vector<std::string> sig_procs = {"reweighted_ggH_htt_0PM","reweighted_ggH_htt_0M", "reweighted_ggH_htt_0Mf05ph0",//"GGH2Jets_sm_M","GGH2Jets_pseudoscalar_M",
					  "reweighted_qqH_htt_0PM","reweighted_qqH_htt_0M","reweighted_qqH_htt_0Mf05ph0",
					  "reweighted_WH_htt_0PM","reweighted_WH_htt_0M","reweighted_WH_htt_0Mf05ph0",
					  "reweighted_ZH_htt_0PM","reweighted_ZH_htt_0M","reweighted_ZH_htt_0Mf05ph0"};
    std::vector<std::string> sig_procs_hww = {"ggH_hww125","qqH_hww125","WH_hww125","ZH_hww125"};
    std::vector<std::string> sig_procs_recoil = {"reweighted_ggH_htt_0PM","reweighted_ggH_htt_0M", "reweighted_ggH_htt_0Mf05ph0",
						 "reweighted_qqH_htt_0PM","reweighted_qqH_htt_0M","reweighted_qqH_htt_0Mf05ph0"};
    std::vector<std::string> sig_procs_ggh = {"reweighted_ggH_htt_0PM","reweighted_ggH_htt_0M","reweighted_ggH_htt_0Mf05ph0"};
    std::vector<std::string> sig_procs_vbf = {"reweighted_qqH_htt_0PM","reweighted_qqH_htt_0M","reweighted_qqH_htt_0Mf05ph0"};
    std::vector<std::string> sig_procs_wh = {"reweighted_WH_htt_0PM","reweighted_WH_htt_0M","reweighted_WH_htt_0Mf05ph0"};
    std::vector<std::string> sig_procs_zh = {"reweighted_ZH_htt_0PM","reweighted_ZH_htt_0M","reweighted_ZH_htt_0Mf05ph0"};
    
    std::vector<std::string> all_mc_bkgs = {"ZL","TTL","VVL","STL",
					    "VV","ZJ","EWKZ","W", "ggH_hww125", "qqH_hww125", "WH_hww125", "ZH_hww125"};   
    std::vector<std::string> all_mc_bkgs_recoil = {"ZL","ZJ","W"};
    
    //Luminosity Uncertainty
    // https://twiki.cern.ch/twiki/bin/view/CMS/TWikiLUM#LumiComb
    cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).AddSyst(cb, "lumi_13TeV_2017", "lnN", SystMap<>::init(1.020));
    cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).AddSyst(cb, "lumi_13TeV_XY", "lnN", SystMap<>::init(1.008));
    cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).AddSyst(cb, "lumi_13TeV_LS", "lnN", SystMap<>::init(1.003));
    cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).AddSyst(cb, "lumi_13TeV_BBD", "lnN", SystMap<>::init(1.004));
    cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).AddSyst(cb, "lumi_13TeV_DB", "lnN", SystMap<>::init(1.005));
    cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).AddSyst(cb, "lumi_13TeV_BCC", "lnN", SystMap<>::init(1.003));
    cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).AddSyst(cb, "lumi_13TeV_GS", "lnN", SystMap<>::init(1.001));								
    
    // b-tagging efficiency: Changed into lnN.
    //5% in ttbar and 0.5% otherwise.
    cb.cp().process({"TTL","STL"}).AddSyst(cb,"CMS_htt_eff_b_TTL","lnN",SystMap<>::init(1.05));
    cb.cp().process(JoinStr({{"ZL","VVL"},sig_procs})).AddSyst(cb,"CMS_htt_eff_b","lnN",SystMap<>::init(1.005));
    
    // XSection Uncertainties
    cb.cp().process({"TTL"}).AddSyst(cb,"CMS_htt_tjXsec", "lnN", SystMap<>::init(1.042));
    cb.cp().process({"VVL"}).AddSyst(cb,"CMS_htt_vvXsec", "lnN", SystMap<>::init(1.05));
    cb.cp().process({"STL"}).AddSyst(cb,"CMS_htt_stXsec", "lnN", SystMap<>::init(1.05));
    cb.cp().process({"ZL"}).AddSyst(cb,"CMS_htt_zjXsec", "lnN", SystMap<>::init(1.02));

    // PDF
    cb.cp().process(JoinStr({sig_procs_wh,{"WH_hww125"}})).AddSyst(cb, "pdf_Higgs_VH", "lnN", SystMap<>::init(1.018));
    cb.cp().process(JoinStr({sig_procs_zh,{"ZH_hww125"}})).AddSyst(cb, "pdf_Higgs_VH", "lnN", SystMap<>::init(1.013));
    cb.cp().process(JoinStr({sig_procs_ggh,{"ggH_hww125"}})).AddSyst(cb, "pdf_Higgs_gg", "lnN", SystMap<>::init(1.032));
    cb.cp().process(JoinStr({sig_procs_vbf,{"qqH_hww125"}})).AddSyst(cb, "pdf_Higgs_qq", "lnN", SystMap<>::init(1.021));

    // Uncertainty on BR for HTT @ 125 GeV
    cb.cp().process(sig_procs).AddSyst(cb,"BR_htt_THU", "lnN", SystMap<>::init(1.017));
    cb.cp().process(sig_procs).AddSyst(cb,"BR_htt_PU_mq", "lnN", SystMap<>::init(1.0099));
    cb.cp().process(sig_procs).AddSyst(cb,"BR_htt_PU_alphas", "lnN", SystMap<>::init(1.0062));

    // Uncertainty on looser DNN ID vs lep
    cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).AddSyst(cb,"CMS_tauideff_vslepton_looser_2017", "lnN", SystMap<>::init(1.090));
    
    // jetFakes 0jet
    cb.cp().process({"jetFakes"}).bin({"tt_0jet"}).AddSyst(cb,"CMS_jetFakesNorm_0jet_tt_2017","lnN",SystMap<>::init(1.05));

    // signal generator (JHU vs Powheg)
    cb.cp().process(JoinStr({sig_procs_vbf, sig_procs_wh, sig_procs_zh})).channel({"tt"}).AddSyst(cb,"JHUvsPow","lnN",SystMap<>::init(1.05));
    cb.cp().process(JoinStr({sig_procs_vbf, sig_procs_wh, sig_procs_zh})).channel({"em"}).AddSyst(cb,"JHUvsPow","lnN",SystMap<>::init(1.022));

    // emu ID/2% for electron ID, 1% for muon, and 4% for trigger (2% per trigger leg) for both MC and Embedded.
    cb.cp().process(JoinStr({sig_procs, all_mc_bkgs, {"embedded"}})).channel({"em"}).AddSyst(cb,"CMS_eff_e_2017","lnN",SystMap<>::init(1.020));
    cb.cp().process(JoinStr({sig_procs, all_mc_bkgs, {"embedded"}})).channel({"em"}).AddSyst(cb,"CMS_eff_m_2017","lnN",SystMap<>::init(1.010));
    cb.cp().process(JoinStr({sig_procs, all_mc_bkgs, {"embedded"}})).channel({"em"}).AddSyst(cb,"CMS_htt_emutrg_2017","lnN",SystMap<>::init(1.040));




    if (do_shapeSyst){
      //************************//
      // MC shape uncertainties //
      //************************//
      std::cout<<"Adding Shapes..."<<std::endl;
      // Prefiring
      std::cout<<"\tprefiring"<<std::endl;
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt","em"}).AddSyst(cb, "CMS_prefiring", "shape", SystMap<>::init(1.00));
      // Tau ID eff in DM bins
      std::cout<<"\tTau ID eff"<<std::endl;
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt"}).AddSyst(cb, "CMS_tauideff_dm0_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt"}).AddSyst(cb, "CMS_tauideff_dm1_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt"}).AddSyst(cb, "CMS_tauideff_dm10_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt"}).AddSyst(cb, "CMS_tauideff_dm11_2017", "shape", SystMap<>::init(1.00));
      // Tau trigger eff
      std::cout<<"\tTau trigger eff"<<std::endl;
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt"}).AddSyst(cb, "CMS_doubletautrg_dm0_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt"}).AddSyst(cb, "CMS_doubletautrg_dm1_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt"}).AddSyst(cb, "CMS_doubletautrg_dm10_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt"}).AddSyst(cb, "CMS_doubletautrg_dm11_2017", "shape", SystMap<>::init(1.00));
      // Fake factors
      std::cout<<"\tFakes shapes"<<std::endl;
      cb.cp().process({"jetFakes"}).bin({"tt_0jet"}).channel({"tt"}).AddSyst(cb, "CMS_rawFF_tt_qcd_0jet_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process({"jetFakes"}).bin({"tt_boosted"}).channel({"tt"}).AddSyst(cb, "CMS_rawFF_tt_qcd_1jet_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process({"jetFakes"}).
	bin({"tt_boosted",
	      "tt_vbf_ggHMELA_bin1_DCPm","tt_vbf_ggHMELA_bin1_DCPp", "tt_vbf_ggHMELA_bin1",
	      "tt_vbf_ggHMELA_bin2_DCPm","tt_vbf_ggHMELA_bin2_DCPp", "tt_vbf_ggHMELA_bin2",
	      "tt_vbf_ggHMELA_bin3_DCPm","tt_vbf_ggHMELA_bin3_DCPp", "tt_vbf_ggHMELA_bin3",
	      "tt_vbf_ggHMELA_bin4_DCPm","tt_vbf_ggHMELA_bin4_DCPp", "tt_vbf_ggHMELA_bin4"}).
	channel({"tt"}).AddSyst(cb, "CMS_rawFF_tt_qcd_2jet_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process({"jetFakes"}).bin({"tt_0jet"}).channel({"tt"}).AddSyst(cb, "CMS_FF_closure_tau2pt_tt_qcd_0jet", "shape", SystMap<>::init(1.00));
      cb.cp().process({"jetFakes"}).bin({"tt_boosted"}).channel({"tt"}).AddSyst(cb, "CMS_FF_closure_tau2pt_tt_qcd_1jet", "shape", SystMap<>::init(1.00));
      cb.cp().process({"jetFakes"}).
	bin({"tt_boosted",
	      "tt_vbf_ggHMELA_bin1_DCPm","tt_vbf_ggHMELA_bin1_DCPp", "tt_vbf_ggHMELA_bin1",
	      "tt_vbf_ggHMELA_bin2_DCPm","tt_vbf_ggHMELA_bin2_DCPp", "tt_vbf_ggHMELA_bin2",
	      "tt_vbf_ggHMELA_bin3_DCPm","tt_vbf_ggHMELA_bin3_DCPp", "tt_vbf_ggHMELA_bin3",
	      "tt_vbf_ggHMELA_bin4_DCPm","tt_vbf_ggHMELA_bin4_DCPp", "tt_vbf_ggHMELA_bin4"}).
	channel({"tt"}).AddSyst(cb, "CMS_FF_closure_tau2pt_tt_qcd_2jet", "shape", SystMap<>::init(1.00));
      cb.cp().process({"jetFakes"}).channel({"tt"}).AddSyst(cb, "CMS_FF_closure_tt_qcd_osss_2017", "shape", SystMap<>::init(1.00));

      // MET Unclustered Energy Scale      
      std::cout<<"\tUnclustered MET"<<std::endl;
      cb.cp().process({"TTL", "VVL", "STL"}).channel({"tt"}).AddSyst(cb, "CMS_scale_met_unclustered_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process({"TTT","TTL","VVT","STT","VVL","STL"}).channel({"et","mt","em"}).AddSyst(cb, "CMS_scale_met_unclustered_2017", "shape", SystMap<>::init(1.00));
      
      // ZpT reweighting 
      std::cout<<"\tZpT reweighting"<<std::endl;
      cb.cp().process({"ZL"}).channel({"tt","em"}).AddSyst(cb, "CMS_htt_dyShape", "shape", SystMap<>::init(1.00));
      // Top pT reweighting 
      std::cout<<"\tTop pT reweighting"<<std::endl;
      cb.cp().process({"TTL"}).channel({"tt","em"}).AddSyst(cb, "CMS_htt_ttbarShape", "shape", SystMap<>::init(1.00));
      // TES uncertainty 
      std::cout<<"\tTau Energy Scale"<<std::endl;
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt"}).AddSyst(cb, "CMS_scale_t_1prong_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt"}).AddSyst(cb, "CMS_scale_t_1prong1pizero_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt"}).AddSyst(cb, "CMS_scale_t_3prong_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt"}).AddSyst(cb, "CMS_scale_t_3prong1pizero_2017", "shape", SystMap<>::init(1.00));
      // JES uncertainty
      std::cout<<"\tJet Energy Scale"<<std::endl;
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt","em"}).AddSyst(cb, "CMS_scale_j_Absolute", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt","em"}).AddSyst(cb, "CMS_scale_j_Absolute_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt","em"}).AddSyst(cb, "CMS_scale_j_BBEC1", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt","em"}).AddSyst(cb, "CMS_scale_j_BBEC1_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt","em"}).AddSyst(cb, "CMS_scale_j_EC2", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt","em"}).AddSyst(cb, "CMS_scale_j_EC2_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt","em"}).AddSyst(cb, "CMS_scale_j_FlavorQCD", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt","em"}).AddSyst(cb, "CMS_scale_j_HF", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt","em"}).AddSyst(cb, "CMS_scale_j_HF_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt","em"}).AddSyst(cb, "CMS_scale_j_RelativeSample_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt","em"}).AddSyst(cb, "CMS_scale_j_RelativeBal", "shape", SystMap<>::init(1.00));
      // JER uncertainty
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"tt"}).AddSyst(cb, "CMS_res_j_2017", "shape", SystMap<>::init(1.00));

      // Electron Energy scale
      std::cout<<"\tElectron Energy Scale"<<std::endl;
      cb.cp().process(JoinStr({sig_procs, all_mc_bkgs})).channel({"em"}).AddSyst(cb, "CMS_scale_e", "shape", SystMap<>::init(1.00));
      // Recoil uncertainty
      std::cout<<"\tRecoil uncertainty"<<std::endl;
      cb.cp().process(JoinStr({{"ZL"}, sig_procs_vbf, sig_procs_ggh})).bin({"tt_0jet"}).channel({"tt","em"}).AddSyst(cb, "CMS_htt_boson_reso_met_0jet_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({{"ZL"}, sig_procs_vbf, sig_procs_ggh})).bin({"tt_0jet"}).channel({"tt","em"}).AddSyst(cb, "CMS_htt_boson_scale_met_0jet_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({{"ZL"}, sig_procs_vbf, sig_procs_ggh})).bin({"tt_boosted"}).channel({"tt","em"}).AddSyst(cb, "CMS_htt_boson_reso_met_1jet_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({{"ZL"}, sig_procs_vbf, sig_procs_ggh})).bin({"tt_boosted"}).channel({"tt","em"}).AddSyst(cb, "CMS_htt_boson_scale_met_1jet_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({{"ZL"}, sig_procs_vbf, sig_procs_ggh})).
	bin({"tt_boosted",
	      "tt_vbf_ggHMELA_bin1_DCPm","tt_vbf_ggHMELA_bin1_DCPp", "tt_vbf_ggHMELA_bin1",
	      "tt_vbf_ggHMELA_bin2_DCPm","tt_vbf_ggHMELA_bin2_DCPp", "tt_vbf_ggHMELA_bin2",
	      "tt_vbf_ggHMELA_bin3_DCPm","tt_vbf_ggHMELA_bin3_DCPp", "tt_vbf_ggHMELA_bin3",
	      "tt_vbf_ggHMELA_bin4_DCPm","tt_vbf_ggHMELA_bin4_DCPp", "tt_vbf_ggHMELA_bin4"}).
	channel({"tt","em"}).AddSyst(cb, "CMS_htt_boson_reso_met_2jet_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process(JoinStr({{"ZL"}, sig_procs_vbf, sig_procs_ggh})).
	bin({"tt_boosted",
	      "tt_vbf_ggHMELA_bin1_DCPm","tt_vbf_ggHMELA_bin1_DCPp", "tt_vbf_ggHMELA_bin1",
	      "tt_vbf_ggHMELA_bin2_DCPm","tt_vbf_ggHMELA_bin2_DCPp", "tt_vbf_ggHMELA_bin2",
	      "tt_vbf_ggHMELA_bin3_DCPm","tt_vbf_ggHMELA_bin3_DCPp", "tt_vbf_ggHMELA_bin3",
	      "tt_vbf_ggHMELA_bin4_DCPm","tt_vbf_ggHMELA_bin4_DCPp", "tt_vbf_ggHMELA_bin4"}).
	channel({"tt","em"}).AddSyst(cb, "CMS_htt_boson_scale_met_2jet_2017", "shape", SystMap<>::init(1.00));

      // ggH WG1 theory
      cb.cp().process(JoinStr({sig_procs_ggh})).channel({"tt","em"}).AddSyst(cb, "THU_ggH_Mu", "shape",  SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs_ggh})).channel({"tt","em"}).AddSyst(cb, "THU_ggH_Res", "shape",  SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs_ggh})).channel({"tt","em"}).AddSyst(cb, "THU_ggH_Mig01", "shape",  SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs_ggh})).channel({"tt","em"}).AddSyst(cb, "THU_ggH_Mig12", "shape",  SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs_ggh})).channel({"tt","em"}).AddSyst(cb, "THU_ggH_VBF2j", "shape",  SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs_ggh})).channel({"tt","em"}).AddSyst(cb, "THU_ggH_VBF3j", "shape",  SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs_ggh})).channel({"tt","em"}).AddSyst(cb, "THU_ggH_qmtop", "shape",  SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs_ggh})).channel({"tt","em"}).AddSyst(cb, "THU_ggH_PT60", "shape",  SystMap<>::init(1.00));
      cb.cp().process(JoinStr({sig_procs_ggh})).channel({"tt","em"}).AddSyst(cb, "THU_ggH_PT120", "shape",  SystMap<>::init(1.00));



      //************************//
      // Embedded uncertainties //
      //************************//
      cb.cp().process({"embedded"}).AddSyst(cb, "CMS_htt_doublemutrg_2017", "lnN", SystMap<>::init(1.04));
      //cb.cp().process({"embedded"}).channel({"tt","em"}).AddSyst(cb, "CMS_htt_emb_ttbar", "shape", SystMap<>::init(1.00));
      // Tau ID eff
      cb.cp().process({"embedded"}).channel({"tt"}).AddSyst(cb, "CMS_eff_t_embedded_dm0_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process({"embedded"}).channel({"tt"}).AddSyst(cb, "CMS_eff_t_embedded_dm1_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process({"embedded"}).channel({"tt"}).AddSyst(cb, "CMS_eff_t_embedded_dm10_2017", "shape", SystMap<>::init(1.00));
      cb.cp().process({"embedded"}).channel({"tt"}).AddSyst(cb, "CMS_eff_t_embedded_dm11_2017", "shape", SystMap<>::init(1.00));
      // TES uncertainty
      cb.cp().process({"embedded"}).channel({"tt"}).AddSyst(cb, "CMS_scale_emb_t_1prong_2017", "shape", SystMap<>::init(0.866));
      cb.cp().process({"embedded"}).channel({"tt"}).AddSyst(cb, "CMS_scale_emb_t_1prong1pizero_2017", "shape", SystMap<>::init(0.866));
      cb.cp().process({"embedded"}).channel({"tt"}).AddSyst(cb, "CMS_scale_emb_t_3prong_2017", "shape", SystMap<>::init(0.866));
      cb.cp().process({"embedded"}).channel({"tt"}).AddSyst(cb, "CMS_scale_emb_t_3prong1pizero_2017", "shape", SystMap<>::init(0.866));
      cb.cp().process({"embedded"}).channel({"tt"}).AddSyst(cb, "CMS_scale_t_1prong_2017", "shape", SystMap<>::init(0.500));
      cb.cp().process({"embedded"}).channel({"tt"}).AddSyst(cb, "CMS_scale_t_1prong1pizero_2017", "shape", SystMap<>::init(0.500));
      cb.cp().process({"embedded"}).channel({"tt"}).AddSyst(cb, "CMS_scale_t_1prong_2017", "shape", SystMap<>::init(0.500));
      cb.cp().process({"embedded"}).channel({"tt"}).AddSyst(cb, "CMS_scale_t_1prong1pizero_2017", "shape", SystMap<>::init(0.500));
      // Trigger uncertainty
      cb.cp().process({"embedded"}).channel({"tt"}).AddSyst(cb, "CMS_doubletautrg_emb_dm0_2017", "shape", SystMap<>::init(0.866));
      cb.cp().process({"embedded"}).channel({"tt"}).AddSyst(cb, "CMS_doubletautrg_emb_dm1_2017", "shape", SystMap<>::init(0.866));
      cb.cp().process({"embedded"}).channel({"tt"}).AddSyst(cb, "CMS_doubletautrg_emb_dm10_2017", "shape", SystMap<>::init(0.866));
      cb.cp().process({"embedded"}).channel({"tt"}).AddSyst(cb, "CMS_doubletautrg_emb_dm11_2017", "shape", SystMap<>::init(0.866));
      cb.cp().process({"embedded"}).channel({"tt"}).AddSyst(cb, "CMS_doubletautrg_dm0_2017", "shape", SystMap<>::init(0.500));
      cb.cp().process({"embedded"}).channel({"tt"}).AddSyst(cb, "CMS_doubletautrg_dm1_2017", "shape", SystMap<>::init(0.500));
      cb.cp().process({"embedded"}).channel({"tt"}).AddSyst(cb, "CMS_doubletautrg_dm10_2017", "shape", SystMap<>::init(0.500));
      cb.cp().process({"embedded"}).channel({"tt"}).AddSyst(cb, "CMS_doubletautrg_dm11_2017", "shape", SystMap<>::init(0.500));    
      // Uncertainty on looser DNN ID vs lep
      cb.cp().process({"embedded"}).AddSyst(cb,"CMS_eff_t_embedded_vslepton_looser_2017", "lnN", SystMap<>::init(1.187489));    
      cb.cp().process({"embedded"}).AddSyst(cb,"CMS_tauideff_vslepton_looser_2017", "lnN", SystMap<>::init(1.0625));    
    }
  }
}
