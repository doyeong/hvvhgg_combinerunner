# Move BinByBin.cc to CombineTool
# Decorrelation of systematics between two DCP bins for non-interference signal processes and all background are included in `BinByBin.cc`.
mv BinByBin.cc ../CombineTools/src/.
# Copy signal models to CombineLimit
cp SignalModels/* ../../HiggsAnalysis/CombinedLimit/python/.

# Creat place for datacards
mkdir ../../auxiliaries
mkdir ../../auxiliaries/datacards
mkdir shapes/USCMS

# Creat folder for impact
mkdir limits
mkdir impact
mkdir unrolled
