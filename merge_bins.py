#!/usr/bin/env python
import ROOT
from ROOT import *
import re, sys, os
from array import array
from optparse import OptionParser
from scripts import printer
import argparse
import time


start = time.time()
parser = argparse.ArgumentParser("Merging fine binning in original datacards")
parser.add_argument(
        "--inputfile","-i",
        action="store",
        dest="inputfile",
        default="../../auxiliaries/datacards/2D_htt_tt_emb_sys_fa3ggH_2016.root",
        help="path of root file")
parser.add_argument(
        "--nbins","-n",
        action="store",
        dest="nbins",
        default=24,
        help="Number of bins are hard coded: 64/48/24")
parser.add_argument(
        "--channel","-c",
        action="store",
        dest="channel",
        default="tt",
        help="tt/mt/et/em")
parser.add_argument(
        "--usePowhegHVV",
        action="store_true",
        dest="usePowhegHVV",
        default=False,
        help="usePowhegHVV signals")
args = parser.parse_args()

nbins=int(args.nbins)
#inputfile = "../../auxiliaries/datacards/"+args.inputfile
inputfile = args.inputfile
if os.path.isfile(inputfile) is False:
        printer.warning(inputfile+" doesn't exist!!")
        sys.exit(0)
                        
pureRoofileName = inputfile[inputfile.rfind("/")+1:]
if "htt_tt" in pureRoofileName: channel = "tt"
elif "htt_mt" in pureRoofileName: channel = "mt"
elif "htt_et" in pureRoofileName: channel = "et"
elif "htt_em" in pureRoofileName: channel = "em"


outputfile=inputfile[:inputfile.rfind(".root")]+"_mergedBins.root"
year="_0000"
pureInputFileName = inputfile[inputfile.rfind("/"):]
print pureInputFileName
if "2016" in pureInputFileName : year="_2016"
elif "2017" in pureInputFileName : year="_2017"
elif "2018" in pureInputFileName : year="_2018"

# moreBins
binsNN=[]
binsD2jet=[]

# tt
if channel == 'tt' and not ("_fa3ggH_" in inputfile):
        #binsNN=[0.000,0.125,0.250,0.375,0.500,0.750,1.000]        
        binsNN=[0.000,0.125,0.250,0.375,0.500,0.625,0.750,0.875,1.000]
        binsD2jet=[0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
        printer.gray("NN binning:\t"+str(binsNN)+ "\nD2jet binning:\t"+str(binsD2jet))
if channel == 'tt' and ("_fa3xggH_" in inputfile):
        binsNN=[0.000,1.000]
        binsD2jet=[0.0,1.0]
        printer.gray("NN binning:\t"+str(binsNN)+ "\nD2jet binning:\t"+str(binsD2jet))
elif channel == 'em':
        #binsNN=[0.000,0.125,0.250,0.375,0.500,0.750,1.000]        
        #binsD2jet=[0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
        #binsD2jet=[0.000,0.125,0.250,0.375,0.500,0.625,0.750,0.875,1.000]
        #binsNN=[0.0,0.1001,0.2001,0.3001,0.4001,0.5001,0.6001,0.7001,0.8001,0.9001,1.0]

        binsD2jet=[0.000,0.250,0.500,0.750,1.000]
        binsNN=[0.0,0.1001,0.2001,0.3001,0.4001,0.5001,0.6001,0.7001,0.8001,0.9001,1.0]
        printer.gray("NN binning:\t"+str(binsNN)+ "\nD2jet binning:\t"+str(binsD2jet))
elif channel == 'mt' :
        binsNN=[0,0.05,0.1,0.15,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.85,0.9,0.95,1.0]
        binsD2jet=[0.000,0.125,0.250,0.375,0.500,0.625,0.750,0.875,1.000]
        printer.gray("NN binning:\t"+str(binsNN)+ "\nD2jet binning:\t"+str(binsD2jet))
'''
if nbins==24 and channel == 'tt' and ("_fa3ggH_" in inputfile):
        binsNN=[0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
        #binsNN=[0.0,0.2,0.4,0.6,0.8,1.0] #binsNN=[0.,0.4,0.7,1.]#####
        #binsD2jet=[0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0] #binsD2jet=[0.,0.5,1.0]
        #binsNN=[0.0,0.1,0.4,0.7,1.0] ##[0.0,0.2,0.6,0.75,1.0] for 2016 
        #binsNN=[0.0,0.05,0.3,0.7,1.0]
        #binsD2jet=[0.0,0.15,0.25,0.35,0.4,0.5,0.6,0.75,0.85,1.0]
        binsD2jet=[0.0,0.2,0.3,0.4,0.5,0.6,0.7,0.8,1.0]
        #binsD2jet=[0.0,0.3,0.5,0.7,1.0]
'''
file=ROOT.TFile(inputfile,"r")
dirList = []
for dir in file.GetListOfKeys():
        #print(dir.GetName())
        dirList.append(dir.GetName())
file.Close()

ofile=ROOT.TFile(outputfile,"recreate")
printer.gray("output:\t\t" + outputfile)
categories_list= file.GetListOfKeys()
categories=[]

def rename_histo(name_histo):
        name_histo=name_histo.replace("ggH_sm_htt125", "reweighted_ggH_htt_0PM125")
        name_histo=name_histo.replace("ggH_ps_htt125", "reweighted_ggH_htt_0M125")
        name_histo=name_histo.replace("ggH_mm_htt125", "reweighted_ggH_htt_0Mf05ph0125")
        name_histo=name_histo.replace("ggh125_powheg", "ggH125")
        #name_histo=name_histo.replace("reweighted_ggH_htt_0PM","GGH2Jets_sm_M")
        #name_histo=name_histo.replace("reweighted_ggH_htt_0Mf05ph0","GGH2Jets_pseudoscalar_Mf05ph0")
        #name_histo=name_histo.replace("reweighted_ggH_htt_0M","GGH2Jets_pseudoscalar_M")
        name_histo=name_histo.replace("reweighted_qqH","JHU_qqH")
        name_histo=name_histo.replace("reweighted_WH","JHU_WH")
        name_histo=name_histo.replace("reweighted_ZH","JHU_ZH")
        name_histo=name_histo.replace("EmbedZTT","embedded")
        name_histo=name_histo.replace("vbf125_powheg","VBF125")#reweighted_qqH_htt_0Mf05ph0125")
        #name_histo=name_histo.replace("VBF125","reweighted_qqH_htt_0Mf05ph0125")
        name_histo=name_histo.replace("zh125_powheg","ZH125")#reweighted_ZH_htt_0Mf05ph0125")
        #name_histo=name_histo.replace("ZH125","reweighted_ZH_htt_0Mf05ph0125")
        name_histo=name_histo.replace("wh125_powheg","WH125")#reweighted_WH_htt_0Mf05ph0125")
        #name_histo=name_histo.replace("WH125","reweighted_WH_htt_0Mf05ph0125")

        name_histo=name_histo.replace("TTT","TTL")
        name_histo=name_histo.replace("VVT","VVL")
        return name_histo

dannyDir = {
        'tt_2018_0jet':'sync_0jet',
        'tt_2018_boosted':'sync_boosted',
        'tt_2018_dijet_loosemjj_boosted':'sync_vbf_lmjj_boost',
        'tt_2018_dijet_loosemjj_lowboost':'sync_vbf_lmjj_lboost',
        'tt_2018_dijet_tightmjj_boosted':'sync_vbf_tmjj_boost',
        'tt_2018_dijet_tightmjj_lowboost':'sync_vbf_tmjj_lboost'
}

def usePowheg(name_histo, histo, mydir):
        sigDic = {"VBF125":"qqH", "WH125":"WH", "ZH125":"ZH"}
        ACpoints = ["0PM", "0M", "0Mf05ph0"]
        for ACpoint in ACpoints:
                if name_histo in sigDic:
                        newHistoName = "reweighted_%s_htt_%s125"%(sigDic[name_histo],ACpoint)
                        mydir.cd()
                        printer.info("Copy Powheg histogram %s to %s"%(name_histo, newHistoName))
                        histo.Write(newHistoName)

for k2 in categories_list:
	categories.append(k2.GetName())

ncat=len(categories)
for dir in dirList: 
    print(">> "+dir)
    file=ROOT.TFile(inputfile,"r")
    h1 = file.Get(dir)
    nom=dir#.GetName()
    h1.cd()
    histoList = gDirectory.GetListOfKeys()
    mydir=ofile.mkdir(dannyDir[nom]) if nom in dannyDir.keys() else ofile.mkdir(nom)
    printer.blue("dir: "+nom+"\t is merging and saved as "+mydir.GetName()+"\n")
    N_histo=0

    if nbins==24 and channel == 'tt' and ("_fa3ggH_" in inputfile):
            if "2018" in year:
                    binsD2jet=[0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
                    #binsD2jet=[0.0,0.2,0.4,0.6,0.8,1.0]
            else:
                    #binsD2jet=[0.0,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
                    binsD2jet=[0.0,0.2,0.4,0.6,0.8,1.0]
            if "tt_vbf_ggHMELA_bin1" in nom:                    
                    #binsNN=[0.0, 0.05, 0.2, 0.4, 0.75, 1.0] ## Best2018                            
                    if "2018" in year:
                            #binsNN=[0.0, 0.1, 0.4, 0.75, 1.0]
                            #binsNN=[0.0, 0.1, 0.4, 0.6, 1.0]
                            binsNN=[0.0, 0.05, 0.5, 0.75, 1.0]
                    else:
                            #binsNN=[0.0, 0.1, 0.35, 1.0]# [0.0, 0.05, 0.2, 0.3, 0.45, 1.0]                   
                            binsNN=[0.0, 0.05, 0.5, 0.75, 1.0]
                            #binsD2jet=[0.0,0.2,0.4,0.6,0.8,1.0]
                            binsD2jet=[0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
            elif "tt_vbf_ggHMELA_bin2" in nom :
                    #binsNN=[0.0, 0.05, 0.1, 0.2, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0] ## Best2018
                    if "2018" in year:
                            #binsNN=[0.0, 0.1, 0.5, 0.9, 1.0]
                            #binsNN=[0.0, 0.1, 0.5, 0.8, 1.0]
                            #binsNN=[0.0,0.01,0.05,0.1,0.2,0.4,0.6,0.8,1.0]
                            binsNN=[0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.8,1.0]
                            binsD2jet=[0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
                    elif "2017" in year:
                            #binsNN=[0.0, 0.1, 0.5, 1.0]
                            #binsNN=[0.0,0.01,0.05,0.1,0.2,0.4,0.6,0.8,1.0]
                            #binsNN=[0.0,0.01,0.05,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
                            #binsNN=[0.0,0.01,0.05,0.1,0.2,0.4,0.6,0.8,1.0]
                            #binsNN=[0.0,0.01,0.05,0.1,0.2,0.5,0.750,0.875,1.0]
                            binsNN=[0.0,0.1,0.3,0.4,0.5,0.625,0.750,0.875,1.0]
                            #[0.000,0.01,0.05,0.1,0.125,0.2,0.250,0.375,0.4,0.500,0.6,0.625,0.750,0.8,0.875,1.000])
                            binsD2jet=[0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
                    elif "2016" in year:
                            #binsNN=[0.0, 0.1, 0.5, 1.0]
                            #binsNN=[0.0,0.01,0.05,0.1,0.2,0.4,0.6,0.8,1.0]
                            binsNN=[0.0,0.01,0.05,0.1,0.2,0.4,0.7,1.0]
                            binsD2jet=[0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
                            #binsNN=[0.0,0.01,0.05,0.1,0.2,0.4,0.6,0.8,1.0]
                            #binsD2jet=[0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
            elif "tt_vbf_ggHMELA_bin3" in nom :                    
                    if "2018" in year:
                            #binsNN=[0.0,0.01,0.05,0.2,1.0]
                            binsNN=[0.0,0.1,0.2,0.3,0.4,0.5,0.625,1.0]
                    elif "2017" in year:
                            #binsNN=[0.0, 0.05, 1.0] ## Best2018
                            #binsNN=[0.0,0.01,0.05,0.2,1.0]
                            binsNN=[0.0,0.1,0.2,0.3,0.6,1.0]
                            binsD2jet=[0.0,0.2,0.4,0.6,0.8,1.0]
                            #binsD2jet=[0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
                    elif "2016" in year:
                            #binsNN=[0.0,0.01,0.05,0.2,1.0]
                            binsNN=[0.0,0.05,0.1,0.2,0.3,0.5,1.0]
                            binsD2jet=[0.0,0.2,0.4,0.6,0.8,1.0]
            if "vbf" in nom:
                    printer.gray("NN binning:\t"+str(binsNN)+ "\t"+str(len(binsNN)-1)+"\nD0-ggH binning:\t"+str(binsD2jet)+ "\t"+str(len(binsD2jet)-1))
    nbins1D = (len(binsNN)-1)*(len(binsD2jet)-1)
    
    for histo in histoList:
        h2 = histo.ReadObj()
        #printer.blue(h2.ClassName())
        if "TH" not in h2.ClassName(): continue        
        h3=h2.Clone()
        h3.SetName(h2.GetName())

        if 'vbf_' in nom and 'sync' not in nom:                
                bin_i=0
                histo=ROOT.TH1F("histo",h3.GetName(),nbins1D,0,nbins1D)
                for binX in range(len(binsNN)-1):
                        for binY in range(len(binsD2jet)-1):
                                bin_i=bin_i+1
                                #print "binX %s binY %s ==> %s"%(binX,binY,bin_i)
                                binNN_low=binsNN[binX]
                                binNN_high=binsNN[binX+1]
                                binD2jet_low=binsD2jet[binY]
                                binD2jet_high=binsD2jet[binY+1]
                                #if "data_obs" in h3.GetName():
                                #	print " bin %s: NN [%s,%s], D2jet [%s,%s]"%(bin,binNN_low,binNN_high,binD2jet_low,binD2jet_high)
                                # find these bins in input 2D plot:
                                binNN_low_i=h3.GetYaxis().FindBin(binNN_low)
                                binNN_high_i=h3.GetYaxis().FindBin(binNN_high)-1
                                binD2jet_low_i=h3.GetXaxis().FindBin(binD2jet_low)
                                binD2jet_high_i=h3.GetXaxis().FindBin(binD2jet_high)-1
                                error_merged = ROOT.Double()
                                yield_merged=h3.IntegralAndError(binD2jet_low_i,binD2jet_high_i,binNN_low_i,binNN_high_i,error_merged)
                                histo.SetBinContent(bin_i,yield_merged)
                                histo.SetBinError(bin_i,error_merged)
                mydir.cd()
                name_histo=h3.GetName()
                if args.usePowhegHVV is True:
                        printer.msg(name_histo)
                        name_histo=rename_histo(name_histo)
                        printer.gray(">> "+name_histo)
                        if 'VBF125' in name_histo or 'WH125' in name_histo or 'ZH125' in name_histo:
                                usePowheg(name_histo, histo, mydir)
                histo.Write(name_histo)

                if "Up" not in name_histo and "Down" not in name_histo and h3.Integral()>0.:
                        if abs(h3.Integral()/histo.Integral()-1.)>0.02:
                                printer.warning( "INPUT and OUTPUT YIELDS DO NOT MATCH: input yield %s: %s output yield: %s"%(h3.GetName(),h3.Integral(),histo.Integral()))
                                exit(0)


        else: # 0jet and boosted category just unroll
                histo=ROOT.TH1F("histo",h3.GetName(), h3.GetNbinsX()*h3.GetNbinsY(),0,h3.GetNbinsX()*h3.GetNbinsY())
                bin_i=0
                for binX in range(1,h3.GetNbinsX()+1):
                    for binY in range(1,h3.GetNbinsY()+1):
                            bin_i=bin_i+1
                            histo.SetBinContent(bin_i,h3.GetBinContent(binX,binY))
                            histo.SetBinError(bin_i,h3.GetBinError(binX,binY))
                            
                mydir.cd()
                name_histo=h3.GetName()
                if 'sync' in nom or 'tt_2018' in nom or args.usePowhegHVV is True:
                        printer.msg(name_histo)
                        name_histo=rename_histo(name_histo)
                        printer.gray(">> "+name_histo)
                        if 'IC' in inputfile and 'reweighted_ggH_htt_0Mf05ph0125' in name_histo:
                                printer.red("IC ggH int scale down by 1/2...")
                                histo.Scale(0.5)
                        if 'VBF125' in name_histo or 'WH125' in name_histo or 'ZH125' in name_histo:
                                usePowheg(name_histo, histo, mydir)
                histo.Write(name_histo)
                '''
                if 'sync' in nom or 'tt_2018' in nom or args.usePowhegHVV is True:
                        if (name_histo=="VBF125"):
                                printer.info("This is sync datacards... Powheg VBF will be used")
                                histo.Write("reweighted_qqH_htt_0PM125")
                                histo.Write("reweighted_qqH_htt_0M125")
                                histo.Write("reweighted_qqH_htt_0Mf05ph0125")
                        if (name_histo=="WH125"):
                                printer.info("This is sync datacards... Powheg WH will be used")
                                histo.Write("reweighted_WH_htt_0PM125")
                                histo.Write("reweighted_WH_htt_0M125")
                                histo.Write("reweighted_WH_htt_0Mf05ph0125")
                        if (name_histo=="ZH125"):
                                printer.info("This is sync datacards... Powheg ZH will be used")
                                histo.Write("reweighted_ZH_htt_0PM125")
                                histo.Write("reweighted_ZH_htt_0M125")
                                histo.Write("reweighted_ZH_htt_0Mf05ph0125")
                '''
    file.Close()

## Printing command for the next step
nextCmd="\n\npython run_optimizedBin_limits.py --emb=1"
nextCmd+=" --chn="+channel
#nextCmd+=" --year="+year[1:]
nextCmd+=" -i="+outputfile
if "a3" in outputfile:
        if "fa3ggH" in outputfile:
                nextCmd+=" --par=fa3ggH --ggHInt"
        else:
                nextCmd+=" --par=fa3"
        nextCmd+=" --useDCP=1"
else:
        nextCmd+=" --useDCP=0"
        if "a2" in outputfile:
                nextCmd+=" --par=fa2"
        elif "L1Zg" in outputfile:
                nextCmd+=" --par=fL1Zg"
        elif "L1" in outputfile:
                nextCmd+=" --par=fL1"
if "noDCP" in outputfile:
        nextCmd = nextCmd.replace("--useDCP=1", "--useDCP=0", 1)

nextCmd+=" -s\n\n"
printer.orange(nextCmd)

if "fa3" in inputfile and "DCPsym" not in inputfile and "noDCP" not in inputfile:
        userAnswer=raw_input("This is fa3, wanna use DCP? [Y/N]")
        if "Y" in userAnswer:
                printer.gray("Do this first")
                printer.orange("python Symmetrize_DCP.py -i "+outputfile+"\n\n")
                #sys.exit(0)


printer.gray("time : %.3fs"%(time.time() - start)  )
