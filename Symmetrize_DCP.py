#!/usr/bin/env python
import ROOT
from ROOT import *
import re, sys, os
from array import array
from scripts import printer
import operator
from optparse import OptionParser
import time


start = time.time()
parser = OptionParser()
parser.add_option('--input', '-i', action='store',
                  default="../../auxiliaries/datacards/2D_htt_tt_emb_sys_fa3ggH_2016_Mar17_mergedBins.root",
                  dest='inputPath',
                  help='input root files folder for the plot'
                  )
parser.add_option('--verbose', '-v', action='store_true',
                  default=False, dest='verbose',
                  help='verbose mode'
                  )
(options, args) = parser.parse_args()


filename_in = options.inputPath
filename_out=filename_in.replace(".root","_DCPsym.root")
#filename_out=filename_out+"_DCPsym.root"

file=ROOT.TFile(filename_in,"r")
dirList = []
for dir in file.GetListOfKeys():
        #print(dir.GetName())
        dirList.append(dir.GetName())
file.Close()

fileOut=ROOT.TFile(filename_out,"recreate")
printer.info( "output name: %s"%fileOut.GetName())

def set_negative_bins_to_0(histo):
    if options.verbose is True:
        print " --------->>>>  N bins= %s"%(histo.GetSize())
    for bin in range(1,histo.GetSize()):
        if histo.GetBinContent(bin)<0:
            histo.SetBinContent(bin,0.)
            if options.verbose is True:
                printer.orange(histo.GetName()+ " has negative bin. Its bin "+str(bin)+" has been set to 0. ")
    return histo

def GetKeyNames(dir):
        dir.cd()
        return [key.GetName() for key in gDirectory.GetListOfKeys()]

def mkdirOutputFile(outputfile, dirName):    
    keyList = GetKeyNames(outputfile)
    #print keyList
    if dirName not in keyList:
        outputfile.mkdir(dirName)

# loop over dirs (0jet/boosted/symetrized):
printer.whiteGreenBold("\tStart Symmetrization / Copying 0jet and boosted categories\t")
for dir in dirList:
    file=ROOT.TFile(filename_in,"r")
    #if "bin1" not in k1.GetName() : continue ###DEL
    # do nothing if 0jet/boosted dir: just save histos into new root file:
    if "0jet" in dir or "boost" in dir:
        printer.whiteBlackBold( "\t%s\t"%(dir))
        h1 = file.Get(dir)
        nom=dir
        if options.verbose is True:  printer.msg( "\nmaking dir %s"%(nom))
        mkdirOutputFile(fileOut, nom)
        if options.verbose is True:
            print " bkg DCP_plus: ", dir, "  -> output: ", nom
        h1.cd()
        histoList = gDirectory.GetListOfKeys()
        for k2 in histoList:
            h2 = k2.ReadObj()
            h3=h2.Clone()
            histo_name=k2.GetName()
            if "DO_NOT_USE" in histo_name or "_ac_" in histo_name or "data_obs_data" in histo_name:
                continue
            if 'jetFakes_' in histo_name and 'jetFakes_CMS' not in histo_name: continue
            #print "\n====> just save %s  %s"%(nom,histo_name)
            set_negative_bins_to_0(h3)
            fileOut.cd(nom)
            h3.SetName(k2.GetName())
            h3.Write()

    # do nothing for data:
    if "DCPp" in dir:
        printer.whiteRedBold( "\t%s\t"%(dir))
        h1 = file.Get(dir)
        nom=dir
        #print "making dir 2 %s"%(nom)
        mkdirOutputFile(fileOut, nom)
        #fileOut.mkdir(nom)
        h1.cd()
        histoList = gDirectory.GetListOfKeys()
        for k2 in histoList:            
            h2 = k2.ReadObj()
            h3 = h2.Clone()
            #h3.Sumw2()
            histo_name=k2.GetName()
            if histo_name=="data_obs":
                fileOut.cd(nom)
                h3.SetName(k2.GetName())                
                h3.Write()
                #print "\n====> just save %s  %s"%(nom,histo_name)

    # symetrize all except int and data in DCP bins:
    if "DCPm" in dir:
        printer.whiteBlueBold( "\t%s\t"%(dir))
        h1 = file.Get(dir)
        nom=dir
        mkdirOutputFile(fileOut, nom)
        nom_p=nom.replace("DCPm","DCPp")
        mkdirOutputFile(fileOut, nom_p)
        h1.cd()
        histoList = gDirectory.GetListOfKeys()
        for k2 in histoList:            
            h2 = k2.ReadObj()
            h3=h2.Clone()
            #h3.Sumw2()
            histo_name=k2.GetName()
            if options.verbose is True: printer.gray(histo_name)
            if histo_name=="data_obs":
                fileOut.cd(nom)
                h3.SetName(k2.GetName())
                h3.Write()
                #print "\n====> just save %s  %s"%(nom,histo_name)
                
            ## tt channel cleaner ##
            if "DO_NOT_USE" in histo_name or  "data_obs" in histo_name:
                continue
            if "_ac_" in histo_name : continue
            if 'jetFakes_' in histo_name and 'jetFakes_CMS' not in histo_name: continue
            if 'STJ' in histo_name: continue

            # Do not symmetrize interference histogram - for HVV, ggH interference will be symmetrize and vice versa
            if (("0Mf05ph0125" in histo_name) and ("ggH" in histo_name) and ("fa3ggH" in filename_in)): # reweighted_ggH_htt_0Mf05ph0125
                continue
            elif (("0Mf05ph0125" in histo_name) and ("WH" in histo_name or "ZH" in histo_name or "qqH" in histo_name) and "fa3ggH" not in filename_in):
                continue

            #print "\n====> sym for  %s"%(histo_name)
            # now read same histo from DCPp and sum them up and divide by 2 --> simetrize
            file.cd(nom.replace("DCPm","DCPp"))
            h3_c_p=gDirectory.Get(k2.GetName())
            h3_p=h3_c_p.Clone()
            if options.verbose is True:
                print "\n====> symmetrize this histogram  %s"%(histo_name)
                print " DCPm histo: %s, yield: %s    min: %s"%(histo_name,h3.Integral(),h3.GetMinimum())
                print " DCPp histo: %s, yield: %s    min: %s"%(histo_name,h3_p.Integral(),h3_p.GetMinimum())            
            h3.Add(h3_p)
            h3.Scale(0.5)

            # now save symetrized histo to DCPm and DCPp dirs in the output file:
            fileOut.cd(nom)
            h3.SetName(k2.GetName())
            set_negative_bins_to_0(h3)
            h3.Write()
            if options.verbose is True:
                print "\n====>  DONE: symmetrized histogram %s  %s"%(nom,histo_name)            
                print " ===>  DCPm histo: %s, yield: %s    min: %s"%(histo_name,h3.Integral(),h3.GetMinimum())
                print " ===>  DCPp histo: %s, yield: %s    min: %s"%(histo_name,h3.Integral(),h3.GetMinimum())

            fileOut.cd(nom_p)
            h3.SetName(k2.GetName())
            h3.Write()
            #print "\n====>  save sym %s  %s"%(nom_p,histo_name)
        
    file.Close()

# loop over dirs (anti-symetrized):
print "\n\n"
printer.whiteGreenBold("\tStart Anti-Symmetrization\t")
for dir in dirList:
    file=ROOT.TFile(filename_in,"r")
    #if "bin1" not in k1.GetName() : continue ###DEL
    #printer.gray( "\n ===> dir %s"%(k1.GetName()))
    # anti-symetrize int in DCP bins:
    if "DCPm" in dir:
        printer.whiteBlueBold( "\t%s\t"%(dir))
        h1 = file.Get(dir)
        nom=dir
        nom_p=nom.replace("DCPm","DCPp")
        h1.cd()
        histoList = gDirectory.GetListOfKeys()
        for k2 in histoList:            
            h2 = k2.ReadObj()
            h3_m=h2.Clone()
            histo_name=k2.GetName()
            # Pick up interference histograms
            if "0Mf05ph0125" in histo_name:
                if "fa3ggH" in filename_in:
                    if "ggH" not in histo_name: continue
                else:
                    if "ggH" in histo_name: continue
            else: continue
            if "_ac_" in histo_name: continue

            if options.verbose is True:
                print "\n====> Anti-symmetrize this histogram   %s"%(histo_name)
                print "\n DCPm histo: %s, yield: %s    min: %s"%(histo_name,h3_m.Integral(),h3_m.GetMinimum())

            name_SM=histo_name.replace("0Mf05ph0125","0PM125")
            name_PS=histo_name.replace("0Mf05ph0125","0M125")
            
            # take SM and PS from symetrized dir:
            fileOut.cd(nom)            
            h3_c_SM_m=gDirectory.Get(name_SM)
            h3_SM_m=h3_c_SM_m.Clone() ###
            h3_c_PS_m=gDirectory.Get(name_PS)
            h3_PS_m=h3_c_PS_m.Clone()

            maxmix_m=h3_m.Clone()
            maxmix_m.Add(h3_SM_m,0.5)
            maxmix_m.Add(h3_PS_m,0.5)
            
            # now read same histo from DCPp and sum them up and divide by 2 --> simetrize
            file.cd(nom_p)
            h3_c_p=gDirectory.Get(histo_name)
            h3_p=h3_c_p.Clone()

            if options.verbose is True:
                print " DCPp histo: %s, yield: %s    min: %s"%(histo_name,h3_p.Integral(),h3_p.GetMinimum())

            # take SM and PS from symetrized dir:
            fileOut.cd(nom_p)
            h3_c_SM_p=gDirectory.Get(name_SM)
            h3_SM_p=h3_c_SM_p.Clone()
            h3_c_PS_p=gDirectory.Get(name_PS)
            h3_PS_p=h3_c_PS_p.Clone()

            maxmix_p=h3_p.Clone()
            maxmix_p.Add(h3_SM_p,0.5)
            maxmix_p.Add(h3_PS_p,0.5)

            antisym_int=maxmix_p.Clone()
            antisym_int.Add(maxmix_m,-1)
            antisym_int.Scale(0.5)

            antisym_int_p=antisym_int.Clone()
            antisym_int_p.Add(h3_SM_p,0.5)
            antisym_int_p.Add(h3_PS_p,0.5)

            antisym_int_m=antisym_int.Clone()
            antisym_int_m.Scale(-1)
            antisym_int_m.Add(h3_SM_m,0.5)
            antisym_int_m.Add(h3_PS_m,0.5)

            
            # now save symetrized histo to DCPm and DCPp dirs in the output file:
            fileOut.cd(nom)
            antisym_int_m.SetName(histo_name)
            set_negative_bins_to_0(antisym_int_m)
            antisym_int_m.Write()
            if options.verbose is True:
                printer.blue( "\n====>  Anti-symmetrized histogram(Minus) %s  %s"%(nom,histo_name))
                print " ===>  DCPm histo: %s, yield: %s    min: %s"%(histo_name,antisym_int_m.Integral(),antisym_int_m.GetMinimum())

            fileOut.cd(nom_p)
            antisym_int_p.SetName(histo_name)
            set_negative_bins_to_0(antisym_int_p)
            antisym_int_p.Write()
            if options.verbose is True:
                printer.red( "\n====>  Anti-symmetrized histogram(Plus) %s  %s"%(nom_p,histo_name))
                print " ===>  DCPp histo: %s, yield: %s    min: %s"%(histo_name,antisym_int_p.Integral(),antisym_int_p.GetMinimum())
        
    file.Close()

nextCmd="\n\npython run_optimizedBin_limits.py --emb=1"
nextCmd+=" --chn=tt"#+channel
#nextCmd+=" --year="+year[1:]
nextCmd+=" -i="+filename_out
if "fa3ggH" in filename_out:
    nextCmd+=" --par=fa3ggH"
else:
    nextCmd+=" --par=fa3"
nextCmd+=" --useDCP=1"

nextCmd+=" -s\n\n"

## channel
if "_em_" in filename_out: nextCmd = nextCmd.replace("--chn=tt","--chn=em")
printer.orange(nextCmd)

printer.gray("time : %.3fs"%(time.time() - start)  )
